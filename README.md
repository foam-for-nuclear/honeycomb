# Honeycomb

[![pipeline status](https://gitlab.com/foam-for-nuclear/honeycomb/badges/main/pipeline.svg)](https://gitlab.com/foam-for-nuclear/honeycomb/-/commits/main)

Honeycomb is a web application to create, visualize, edit, translate, import and export lattices. The primary objective is to facilitate the modification of lattice for the nuclear engineering community, which are commonly used in nuclear codes such as [MCNP](https://mcnp.lanl.gov/), [Serpent](https://serpent.vtt.fi/serpent/), [OpenMC](https://openmc.org/), or [CASMO](https://www.studsvik.com/key-offerings/nuclear-simulation-software/software-products/casmo5/).

The application is accessible at the following URL: https://foam-for-nuclear.gitlab.io/honeycomb.


## Recommended publication for citing

Thomas Guilbaud (2024, June 17-19), *Honeycomb, an effective web tool to visualize and edit lattices*. In Transactions of the American Nuclear Society - ANS Annual Meeting, Las Vegas, USA, (pp. 1052-1053). doi.org/10.13182/T130-44800.


## How to use

Honeycomb is a lattice editor that uses a Graphical User Interface rendered via a web browser. This means that it can be used on any computer provided with an internet connection or even tablets. We used the React framework and web languages to develop the view with HTML and CSS, and Javascript for the logic. Honeycomb can handle square, hexagonal and circular lattices of any size that can be used to model respectively PWR, SFR and TRIGA reactors. Honeycomb is composed of three main panels (see example in [Fig. 1](./images/v2312-sfrLatticeExample.png)):
1. On the left, a main editing view displaying the lattice;
2. On the top right, a table listing the elements used, the color in the lattice, and the number of elements placed;
3. On the bottom right, a text area to copy-paste the lattice from or to an existing Monte Carlo file. Several examples in MCNP format are provided in the [examples](./examples/) folder. They can be copy-pasted in the text area of Honeycomb.

The user can copy-paste any lattice in the text area. Honeycomb will automatically render the lattice on the main view and update the list of elements by assigning a random color and counting the number of occurrences of the element. The user is free to change the color or rename an element which can be a character or a word. The lattice can also be translated into OpenMC format and vice-versa (only for hexagonal lattices).

To change an element at a specific location in the lattice, the user can *click* on the desired cell in the lattice view. The application will change the label and the color of the cell, both on the lattice view and in the text area. It is also possible to manually change the element in the text area of Honeycomb, which will automatically re-render the lattice. It is possible to increase or reduce the size of the lattice using the four adding and suppression buttons placed at the top, bottom, left, and right of the lattice main view.


### Heatmap distribution visualization

Honeycomb proposes to visualize distribution maps such as neutron flux or power density using the same format as regular lattice (see example in [Fig. 3](./images/v2312-distributionMapExample.png)). The user can select the colors for the lowest and the highest values. Then, the application performs a linear interpolation of the colors for each value.


### Speedup features

Honeycomb also features several options to speed up the editing of lattices.
- A symmetry option allows placing multiple elements at once using 2, 3, 4, or 6 symmetry rules.
- A symmetry flag allows one to quickly check the weighted average central symmetry of the lattice.
- One can permutate two cells by pressing the *Ctrl* key and dragging/dropping. This feature also works in symmetry mode.
- Several flip-and-rotate buttons allow the user to flip and rotate the lattice.


### Helper features

Several options have been implemented to help users design or navigate in a lattice. 
- The display of a circle can be dimensioned using the pitch of the lattice and the desired size of the circle. This feature can be used to create a reactor core that needs to be close to a cylinder to minimize neutron leakage.
- The display of letter/number locations of the elements.


### Tips

- The lattice can contain comments starting with "`$`", "`//`", "`#`", or "`%`". The comments will be removed during the lattice rendering.
- If the cell color is pure white (#FFFFFF), the label font will also be white, which makes the cell invisible.
- To improve performance on large lattices, it is recommended to remove external empty lines that are sometimes a line of zeros.
- It is possible to import/export the color map in text format by toggling the button next to the total elements in the color map section.
- The total is not counting the "0" element.
- Revert a change with the undo and redo buttons, or use *Ctrl+z* and *Ctrl+Shift+z*.
- Use the brush option with the *shift* key to quickly fill space.


## Gallery

![](./images/v2312-sfrLatticeExample.png)

*Fig 1: Example of a core lattice pattern for an SFR (see [example](./examples/lattice-example-sfr1.txt)).*


![](./images/v2312-pwrLatticeExample.png)

*Fig 2: Example of a core lattice pattern for a PWR (see [example](./examples/lattice-example-pwr1.txt)).*


![](./images/v2406-trigaLatticeExample.png)

*Fig 3: Example of a core lattice pattern for a TRIGA (see [example](./examples/lattice-example-triga1.txt)).*


![](./images/v2312-distributionMapExample.png)

*Fig 4: Example of neutron flux distribution heat map with arbitrary units (see [example](./examples/lattice-example-sfr2-distribution.txt)).*


![](./images/v2312-pwrAssemblyLatticeExample.png)

*Fig 5: Example of a fuel assembly lattice pattern for a PWR (see [example](./examples/lattice-example-pwrAssembly1.txt)).*


<img src="./images/ntpLatticeExample.png" width=600px/>

*Fig 6: Example of a core lattice pattern for an NTP.*


## Run the app locally

Install all the packages to build the project:
```bash
npm install
```

Run the following command to start the app locally:
```bash
npm run dev
```

To run the test suites:
```bash
npm run test
# or
npm run test-all
```

See the [package.json](package.json) file for more options.


## Copyright

© Contributions are individually acknowledged in the file's header.


## Contributors

Thomas Guilbaud, EPFL/Transmutex SA, Main author  
Marion Séminel, Style
