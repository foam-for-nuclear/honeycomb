/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/


import { Dispatch, SetStateAction, createContext, useCallback, useEffect, useState } from "react";
import { LatticeCanvas } from "../components/latticeCanvas";
import { ColorTable } from "../components/colorTable";
import { LatticeParameters } from "../components/latticeParameters";
import { LatticeTextarea } from "../components/latticeTextarea";

import { getCircularParametersAsText, getCircularParametersFromText, getLatticeTextAsArray, getLatticeTextFromArray } from "../lib/latticeEdition";
import { getColorMapAsText, getColorMapFromText } from "../lib/colorMapEdition";
import './styles.css';


export interface LatticeObject {
    lattice: string[][],
    colorMap: ColorLine[],
    circularParameters: CircularParameter[]
}

export interface LatticeParameterObject {
    pitch: number,
    elementDiameter: number, // Also equal to element diameter
    elementMargin: number, // always (pitch - elementDiameter)
    scale: number, // Scale from real dimensions to px
    tileSize: number,
    tileMargin: number, // Displayed margin, does not affect the real dimensions
    isElementCircle: boolean
}

export type ColorLine = { // For ColorMap table
    label: string,
    color: string,
    isActivated: boolean,
    isCount: boolean
}

export type CircularParameter = {
    r: number,
    theta: number
}

export type CircleLine = { // For circle helper table
    diameter: number,
    circleSize: number,
    color: string,
    length: number,
    numberTileInCircle: number,
    isDisplay: boolean
}

// eslint-disable-next-line react-refresh/only-export-components
export enum LatticeType {
    HEXAGON,
    SQUARE,
    TRIANGULAR,
    CIRCLE
}

// eslint-disable-next-line react-refresh/only-export-components
export enum LatticeTextCodeFormat {
    MCNP,
    SERPENT,
    OPENMC,
    CASMO
}

// eslint-disable-next-line react-refresh/only-export-components
export enum LatticeTextFormat {
    TILTED, // MCNP, Serpent
    RING, // OpenMC
    ALTERNATED, // CASMO
    CIRCULAR // Serpent lattice circular cluster array type 4
}

// eslint-disable-next-line react-refresh/only-export-components
export enum CopyState {
    DEFAULT,
    ERROR,
    SUCCESS
}

// eslint-disable-next-line react-refresh/only-export-components
export enum PhysicalParameter {
    WET_PERIMETER,
    VOLUMETRIC_AREA,
    STRUCTURE_AREA,
    TOTAL_AREA,
    CROSSING_SECTION,
    HYDRAULIC_DIAMETER,
    VOLUME_FRACTION_STRUCTURE 
}

export const LatticeContext = createContext<{
    latticeObject: LatticeObject,
    setLatticeObject: Dispatch<SetStateAction<LatticeObject>>,
    setLatticeObjectWithHistory: (value: LatticeObject) => void
    setLatticeType: Dispatch<SetStateAction<LatticeType>>,
    setLatticeTextFormat: Dispatch<SetStateAction<LatticeTextFormat>>,
    // setLatticeTextCodeFormat: Dispatch<SetStateAction<LatticeTextCodeFormat>>,
    setLatticeCoordOrientation: Dispatch<SetStateAction<number>>,
    setFontSizeScale: Dispatch<SetStateAction<number>>,
    setCircleDiameter: Dispatch<SetStateAction<CircleLine[]>>,
    setLatticeParameters: Dispatch<SetStateAction<LatticeParameterObject>>,
    setIsShowCircleDimensions: Dispatch<SetStateAction<boolean>>,
    setIsBorder: Dispatch<SetStateAction<boolean>>,
    setIsColorGradient: Dispatch<SetStateAction<boolean>>,
    undo: () => void,
    redo: () => void,
}>({
    latticeObject: {
        lattice: [["0", "0", "0"], ["0", "0", "0"], ["0", "0", "0"]],
        colorMap: [
            {label:"F", color:"#F07575", isActivated:true, isCount:true},
            {label:"W", color:"#75B3F0", isActivated:true, isCount:true},
            {label:"0", color:"#000000", isActivated:true, isCount:false}
        ],
        circularParameters: [{r: 10, theta: 0}, {r: 20, theta: 0}, {r: 30, theta: 0}]
    },
    setLatticeObject: () => {},
    setLatticeObjectWithHistory: () => {},
    setLatticeType: () => {},
    setLatticeTextFormat: () => {},
    // setLatticeTextCodeFormat: () => {},
    setLatticeCoordOrientation: () => {},
    setFontSizeScale: () => {},
    setCircleDiameter: () => {},
    setLatticeParameters: () => {},
    setIsShowCircleDimensions: () => {},
    setIsBorder: () => {},
    setIsColorGradient: () => {},
    undo: () => {},
    redo: () => {}
});

export function LatticeProvider()
{
    const [latticeObject, setLatticeObject] = useState(
        {
            lattice: [["0", "0", "0"], ["0", "0", "0"], ["0", "0", "0"]],
            colorMap: [
                {label:"F", color:"#F07575", isActivated:true, isCount:true},
                {label:"W", color:"#75B3F0", isActivated:true, isCount:true},
                {label:"0", color:"#000000", isActivated:true, isCount:false}
            ],
            circularParameters: [{r: 10, theta: 0}, {r: 20, theta: 0}, {r: 30, theta: 0}]
        }
    );
    const [latticeType, setLatticeType] = useState(LatticeType.HEXAGON);
    const [latticeTextFormat, setLatticeTextFormat] = useState(LatticeTextFormat.TILTED);
    // const [latticeTextCodeFormat, setLatticeTextCodeFormat] = useState(LatticeTextCodeFormat.MCNP);
    const [latticeCoordOrientation, setLatticeCoordOrientation] = useState(0);

    const [latticeStackHistory, setLatticeStackHistory] = useState(
        [{
            lattice: getLatticeTextFromArray(latticeObject.lattice),
            colorMap: getColorMapAsText(latticeObject.colorMap),
            circularParameters: getCircularParametersAsText(latticeObject.circularParameters)
        }]
    );
    
    const [nUndo, setNUndo] = useState(0);
    const [isFullscreen, setIsFullscreen] = useState(window.innerWidth <= 768);

    const [circleDiameter, setCircleDiameter] = useState<CircleLine[]>([]);
    
    const initialScale: number = 6;
    const initialPitch: number = 10;
    const initialElementDiameter: number = 9;
    const [latticeParameters, setLatticeParameters] = useState({
        pitch: initialPitch,
        elementDiameter: initialElementDiameter,
        elementMargin: initialPitch - initialElementDiameter,
        scale: initialScale,
        tileSize: initialScale * initialElementDiameter,
        tileMargin: initialScale * (initialPitch - initialElementDiameter) / 2,
        isElementCircle: false
    });
    const [fontSizeScale, setFontSizeScale] = useState(1);
    const [isShowCircleDimensions, setIsShowCircleDimensions] = useState(true);
    const [isBorder, setIsBorder] = useState(false);
    const [isColorGradient, setIsColorGradient] = useState(false);

    const callSetIsFullscreen = useCallback(() => {
        setIsFullscreen(curr => !curr);
    }, []);

    const appendToStackHistory = useCallback((newLatticeObject: LatticeObject) =>
    {
        setLatticeStackHistory((curr) => {
            if (
                getLatticeTextFromArray(newLatticeObject.lattice) === curr[curr.length-1].lattice
                && getColorMapAsText(newLatticeObject.colorMap) === curr[curr.length-1].colorMap
                && getCircularParametersAsText(newLatticeObject.circularParameters) === curr[curr.length-1].circularParameters
            ) {
                return(curr);
            }
            const newLatticeStackHistory = [...curr];

            if (nUndo > 0)
            {
                newLatticeStackHistory.splice(newLatticeStackHistory.length-nUndo, nUndo);
            }

            setNUndo(0);

            return([...newLatticeStackHistory, {
                lattice: getLatticeTextFromArray(newLatticeObject.lattice),
                colorMap: getColorMapAsText(newLatticeObject.colorMap),
                circularParameters: getCircularParametersAsText(newLatticeObject.circularParameters)
            }]);
        });
    }, [nUndo]);

    const undo = useCallback(() => {
        const length = latticeStackHistory.length - 1;
        if (length - nUndo >= 1)
        {
            setLatticeObject({
                ...latticeObject,
                lattice: getLatticeTextAsArray(
                    latticeStackHistory[length - (nUndo+1)].lattice
                ),
                colorMap: getColorMapFromText(
                    latticeStackHistory[length - (nUndo+1)].colorMap
                ),
                circularParameters: getCircularParametersFromText(
                    latticeStackHistory[length - (nUndo+1)].circularParameters
                )
            });
            setNUndo(nUndo+1);
        }
    }, [latticeObject, latticeStackHistory, nUndo]);

    const redo = useCallback(() => {
        if (nUndo > 0)
        {
            const length = latticeStackHistory.length - 1;
            setLatticeObject({
                ...latticeObject,
                lattice: getLatticeTextAsArray(
                    latticeStackHistory[length - (nUndo-1)].lattice
                ),
                colorMap: getColorMapFromText(
                    latticeStackHistory[length - (nUndo-1)].colorMap
                ),
                circularParameters: getCircularParametersFromText(
                    latticeStackHistory[length - (nUndo-1)].circularParameters
                )
            });
            setNUndo(nUndo-1);
        }
    }, [latticeObject, latticeStackHistory, nUndo]);

    const setLatticeObjectWithHistory = useCallback((newLatticeObject: LatticeObject) => {
        setLatticeObject(newLatticeObject)
        appendToStackHistory(newLatticeObject);
    }, [appendToStackHistory]);

    
    useEffect(() => {
        setLatticeParameters(curr => ({
            ...curr,
            elementMargin: curr.isElementCircle
                ? curr.pitch - curr.elementDiameter
                : 0 / curr.scale,
            tileSize: curr.isElementCircle
                ? curr.scale * curr.elementDiameter
                : curr.scale * curr.pitch,
            tileMargin: curr.isElementCircle
                ? curr.scale * (curr.pitch - curr.elementDiameter) / 2.0
                : 0
        }));
    }, [
        latticeParameters.pitch, latticeParameters.elementDiameter,
        latticeParameters.isElementCircle, latticeParameters.scale
    ]);


    return (
        <LatticeContext.Provider value={{
            latticeObject,
            setLatticeObject,
            setLatticeObjectWithHistory,
            setLatticeType,
            setLatticeTextFormat,
            // setLatticeTextCodeFormat,
            setLatticeCoordOrientation,
            setFontSizeScale,
            setCircleDiameter,
            setLatticeParameters,
            setIsShowCircleDimensions,
            setIsBorder,
            setIsColorGradient,
            undo,
            redo
        }}>
            <div className={"container " + (isFullscreen ? "grid-fullscreen" : "grid")}>
                <LatticeCanvas
                    latticeType={latticeType}
                    latticeTextFormat={latticeTextFormat}
                    latticeParameters={latticeParameters}
                    latticeCoordOrientation={latticeCoordOrientation}
                    fontSizeScale={fontSizeScale}
                    circleDiameter={circleDiameter}
                    isShowCircleDimensions={isShowCircleDimensions}
                    isBorder={isBorder}
                    isFullscreen={isFullscreen}
                    setIsFullscreen={callSetIsFullscreen}
                    appendToStackHistory={appendToStackHistory}
                />
                <div style={{
                    height: '100%',
                    gap: '1rem',
                    display: 'flex',
                    flexDirection: 'column',
                    // overflow: 'auto',
                }}>
                    <ColorTable />
                    <LatticeParameters
                        latticeType={latticeType}
                        latticeParameters={latticeParameters}
                        fontSizeScale={fontSizeScale}
                        circleDiameter={circleDiameter}
                        isShowCircleDimensions={isShowCircleDimensions}
                        isBorder={isBorder}
                        isColorGradient={isColorGradient}
                    />
                    <LatticeTextarea
                        latticeObject={latticeObject}
                        latticeType={latticeType}
                        latticeTextFormat={latticeTextFormat}
                        // latticeTextCodeFormat={latticeTextCodeFormat}
                        latticeCoordOrientation={latticeCoordOrientation}
                        latticeParameters={latticeParameters}
                    />
                </div>
            </div>
        </LatticeContext.Provider>
    );
}

// ************************************************************************** //
