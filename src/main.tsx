/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)

// ************************************************************************** //
