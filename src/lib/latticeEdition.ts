/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { CircularParameter, LatticeTextCodeFormat, LatticeType } from "../context/latticeContext";
import { computeCoordinates, padText } from "./mathFunctions";


//=============================================================================*
// Modification

export function addColLeftToLattice(latticeArray: string[][], element: string='0'): string[][]
{
    return(latticeArray.map(line => [element].concat(line)));
}

export function removeColLeftToLattice(latticeArray: string[][]): string[][]
{
    return(latticeArray.map(line => line.splice(1)));
}

export function addColRightToLattice(latticeArray: string[][], element: string='0'): string[][]
{
    return(latticeArray.map(line => line.concat([element])));
}

export function removeColRightToLattice(latticeArray: string[][]): string[][]
{
    return(latticeArray.map(line => line.slice(0, -1)));
}

export function addRowTopToLattice(latticeArray: string[][], element: string='0'): string[][]
{
    return([Array(latticeArray[0].length).fill(element)].concat(latticeArray));
}

export function removeRowTopToLattice(latticeArray: string[][]): string[][]
{
    return(latticeArray.slice(1));
}

export function addRowBottomToLattice(latticeArray: string[][], element: string='0'): string[][]
{
    return(latticeArray.concat([Array(latticeArray[0].length).fill(element)]));
}

export function removeRowBottomToLattice(latticeArray: string[][]): string[][]
{
    return(latticeArray.slice(0, -1));
}

export function addAllDirectionsToLattice(latticeArray: string[][], element: string='0'): string[][]
{
    return(
        addRowTopToLattice(
            addRowBottomToLattice(
                addColLeftToLattice(
                    addColRightToLattice(latticeArray, element),
                    element
                ),
                element
            ),
            element
        )
    );
}

export function removeAllDirectionsToLattice(latticeArray: string[][]): string[][]
{
    return(
        removeRowTopToLattice(
            removeRowBottomToLattice(
                removeColLeftToLattice(
                    removeColRightToLattice(latticeArray)
                )
            )
        )
    );
}

export function rotateLeftSquareLattice(latticeArray: string[][]): string[][]
{
    const newLattice: string[][] = [];

    for (let rowIdx = 0; rowIdx < latticeArray[0].length; rowIdx++)
    {
        const newRow: string[] = [];
        for (let colIdx = 0; colIdx < latticeArray.length; colIdx++)
        {
            newRow.push(latticeArray[colIdx][rowIdx]);
        }
        newLattice.push(newRow);
    }
    newLattice.reverse();

    return(newLattice);
}

export function rotateRightSquareLattice(latticeArray: string[][]): string[][]
{
    const newLattice: string[][] = [];

    for (let rowIdx = 0; rowIdx < latticeArray[0].length; rowIdx++)
    {
        const newRow: string[] = [];
        for (let colIdx = 0; colIdx < latticeArray.length; colIdx++)
        {
            newRow.push(latticeArray[colIdx][rowIdx]);
        }
        newRow.reverse();
        newLattice.push(newRow);
    }

    return(newLattice);
}

export function rotateHexagonalLattice(latticeArray: string[][], theta: number): string[][]
{
    if (!isSameRowColumnNumber(latticeArray))
    {
        return(latticeArray);
    }

    const coord = computeCoordinates(latticeArray, LatticeType.HEXAGON);

    if (coord.length === 0)
    {
        return(latticeArray);
    }

    // Deep copy lattice
    const newLattice = JSON.parse(JSON.stringify(latticeArray));

    for (let rowIdx = 0; rowIdx < latticeArray[0].length; rowIdx++)
    {
        for (let colIdx = 0; colIdx < latticeArray.length; colIdx++)
        {
            const x = coord[rowIdx][colIdx][0];
            const y = coord[rowIdx][colIdx][1];
    
            const newX = x * Math.cos(theta) - y * Math.sin(theta);
            const newY = x * Math.sin(theta) + y * Math.cos(theta);
    
            // Search the new element location based on the calculated new X and Y
            for (let j = 0; j < coord.length; j++)
            {
                let isBreak = false;
                for (let k = 0; k < coord[j].length; k++)
                {
                    if (
                        Math.abs(coord[j][k][0] - newX) < 1e-9 &&
                        Math.abs(coord[j][k][1] - newY) < 1e-9 && 
                        (rowIdx !== j || colIdx !== k)
                    ) {
                        newLattice[j][k] = latticeArray[rowIdx][colIdx];
                        isBreak = true;
                        break;
                    }
                }
                if (isBreak) {
                    break;
                }
            }
        }
    }

    return(newLattice);
}

export function flipHorizontalSquareLattice(latticeArray: string[][]): string[][]
{
    return(latticeArray.map(row => [...row].reverse()));
}

export function flipVerticalSquareLattice(latticeArray: string[][]): string[][]
{
    return([...latticeArray].reverse());
}

export function flipHexagonalLattice(latticeArray: string[][], theta: number): string[][]
{
    const coord = computeCoordinates(latticeArray, LatticeType.HEXAGON);
    
    if (coord.length === 0) {
        return(latticeArray);
    }

    // Deep copy lattice
    const newLattice = JSON.parse(JSON.stringify(latticeArray));

    for (let rowIdx = 0; rowIdx < latticeArray.length; rowIdx++)
    {
        for (let colIdx = 0; colIdx < latticeArray[rowIdx].length; colIdx++)
        {
            const x = coord[rowIdx][colIdx][0];
            const y = coord[rowIdx][colIdx][1];
    
            const newX = -x * Math.cos(2*theta) + y * Math.sin(2*theta);
            const newY =  x * Math.sin(2*theta) + y * Math.cos(2*theta);
    
            // Search the new element location based on the calculated new X and Y
            for (let j = 0; j < coord.length; j++)
            {
                let isBreak = false;
                for (let k = 0; k < coord[j].length; k++)
                {
                    if (
                        Math.abs(coord[j][k][0] - newX) < 1e-9 &&
                        Math.abs(coord[j][k][1] - newY) < 1e-9 &&
                        (rowIdx != j || colIdx != k)
                    ) {
                        newLattice[j][k] = latticeArray[rowIdx][colIdx];
                        isBreak = true;
                        break;
                    }
                }
                if (isBreak) {
                    break;
                }
            }
        }
    }

    return(newLattice);
}

export function replaceLabelInLattice(
    latticeArray: string[][],
    oldLabel: string,
    newLabel: string
): string[][]
{
    return (
        latticeArray
            .map(row => row
                .map(e => e === oldLabel ? newLabel : e)
            )
    );
}

export function replaceTileByIndexes
(
    latticeArray: string[][],
    row: number,
    col: number,
    newLabel: string
): string[][]
{
    if (row >= 0 && col >= 0 && row < latticeArray.length && col < latticeArray[row].length)
    {
        const newLattice = [...latticeArray];
        newLattice[row][col] = newLabel;
        return(newLattice);
    }
    return(latticeArray);
}


//=============================================================================*
// Boolean

export function isLatticeEmpty(latticeArray: string[][]): boolean
{
    return(latticeArray.flat().every(e => e === "0"));
}

export function isFirstRowZeros(latticeArray: string[][]): boolean
{
    return(latticeArray[0].every(e => e === "0"));
}

export function isLastRowZeros(latticeArray: string[][]): boolean
{
    return(latticeArray[latticeArray.length-1].every(e => e === "0"));
}

export function isFirstColumnZeros(latticeArray: string[][]): boolean
{
    return(latticeArray.map(e => e[0]).every(e => e === "0"));
}

export function isLastColumnZeros(latticeArray: string[][]): boolean
{
    return(latticeArray.map(e => e[e.length-1]).every(e => e === "0"));
}

export function isLatticeSquareSymmetricHorizontally(latticeArray: string[][]): boolean
{
    const latticeFliped = flipHorizontalSquareLattice(latticeArray);

    return(isSameLattice(latticeArray, latticeFliped));
}

export function isLatticeSquareSymmetricVertically(latticeArray: string[][]): boolean
{
    const latticeFliped = flipVerticalSquareLattice(latticeArray);

    return(isSameLattice(latticeArray, latticeFliped));
}

export function isLatticeSquareSymmetricRotationLeft(latticeArray: string[][]): boolean
{
    const latticeRotated = rotateLeftSquareLattice(latticeArray);

    return(isSameLattice(latticeArray, latticeRotated));
}

export function isLatticeSquareSymmetricRotationRight(latticeArray: string[][]): boolean
{
    const latticeRotated = rotateRightSquareLattice(latticeArray);

    return(isSameLattice(latticeArray, latticeRotated));
}

export function isLatticeHexagonSymmetricHorizontally(latticeArray: string[][]): boolean
{
    const latticeFliped = flipHexagonalLattice(latticeArray, Math.PI/2);

    return(isSameLattice(latticeArray, latticeFliped));
}

export function isLatticeHexagonSymmetricVertically(latticeArray: string[][]): boolean
{
    const latticeFliped = flipHexagonalLattice(latticeArray, 0);

    return(isSameLattice(latticeArray, latticeFliped));
}

export function isLatticeHexagonSymmetricUpLeft(latticeArray: string[][]): boolean
{
    const latticeFliped = flipHexagonalLattice(latticeArray, 2*Math.PI/3);

    return(isSameLattice(latticeArray, latticeFliped));
}

export function isLatticeHexagonSymmetricUpRight(latticeArray: string[][]): boolean
{
    const latticeFliped = flipHexagonalLattice(latticeArray, Math.PI/3);

    return(isSameLattice(latticeArray, latticeFliped));
}

export function isLatticeHexagonSymmetricRotationLeft(latticeArray: string[][]): boolean
{
    const latticeRotated = rotateHexagonalLattice(latticeArray, Math.PI/3);

    return(isSameLattice(latticeArray, latticeRotated));
}

export function isLatticeHexagonSymmetricRotationRight(latticeArray: string[][]): boolean
{
    const latticeRotated = rotateHexagonalLattice(latticeArray, -Math.PI/3);

    return(isSameLattice(latticeArray, latticeRotated));
}

export function isSameLattice(latticeArrayA: string[][], latticeArrayB: string[][]): boolean
{
    return(
        getLatticeTextFromArray(latticeArrayA) === getLatticeTextFromArray(latticeArrayB)
    );
}

export function isSameRowColumnNumber(latticeArray: string[][]): boolean
{
    return(
        isRegularLattice(latticeArray) &&
        latticeArray.length === latticeArray[0].length
    );
}

export function isRegularLattice(latticeArray: string[][]): boolean
{
    const nRows: number = latticeArray.length;
    const sizeRow1: number = latticeArray[0].length;
    for (let i = 1; i < nRows; i++)
    {
        if (latticeArray[i].length !== sizeRow1) {
            return(false);
        }
    }
    return(true);
}

export function isSymmetric(
    latticeArray: string[][],
    latticeType: LatticeType
): boolean {
    const mockCircularParams = latticeArray.map((_, idx) => {
        return({r: idx, theta: 0})
    });
    const coordinates = computeCoordinates(latticeArray, latticeType, 1, mockCircularParams);

    if ((isRegularLattice(latticeArray) || latticeType === LatticeType.CIRCLE) && coordinates.length > 0)
    {
        let avgX = 0;
        let avgY = 0;
        for (let i = 0; i < latticeArray.length; i++)
        {
            for (let j = 0; j < latticeArray[i].length; j++)
            {
                const x = coordinates[i][j][0];
                const y = coordinates[i][j][1];
                if (latticeArray[i][j] !== '0')
                {
                    // Compute the barycenter using the Unicode value of the
                    // character as weight
                    const weight: number = latticeArray[i][j]
                        .split('')
                        .map(e => e.charCodeAt(0))
                        .reduce((a, b) => a+b);
                    avgX += weight * x;
                    avgY += weight * y;
                }
            }
        }

        if (Math.abs(avgX) < 1e-6 && Math.abs(avgY) < 1e-6) {
            return(true);
        }
    }
    return(false);
}


//=============================================================================*
// Getter

export function getLatticeTextAsArray(text: string): string[][]
{
    return(
        text
            .trim()
            // Split the lattice into lines
            .split('\n')
            // Remove empty lines
            .filter(line => line !== "")
            // Remove comments starting at a line
            .filter(line => line.trim().substring(0, 1) !== "$")
            .filter(line => line.trim().substring(0, 2) !== "//")
            .filter(line => line.trim().substring(0, 1) !== "%")
            .filter(line => line.trim().substring(0, 1) !== "#")
            .map(line => line
                .trim()
                // Remove comments after the lattice line
                .split('$')[0]
                .split('//')[0]
                .split('%')[0]
                .split('#')[0]
                // Split the line into a list
                .split(' ')
                // Remove additional spaces between elements
                .filter(e => e !== "")
            )
    );
}

export function getLatticeTextFromArray(
    array: string[][], 
    isHexagon: boolean = false
): string {
    if (isHexagon) {
        return(
            array.map((e, idx) => {
                const row = e.join(' ');
                return(" ".repeat(idx) + row);
            }).join('\n')
        );
    }

    // Build naively a text with all elements separated by a single space 
    return(
        array.map(e => e.join(' ').trim()).join('\n')
    );
}

export function getCircularParametersAsText(params: CircularParameter[]): string
{
    return(
        params
            .map(row => row.r+', '+row.theta)
            .join('; ')
    );
}

export function getCircularParametersFromText(params: string): CircularParameter[]
{
    return(
        params
            .split('; ')
            .map(row => row.split(', '))
            .map(row => ({r: parseFloat(row[0]), theta: parseFloat(row[1])}))
    );
}

export function getFlattenLatticeText(latticeText: string): string
{
    return (
        latticeText
            .split('\n')
            .map(l => 
                l.split(' ').filter(e => e !== "").join(' ')
            )
            .join(' ')
    );
}

export function getNumberLabelInLatticeArrayByLabel
(
    latticeArray: string[][], 
    label: string
): number {
    return(
        latticeArray
            .flat()
            .filter(e => e === label)
            .length
    );
}

export function getLatticeHeader(
    latticeTextCodeFormat: LatticeTextCodeFormat,
    latticeType: LatticeType,
    NX: number,
    NY: number,
    pitch: number
): string
{
    if (latticeTextCodeFormat === LatticeTextCodeFormat.SERPENT)
    {
        const suffix: string = String(NX)+" "+String(NY)+" "+String(pitch);
        switch (latticeType) {
            case LatticeType.HEXAGON:
                return("lat uLat 3  0 0  "+suffix);
            case LatticeType.SQUARE:
                return("lat uLat 1  0 0  "+suffix);
            case LatticeType.TRIANGULAR:
                return("lat uLat 14  0 0  "+suffix)
            default:
                break;
        }
    }
    else if (latticeTextCodeFormat === LatticeTextCodeFormat.MCNP)
    {
        const hNXm: string = String(NX % 2 === 0 ? NX/2 : (NX-1)/2);
        const hNYm: string = String(NY % 2 === 0 ? NY/2 : (NY-1)/2);
        const hNXp: string = String(NX - parseInt(hNXm) - 1);
        const hNYp: string = String(NY - parseInt(hNYm) - 1);
        switch (latticeType) {
            case LatticeType.HEXAGON:
                return("lat=2 u=1  fill=-"+hNXm+":"+hNXp+" -"+hNYm+":"+hNYp);
            case LatticeType.SQUARE:
                return("lat=1 u=1  fill=-"+hNXm+":"+hNXp+" -"+hNYm+":"+hNYp);
            default:
                break;
        }
    }
    return('');
}

export function extractLabelsFromLatticeText(latticeArray: string[][]): string[]
{
    return(
        latticeArray
            .flat()
            .filter((label, index, self) => index === self.indexOf(label))
    );
}


//=============================================================================*
// Generation

export function generateRandomString(length: number): string
{
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++)
    {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return(result);
}


//=============================================================================*
// Translator

export function translateFromMCNPtoOpenMC(latticeTextMCNP: string): string
{
    const latticeMCNP = getLatticeTextAsArray(latticeTextMCNP);

    const nRow = latticeMCNP.length;
    const nCol = latticeMCNP[0].length;
    if (nRow % 2 !== 1 || nCol % 2 !== 1 || nCol !== nRow) {
        return('');
    }
    const centerX = Math.trunc(nRow/2);
    const centerY = Math.trunc(nCol/2);

    let latticeOpenMC = '';
    // Set initial indices at the center 
    let idxX = centerX;
    let idxY = centerY;
    for (let i = 0; i <= centerX; i++)
    {
        let ringList = '';
        // Special case for the centered element
        if (i == 0) {
            ringList += latticeMCNP[idxX][idxY];
        }
        // Loop over the element of a ring
        for (let j = 0; j < 6*i; j++)
        {   
            ringList += latticeMCNP[idxX][idxY] + ' ';
            if (j < i) {
                idxY += 1;
            }
            else if (i <= j && j < 2*i) {
                idxX += 1;
            }
            else if (2*i <= j && j < 3*i) {
                idxX += 1;
                idxY -= 1;
            }
            else if (3*i <= j && j < 4*i) {
                idxY -= 1;
            }
            else if (4*i <= j && j < 5*i) {
                idxX -= 1;
            }
            else if (5*i <= j && j < 6*i) {
                idxX -= 1;
                idxY += 1;
            }
        }
        latticeOpenMC += ringList + '\n';

        // Set position of the first element of a ring
        idxX = centerX-i-1;
        idxY = centerY;
    }
    return(latticeOpenMC.trim());
}

export function translateFromOpenMCtoMCNP(latticeTextOpenMC: string): string
{
    const latticeOpenMC = getLatticeTextAsArray(latticeTextOpenMC);

    const nRing = latticeOpenMC.length-1;

    const latticeMCNP: string[][] = [];
    for (let i = -nRing; i <= nRing; i++) {
        latticeMCNP.push(Array(2*nRing+1).fill('0'));
    }

    latticeMCNP[nRing][nRing] = latticeOpenMC[0][0];
    //Set initial indices at the center 
    let idxX = nRing-1;
    let idxY = nRing;
    for (let ringNumber = 1; ringNumber < nRing+1; ringNumber++)
    {
        for (let elementNumber = 0; elementNumber < 6*ringNumber; elementNumber++)
        {
            latticeMCNP[idxX][idxY] = latticeOpenMC[ringNumber][elementNumber];
            if (elementNumber < ringNumber) {
                idxY += 1;
            }
            else if (ringNumber <= elementNumber && elementNumber < 2*ringNumber) {
                idxX += 1;
            }
            else if (2*ringNumber <= elementNumber && elementNumber < 3*ringNumber) {
                idxY -= 1;
                idxX += 1;
            }
            else if (3*ringNumber <= elementNumber && elementNumber < 4*ringNumber) {
                idxY -= 1;
            }
            else if (4*ringNumber <= elementNumber && elementNumber < 5*ringNumber) {
                idxX -= 1;
            }
            else if (5*ringNumber <= elementNumber && elementNumber < 6*ringNumber) {
                idxX -= 1;
                idxY += 1;
            }
        }

        //Set position of the first element of a ring
        idxX = nRing-ringNumber-1;
        idxY = nRing;
    }

    return(getLatticeTextFromArray(latticeMCNP, true));
}

export function translateFromMCNPtoCASMO(latticeText: string): string
{
    const latticeArray = getLatticeTextAsArray(latticeText);
    const centerX = (latticeArray.length-1)/2;
    
    let latticeCASMO = "";

    for (let row = 0; row < latticeArray.length; row++)
    {
        if (row % 2 === 1)
        {
            latticeCASMO += " ";
        }
        
        if (row === centerX)
        {
            latticeCASMO += latticeArray[row].join(' ');
        }
        else
        {
            let line = [];
            if (row < centerX)
            {
                line = latticeArray[row].slice(Math.abs(row-centerX));
            }
            else
            {
                line = latticeArray[row].slice(0, latticeArray.length-Math.abs(row-centerX));
            }

            const n = Math.abs(row-centerX)/2 + (row+1)%2 * (latticeArray.length%4 === 3 ? 1 : 0);
            const zeros = "0 ".repeat(n).trim();
            const space = n >= 1 ? " " : "";
            latticeCASMO += zeros + space + line.join(' ') + space + zeros;
        }
        latticeCASMO += "\n";
    }

    return(latticeCASMO.trim());
}

export function translateFromCASMOtoMCNP(latticeText: string): string
{
    const latticeArray = getLatticeTextAsArray(latticeText);
    const centerX = (latticeArray.length-1)/2;
    
    let latticeMCNP = "";

    for (let row = 0; row < latticeArray.length; row++)
    {
        latticeMCNP += " ".repeat(row);

        const nRep = Math.abs(row-centerX);
        const n = Math.floor(nRep/2 + (row+1)%2 * (latticeArray.length%4 === 3 ? 1 : 0));

        const line = (
            n > 0
            ? latticeArray[row].slice(n, -n)
            : latticeArray[row]
        ).join(' ');
        
        if (row === centerX)
        {
            latticeMCNP += line;
        }
        else if (row < centerX)
        {
            latticeMCNP += "0 ".repeat(nRep) + line;
        }
        else if (row > centerX)
        {
            latticeMCNP += line + " 0".repeat(nRep);
        }
        
        latticeMCNP += "\n";
    }
    
    return(latticeMCNP.trim());
}

export function extractLatticeCircularFromText(latticeText: string): {array: string[][], parameters: CircularParameter[]}
{
    const lattice = getLatticeTextAsArray(latticeText);
    // const nRings = lattice[0][0];

    const latticeArray = lattice.slice(1).map((ring) => ring.slice(3));
    const parameters = lattice
        .slice(1)
        .map((ring) => ring.slice(0, 3))
        .map((ring) => ({
            r: parseFloat(ring[1]),
            theta: parseFloat(ring[2])
        }));

    return({
        array: latticeArray,
        parameters: parameters
    });
}

export function translateFromCircularToText(latticeArray: string[][], latticeCircularParameters: CircularParameter[]): string
{
    const maxN: number = Math.max(...latticeArray.map(ring => String(ring.length).length));
    const maxR: number = Math.max(...latticeCircularParameters.map(ring => String(ring.r).length));
    const maxT: number = Math.max(...latticeCircularParameters.map(ring => String(ring.theta).length));

    const strLatticeCircularParameters: string[] = latticeArray
        .map((ring, idx) => (
            latticeCircularParameters[idx] !== undefined
                ? padText(ring.length, maxN) + '  ' +
                  padText(latticeCircularParameters[idx]?.r || 0, maxR) + '  ' +
                  padText(latticeCircularParameters[idx]?.theta || 0, maxT)
                : padText(ring.length, maxN) + '  ' +
                  padText(0, maxR) + '  ' +
                  padText(0, maxT)
            )
        );

    const text: string = String(latticeArray.length) + '\n'
        + latticeArray.map((ring, idx) => 
            strLatticeCircularParameters[idx] + '  ' + ring.join(' ')
        ).join('\n');

    return(text);
}

// ************************************************************************** //
