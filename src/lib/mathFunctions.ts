/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { CircularParameter, LatticeType } from "../context/latticeContext";
import { isRegularLattice, isSameRowColumnNumber } from "./latticeEdition";

export function skewedRandom(): number
{
    const a = Math.pow(Math.random(), 2);
    if (Math.random() < 0.5) {
        return a;
    }
    return 1 - a;
}

export function padText(text: string | number, n: number=2): string
{
    return(("     " + text).slice(-n));
}

export function getCoordinatesBySymmetry(
    row: number, col: number, 
    latticeArray: string[][],
    symmetryNumber: number,
    latticeType: LatticeType,
    isSymmetryMode: boolean,
    circularParameters: CircularParameter[] = []
): number[][] {
    const elementCoord = [row, col];
    const listCoord: number[][] = [elementCoord];

    if (
        !(
            isSymmetryMode &&
            row >= 0 && col >= 0 &&
            (isRegularLattice(latticeArray) || latticeType === LatticeType.CIRCLE) &&
            (isSameRowColumnNumber(latticeArray) || latticeType === LatticeType.CIRCLE)
        )
    ) {
        return(listCoord);
    }

    const theta = 2*Math.PI / symmetryNumber;
    
    if (latticeType === LatticeType.HEXAGON)
    {
        const coord = computeCoordinates(latticeArray, LatticeType.HEXAGON);

        const idx0 = elementCoord[0];
        const idy0 = elementCoord[1];

        if (isNaN(idx0) || isNaN(idy0)) {
            return(listCoord);
        }

        const x = coord[idx0][idy0][0];
        const y = coord[idx0][idy0][1];

        for (let i = 1; i < symmetryNumber; i++)
        {
            const newX = x * Math.cos(i * theta) - y * Math.sin(i * theta);
            const newY = x * Math.sin(i * theta) + y * Math.cos(i * theta);

            for (let j = 0; j < coord.length; j++)
            {
                for (let k = 0; k < coord[j].length; k++)
                {
                    if (Math.abs(coord[j][k][0] - newX) < 1e-9 && Math.abs(coord[j][k][1] - newY) < 1e-9 && (idx0 != j || idy0 != k))
                    {
                        listCoord.push([j, k]);
                        break;
                    }
                }
            }
        }
    }
    else if (latticeType === LatticeType.SQUARE)
    {
        const offsetX = (latticeArray.length-1)/2;
        const offsetY = (latticeArray[0].length-1)/2;

        const x = elementCoord[0] - offsetX;
        const y = elementCoord[1] - offsetY;

        for (let i = 1; i < symmetryNumber; i++)
        {
            const newX = Math.round(x * Math.cos(i*theta) - y * Math.sin(i*theta) + offsetX);
            const newY = Math.round(x * Math.sin(i*theta) + y * Math.cos(i*theta) + offsetY);
            
            if (newX !== elementCoord[0] || newY !== elementCoord[1])
            {
                listCoord.push([newX, newY]);
            }
        }
    }
    else if (
        latticeType === LatticeType.CIRCLE && 
        latticeArray.length === circularParameters.length &&
        0 <= row && row <= latticeArray.length-1
    ) { 
        const n: number = latticeArray[row].length;

        for (let i = 1; i < symmetryNumber; i++)
        {
            const newAngle = (col + i * n / symmetryNumber) % n;
            if (newAngle !== elementCoord[1])
            {
                listCoord.push([row, newAngle]);
            }
        }
    }

    return(listCoord);
}

export function computeCircleSize(
    diameter: number,
    latticeScale: number
): number {
    return(
        // Math.round(diameter/latticePitch * (latticeTileSize + 2*latticeTileMargin) * 1e6)/1e6 * 100/window.innerWidth
        Math.round(diameter * latticeScale * 1e6)/1e6 // * 100/window.innerWidth
    );
}

export function computeCoordinates(
    latticeArray: string[][],
    latticeType: LatticeType,
    pitch: number = 1,
    circularParameters: CircularParameter[] = []
): number[][][] {
    const list: number[][][] = [];
    const nRows = latticeArray.length;

    if (latticeType === LatticeType.HEXAGON)
    {
        const summitToSummit = pitch * 2/Math.sqrt(3);
        for (let i = 0; i < nRows; i++)
        {
            const nCols = latticeArray[i].length;
            const temp: number[][] = [];
            for (let j = 0; j < nCols; j++)
            {
                const x = summitToSummit * 0.75 * (i - (nRows-1)/2);
                const y = pitch * (j - (nCols-1)/2);
                temp.push([x, y + (i - (nRows-1)/2)*pitch/2]);
            }
            list.push(temp);
        }
    }
    else if (latticeType === LatticeType.SQUARE)
    {
        for (let i = 0; i < nRows; i++)
        {
            const nCols = latticeArray[i].length;
            const temp: number[][] = [];
            for (let j = 0; j < nCols; j++)
            {
                const x = pitch * (i - (nRows-1)/2);
                const y = pitch * (j - (nCols-1)/2);
                temp.push([x, y]);
            }
            list.push(temp);
        }
    }
    else if (latticeType === LatticeType.CIRCLE && latticeArray.length === circularParameters.length)
    {
        for (let rowIdx = 0; rowIdx < latticeArray.length; rowIdx++)
        {
            const n: number = latticeArray[rowIdx].length;
            const r: number = circularParameters[rowIdx].r;
            const theta: number = circularParameters[rowIdx].theta;

            const temp: number[][] = [];

            for (let angleIdx = 0; angleIdx < n; angleIdx++)
            {
                const x = -r * Math.cos(angleIdx * 2*Math.PI/n + Math.PI/180*theta);
                const y = +r * Math.sin(angleIdx * 2*Math.PI/n + Math.PI/180*theta);
                temp.push([x, y]);
            }
            list.push(temp);
        }
    }
    return(list);
}

export function linearInterpolation(
    x1: number,
    x2: number,
    y1: number,
    y2: number,
    x: number
): number {
    const m = (y2-y1) / (x2-x1);
    return(m*(x-x1) + y1);
}

export function norm(x: number, y: number)
{
    return(Math.sqrt(Math.pow(x, 2.0) + Math.pow(y, 2.0)));
}

export function rotateVector(vec: number[], theta: number): number[]
{
    const thetaRad = theta * Math.PI/180;
    const cost = Math.cos(thetaRad);
    const sint = Math.sin(thetaRad);
    return([
        cost * vec[0] - sint * vec[1],
        sint * vec[0] + cost * vec[1]
    ]);
}

export function getNumberTileInCircle(
    latticeArray: string[][],
    diameter: number,
    latticePitch: number,
    latticeType: LatticeType,
    isCircle: boolean=false,
    elementDiameter: number=0,
    circularParameters: CircularParameter[] = []
): number {
    const coord: number[][] = computeCoordinates(
        latticeArray, latticeType, latticePitch, circularParameters
    ).flat();

    const halfPitch: number = latticePitch/2.0;

    if (isCircle || latticeType === LatticeType.CIRCLE)
    {
        return(
            coord.map(c => norm(c[0], c[1]) + elementDiameter/2.0)
                .filter(e => e <= diameter/2.0 + 1e-8)
                .length
        );
    }
    
    if (latticeType === LatticeType.HEXAGON)
    {
        const s = latticePitch / Math.sqrt(3.0);

        return(
            coord.map(c => [
                norm(c[0]+s, c[1]),
                norm(c[0]-s, c[1]),
                norm(c[0]+s/2, c[1]+halfPitch),
                norm(c[0]+s/2, c[1]-halfPitch),
                norm(c[0]-s/2, c[1]-halfPitch),
                norm(c[0]-s/2, c[1]+halfPitch)
            ])
            .filter(l => l.filter(e => e <= diameter/2).length === 6)
            .length
        );
    }
    else if (latticeType === LatticeType.SQUARE)
    {
        return(
            coord.map(c => [
                norm(c[0]+halfPitch, c[1]+halfPitch),
                norm(c[0]+halfPitch, c[1]-halfPitch),
                norm(c[0]-halfPitch, c[1]-halfPitch),
                norm(c[0]-halfPitch, c[1]+halfPitch)
            ])
            .filter(l => l.filter(e => e <= diameter/2).length === 4)
            .length
        );
    }

    return(parseFloat('nan'));
}

export function numberToLetter(n: number): string
{
    let element = "";
    if (n >= 26) {
        element = String.fromCharCode(65-1+(n+1 - (n % 26))/26);
    }
    element += String.fromCharCode(65+(n) % 26);
    return(element);
}


export function structureArea(structureDiameter: number, latticeType: LatticeType): number
{
    switch (latticeType) {
        case LatticeType.SQUARE:
            return(Math.PI * Math.pow(structureDiameter/2.0, 2.0));
        case LatticeType.HEXAGON:
            return(Math.PI * Math.pow(structureDiameter/2.0, 2.0) / 2.0);
        default:
            return(0);
    }
}

export function structureArea_r(Ap: number, latticeType: LatticeType): number
// Return structure diameter
{
    switch (latticeType) {
        case LatticeType.SQUARE:
            return(2.0 * Math.sqrt(Ap / Math.PI));
        case LatticeType.HEXAGON:
            return(2.0 * Math.sqrt(2.0 * Ap / Math.PI));
        default:
            return(0);
    }
}

export function wetPerimeter(structureDiameter: number, latticeType: LatticeType): number
{
    switch (latticeType) {
        case LatticeType.SQUARE:
            return(Math.PI * structureDiameter);
        case LatticeType.HEXAGON:
            return(Math.PI * structureDiameter / 2.0);
        default:
            return(0);
    }
}

export function wetPerimeter_r(Pm: number, latticeType: LatticeType): number
// Return structure diameter
{
    switch (latticeType) {
        case LatticeType.SQUARE:
            return(Pm / Math.PI);
        case LatticeType.HEXAGON:
            return(2.0 * Pm / Math.PI);
        default:
            return(0);
    }
}

export function crossingSection(At: number, Ap: number): number
{
    return(At - Ap);
}

export function crossingSection_r(Sp: number, Ap: number): number
// Return total area
{
    return(Sp + Ap);
}

export function totalArea(pitch: number, latticeType: LatticeType): number
{
    switch (latticeType) {
        case LatticeType.SQUARE:
            return(Math.pow(pitch, 2.0));
        case LatticeType.HEXAGON:
            return(Math.sqrt(3) * Math.pow(pitch, 2.0) / 4.0);
        default:
            return(0);
    }
}

export function totalArea_r(At: number, latticeType: LatticeType): number
// Return pitch
{
    switch (latticeType) {
        case LatticeType.SQUARE:
            return(Math.sqrt(At));
        case LatticeType.HEXAGON:
            return(Math.sqrt(4.0 * At / Math.sqrt(3)));
        default:
            return(0);
    }
}

export function hydraulicDiameter(Sp: number, Pm: number): number
{
    return(4.0 * Sp / Pm);
}

export function hydraulicDiameter_r(Dh: number, Pm: number): number
// Return crossing section
{
    return(Pm * Dh / 4.0);
}

export function volumetricArea(Pm: number, At: number): number
{
    return(Pm / At);
}

export function volumetricArea_r(Av: number, Pm: number): number
// Return total area
{
    return(Pm / Av);
}

export function volumeFractionStructure(Ap: number, At: number): number
{
    return(Ap / At);
}

export function volumeFractionStructure_r(fs: number, Ap: number): number
// Return total area
{
    return(Ap / fs);
}

export function hydraulicDiameterExpanded(
    pitch: number,
    pinDiameter: number,
    latticeType: LatticeType
): number {
    switch (latticeType) {
        case LatticeType.HEXAGON:
            return(
                Math.sqrt(3.0) * Math.pow(pitch, 2.0) / (Math.PI * pinDiameter/2.0) - pinDiameter
            );
    
        case LatticeType.SQUARE:
            return(
                2.0*Math.pow(pitch, 2.0) / (Math.PI * pinDiameter/2.0) - pinDiameter
            );
    }

    return(0);
}

export function volumetricAreaExpanded(
    pitch: number,
    pinDiameter: number,
    latticeType: LatticeType
): number {
    switch (latticeType) {
        case LatticeType.HEXAGON:
            return(
                4.0 * Math.PI * pinDiameter/2.0 / (Math.sqrt(3.0) * Math.pow(pitch, 2.0))
            );
    
        case LatticeType.SQUARE:
            return(
                2.0 * Math.PI * pinDiameter/2.0 / Math.pow(pitch, 2.0)
            );
    }
    return(0);
}

export function volumeFractionStructureExpanded(
    pitch: number,
    pinDiameter: number,
    latticeType: LatticeType
): number {
    switch (latticeType) {
        case LatticeType.HEXAGON:
            return(
                4.0 * Math.PI * Math.pow(pinDiameter/2.0, 2.0) / (2.0 * Math.sqrt(3.0) * Math.pow(pitch, 2.0))
            );
    
        case LatticeType.SQUARE:
            return(
                Math.PI * Math.pow(pinDiameter/2.0, 2.0) / Math.pow(pitch, 2.0)
            );
    }

    return(0);
}

export function getLatticeTypeAsString(latticeType: LatticeType): string
{
    switch (latticeType) {
        case LatticeType.HEXAGON:
            return('hexagon');
        case LatticeType.SQUARE:
            return('square');
        case LatticeType.CIRCLE:
            return('circular');
        case LatticeType.TRIANGULAR:
            return('triangular');
    }
    return('');
}

// ************************************************************************** //
