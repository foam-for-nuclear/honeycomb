/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { linearInterpolation, skewedRandom } from "./mathFunctions";

export type hsl = {
    h: number,
    s: number,
    l: number
}

export type rgb = {
    r: number,
    g: number,
    b: number
}

export function hexToRgb(hex: string): rgb
{
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    if (result)
    {
        return ({
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        });
    }
    return {r: 0, g: 0, b: 0};
}

export function hexToHsl(hex: string): hsl
{
    // Convert hex to RGB first
    const rgbColor = hexToRgb(hex);
    const r = rgbColor.r / 255;
    const g = rgbColor.g / 255;
    const b = rgbColor.b / 255;

    // Then to HSL
    const cmin = Math.min(r,g,b);
    const cmax = Math.max(r,g,b);
    const delta = cmax - cmin;
    
    // Find h
    let h = 0;
    if (delta === 0) {
        h = 0;
    } else if (cmax === r) {
        h = ((g - b) / delta) % 6;
    } else if (cmax === g) {
        h = (b - r) / delta + 2;
    } else {
        h = (r - g) / delta + 4;
    }
    h = Math.round(h * 60);
    if (h < 0) {
        h += 360;
    }
  
    // Find l and s
    const l = (cmax + cmin) / 2;
    const s = delta === 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));

    return {
        h: h,
        s: +(s * 100).toFixed(1),
        l: +(l * 100).toFixed(1)
    };
}

export function hslToHex(hsl: hsl): string
{
    const h = hsl.h;
    const s = hsl.s/100;
    const l = hsl.l/100;
  
    const c = (1 - Math.abs(2 * l - 1)) * s,
        x = c * (1 - Math.abs((h / 60) % 2 - 1)),
        m = l - c/2;
    let r = 0,
        g = 0, 
        b = 0; 
  
    if (0 <= h && h < 60) {
      r = c; g = x; b = 0;
    } else if (60 <= h && h < 120) {
      r = x; g = c; b = 0;
    } else if (120 <= h && h < 180) {
      r = 0; g = c; b = x;
    } else if (180 <= h && h < 240) {
      r = 0; g = x; b = c;
    } else if (240 <= h && h < 300) {
      r = x; g = 0; b = c;
    } else if (300 <= h && h < 360) {
      r = c; g = 0; b = x;
    }
    // Having obtained RGB, convert channels to hex
    let rString = Math.trunc((r + m) * 255).toString(16);
    let gString = Math.trunc((g + m) * 255).toString(16);
    let bString = Math.trunc((b + m) * 255).toString(16);
  
    // Prepend 0s, if necessary
    if (rString.length == 1)
      rString = "0" + rString;
    if (gString.length == 1)
      gString = "0" + gString;
    if (bString.length == 1)
      bString = "0" + bString;
  
    return(("#" + rString + gString + bString).toUpperCase());
}

export function rgbToString(rgb: rgb): string
{
    return "rgb("+ +rgb.r + "," + +rgb.g + "," + +rgb.b + ")";
}

export function hslToString(hsl: hsl): string
{
    return("hsl(" + hsl.h + "," + hsl.s + "%," + hsl.l + "%)");
}

export function getLuminance(color: string): number
{
    // From: https://www.w3.org/TR/WCAG20/#relativeluminancedef
    const rgb = hexToRgb(color);
    const Rs = rgb.r/255;
    const Gs = rgb.g/255;
    const Bs = rgb.b/255;
    const R = Rs <= 0.03928 ? Rs/12.92 : Math.pow((Rs+0.055)/1.055, 2.4);
    const G = Gs <= 0.03928 ? Gs/12.92 : Math.pow((Gs+0.055)/1.055, 2.4);
    const B = Bs <= 0.03928 ? Bs/12.92 : Math.pow((Bs+0.055)/1.055, 2.4);
    return(0.2126 * R + 0.7152 * G + 0.0722 * B);
}

export function getColorContrast(color1: string, color2: string): number
{
    // From: https://www.accessibility-developer-guide.com/knowledge/colours-and-contrast/how-to-calculate/
    const luminance1 = getLuminance(color1);
    const luminance2 = getLuminance(color2);
    return((Math.max(luminance1, luminance2)+0.05) / (Math.min(luminance1, luminance2)+0.05));
}

export function getFontColor(backgroundColor: string): string
{
    if (getColorContrast(backgroundColor, "#FFFFFF") < getColorContrast(backgroundColor, "#000000") && backgroundColor !== "#FFFFFF")
    {
        return("#000000");
    }
    return("#FFFFFF");
}


// Color theory function
export function getColorComplementary(hex: string): string
{
    const colorHsl = hexToHsl(hex);
    colorHsl.h = Math.abs(colorHsl.h + 180 - 360);
    return(hslToHex(colorHsl));
}

export function getColorHueMean(hex1: string, hex2: string): string
{
    const colorHsl1 = hexToHsl(hex1);
    const colorHsl2 = hexToHsl(hex2);
    const maxH = Math.max(colorHsl1.h, colorHsl2.h);
    const minH = Math.min(colorHsl1.h, colorHsl2.h);
    if (maxH-minH < (minH+360) - maxH) {
        colorHsl1.h = (maxH+minH)/2;
    } else {
        colorHsl1.h = Math.abs((minH+360+maxH)/2 - 360);
    }
    return(hslToHex(colorHsl1));
}

export function getColorSaturationMean(hex1: string, hex2: string): string
{
    const colorHsl1 = hexToHsl(hex1);
    const colorHsl2 = hexToHsl(hex2);
    colorHsl1.s = (colorHsl1.s + colorHsl2.s)/2;
    return(hslToHex(colorHsl1));
}

export function getColorLightnessMean(hex1: string, hex2: string): string
{
    const colorHsl1 = hexToHsl(hex1);
    const colorHsl2 = hexToHsl(hex2);
    colorHsl1.l = (colorHsl1.l + colorHsl2.l)/2;
    return(hslToHex(colorHsl1));
}

export function generateRandomColor(): string
{
    // return("#"+parseInt(Math.random() * 0xffffff).toString(16));
    return(hslToHex({
        h: Math.trunc(skewedRandom()*30)*12,
        s: Math.random()*5 + 80,
        l: Math.random()*5 + 70
    }));
}

export function generateRandomColorFromList(colors: string[]): string
{
    // Remove black color
    const colorsFiltered = colors.filter(c => c !== "#000000");

    if (colorsFiltered.length === 0)
    {
        return(generateRandomColor());
    }
    else if (colorsFiltered.length === 1)
    {
        return(getColorComplementary(colorsFiltered[0]));
    }

    const colorsHsl = colorsFiltered.map(c => hexToHsl(c));
    const avgS = colorsHsl.map(c => c.s).reduce((acc, curr) => acc+curr, 0)/colorsFiltered.length;
    const avgL = colorsHsl.map(c => c.l).reduce((acc, curr) => acc+curr, 0)/colorsFiltered.length;
    for (let index = 0; index < 30; index++)
    {
        const h = Math.trunc(skewedRandom() * 30)*12;
        if (!colorsHsl.map(c => c.h).includes(h))
        {
            return(hslToHex({h: h, s: avgS, l: avgL}));
        }
    }
    const newColor = {
        h: Math.trunc(Math.random() * 361 - 1), s: avgS, l: avgL
    };
    return(hslToHex(newColor));

    /*
    else if (colorsFiltered.length === 2)
    {
        return(getColorHueMean(colorsFiltered[0], colorsFiltered[1]));
    }

    if (Math.random() > 0.5)
    {
        let idx1 = 0;
        let idx2 = 0;
        let newColor = colorsFiltered[0];
        for (let i = 0; i < 10; i++)
        {
            idx1 = parseInt(Math.random() * colorsFiltered.length);
            idx2 = parseInt(Math.random() * colorsFiltered.length);
            newColor = getColorHueMean(colorsFiltered[idx1], colorsFiltered[idx2]);
            if (idx1 !== idx2 && !colorsFiltered.includes(newColor))
            {
                return(newColor);
            }
        }
    }
    
    const idx = parseInt(Math.random() * colorsFiltered.length);
    return(getColorComplementary(colorsFiltered[idx]));
    */
}

export function generateColorFromRange(
    lowColor: string,
    highColor: string,
    lowValue: number,
    highValue: number,
    value: number
): string {
    const lowHsl = hexToHsl(lowColor);
    const highHsl = hexToHsl(highColor);

    const h = linearInterpolation(lowValue, highValue, lowHsl.h, highHsl.h, value);
    const s = (lowHsl.s + highHsl.s)/2;
    const l = (lowHsl.l + highHsl.l)/2;

    return(hslToHex({h: h, s: s, l: l}));
}

export function generateColorListFromRange(
    list: number[],
    lowColor: string = "#75B3F0",
    highColor: string = "#F07575"
): string[] {
    const min = Math.min(...list);
    const max = Math.max(...list);
    return(list
        .map(value => generateColorFromRange(
            lowColor, highColor, min, max, value
        ))
    );
}

// ************************************************************************** //
