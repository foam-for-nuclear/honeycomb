/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { ColorLine } from "../context/latticeContext";
import {
    generateRandomColor,
    generateRandomColorFromList,
    hexToHsl
} from "./color";
import { extractLabelsFromLatticeText, getNumberLabelInLatticeArrayByLabel } from "./latticeEdition";

//=============================================================================*
// Getter

export function getColorMapAsText(colorMap: ColorLine[]): string
{
    return (
        colorMap
            .filter((e) => e.color !== undefined)
            .map((e) => e.label+":"+e.color.toUpperCase())
            .join('; ')
    );
}

export function getColorMapFromText(colorMapText: string): ColorLine[]
{
    return (
        colorMapText
            .split('; ')
            .map((curr) => {
                const row = curr.split(':').map(curr => curr.trim());
                return({
                    label: row[0],
                    color: row[1].toUpperCase(),
                    isActivated: true,
                    isCount: row[0] !== "0"
                });
            })
    );
}

// export function getColorMapLabelsAndColorsAsText(colorMapText: string)
// {
//     return(
//         colorMapText
//             .replace(/:true/g, '')
//             .replace(/:false/g, '')
//             .split("; ")
//             .map(e => e.trim())
//             .join("; ")
//     );
// }

export function extractLabelListFromColorMap(colorMap: ColorLine[])
{
    return(
        colorMap
            .map(e => e.label)
            .filter(e => e !== '')
    );
}

export function extractColorListFromColorMap(colorMap: ColorLine[])
{
    return(
        colorMap
            .map(e => e.color.toUpperCase())
            .filter(e => e !== '')
    );
}

export function completeColorMap(colorMap: ColorLine[], latticeArray: string[][])
{
    const labels = extractLabelListFromColorMap(colorMap);
    const colors = extractColorListFromColorMap(colorMap);

    const labelsFromLatticeText = extractLabelsFromLatticeText(latticeArray);

    const isAllLabelsAlreadyInList = labelsFromLatticeText.every(
        label => labels.includes(label)
    );

    let newColorMap = [...colorMap];
    
    if (!isAllLabelsAlreadyInList)
    {   
        const newLabels = labels
            // Concat existing labels with labels found in the text area
            .concat(labelsFromLatticeText)
            // Remove duplicate
            .filter((label, index, self) => self.indexOf(label) === index);
            
        const newColors = colors
            .filter(
                (_e, idx) => getNumberLabelInLatticeArrayByLabel(latticeArray, labels[idx]) > 0
            );
        
        for (let index = labels.length; index < newLabels.length; index++)
        {
            let foundColor = false;
            for (let iter = 0; iter < 30; iter++)
            {
                const newColor = generateRandomColorFromList(
                    colors.concat(newColors)
                );
                if (
                    newColors.length === 0
                    || !newColors
                        .filter(c => c !== "#000000")
                        .map(c => hexToHsl(c).h)
                        .includes(hexToHsl(newColor).h)
                ) {
                    newColors.push(newColor);
                    foundColor = true;
                    break;
                }
            }
            if (!foundColor)
            {
                newColors.push(generateRandomColor());
            }
        }
        
        newColorMap = newLabels
            // Remove not used label
            .filter(e => getNumberLabelInLatticeArrayByLabel(latticeArray, e.split(":")[0]) > 0)
            .map((e, idx) => ({
                label: e,
                color: newColors[idx] === undefined ? generateRandomColorFromList(colors) : newColors[idx],
                isActivated: true,
                isCount: e !== "0"
            }));
    }

    return(newColorMap);
}

//=============================================================================*
// Sorting

export function sortColorMap(colorMapToSort: string): string
{
    return(
        colorMapToSort
            .split('; ')
            .filter((e) => e.split(':')[0] !== '0') // Filter non '0' elements
            .sort()
            .concat(
                colorMapToSort
                    .split('; ')
                    .filter((e) => e.split(':')[0] === '0') // Filter '0' elements
            )
            .join('; ')
    );
}

// ************************************************************************** //
