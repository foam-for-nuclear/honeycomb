/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import '@testing-library/jest-dom';
import {
    crossingSection,
    getCoordinatesBySymmetry, getNumberTileInCircle, hydraulicDiameter, hydraulicDiameterExpanded,
    linearInterpolation,
    structureArea,
    totalArea,
    volumeFractionStructure,
    volumeFractionStructureExpanded,
    volumetricArea,
    volumetricAreaExpanded,
    wetPerimeter,
} from "../lib/mathFunctions";
import { LatticeType } from '../context/latticeContext';


describe("Test pure math functions", () => {
    it("Linear interpolation", () => {
        expect(
            linearInterpolation(5, 10, 20, 40, 7.5)
        ).toEqual(30);
    });
});

describe("Test counting in a circle functions", () => {
    const lattice = [
        ['0', 'F', 'F'],
          ['F', 'F', 'F'],
            ['F', 'F', '0']
    ];

    it("Test counting hexagons in a circle", () => {  
        expect(getNumberTileInCircle(lattice, 46, 15, LatticeType.HEXAGON, false)).toBe(7);
    });

    it("Test counting squares in a circle", () => {
        expect(getNumberTileInCircle(lattice, 47.6, 15, LatticeType.SQUARE, false)).toBe(5);
    });

    it("Test counting circles in a circle", () => {
        expect(getNumberTileInCircle([
            ['F'],
            ['W', 'F', 'F', 'W', 'F', 'F'],
            ['F', 'W', 'F', 'F', 'F', 'W', 'F', 'F', 'F', 'W', 'F', 'F'],
            ['F', 'F', 'W', 'F', 'F', 'W', 'F', 'F', 'W', 'F', 'F', 'W', 'F', 'F', 'W', 'F', 'F', 'W']],
            50, 0, LatticeType.CIRCLE, false, 0, 
            [{r: 0, theta: 0}, {r: 10, theta: 0}, {r: 20, theta: 0}, {r: 30, theta: 0}]
        )).toBe(19);
    });

    it("Test counting circles in a circle (hexagonal lattice)", () => {
        expect(getNumberTileInCircle(lattice, 45, 15, LatticeType.HEXAGON, true, 15)).toBe(7);
    });

    it("Test counting circles in a circle (square lattice)", () => {
        expect(getNumberTileInCircle(lattice, 45, 15, LatticeType.SQUARE, true, 15)).toBe(5);
    });

    it("Test counting circles in a circle (circle lattice)", () => {
        expect(getNumberTileInCircle([
            ['F'],
            ['W', 'F', 'F', 'W', 'F', 'F'],
            ['F', 'W', 'F', 'F', 'F', 'W', 'F', 'F', 'F', 'W', 'F', 'F'],
            ['F', 'F', 'W', 'F', 'F', 'W', 'F', 'F', 'W', 'F', 'F', 'W', 'F', 'F', 'W', 'F', 'F', 'W']],
            49, 0, LatticeType.CIRCLE, true, 9, 
            [{r: 0, theta: 0}, {r: 10, theta: 0}, {r: 20, theta: 0}, {r: 30, theta: 0}]
        )).toBe(19);
    });
});

describe("Test thermal-hydraulics functions for square lattice", () => {
    const diameter: number = 3.0;
    const pitch: number = 4.0;
    const latticeType = LatticeType.SQUARE;

    const At = totalArea(pitch, latticeType);
    const Ap = structureArea(diameter, latticeType);
    const Pm = wetPerimeter(diameter, latticeType);
    const Sp = crossingSection(At, Ap);
    const Dh = hydraulicDiameter(Sp, Pm);
    const fs = volumeFractionStructure(Ap, At);
    const Av = volumetricArea(Pm, At);

    it("Hydraulic diameter square", () => {
        expect(
            hydraulicDiameterExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(32.0/(1.5*Math.PI) - 3.0, 12);
    });

    it('Hydraulic diameter square against theoretical formula', () => {
        expect(
            hydraulicDiameterExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(Dh, 12);
    });

    it('Hydraulic diameter square against theoretical formula', () => {
        expect(
            hydraulicDiameterExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(diameter * (4/Math.PI * Math.pow(pitch/diameter, 2) - 1), 12);
    });
    
    it("Volumetric area square", () => {
        expect(
            volumetricAreaExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(3.0*Math.PI / 16.0, 12);
    });

    it('Volumetric area square against theoretical formula', () => {
        expect(
            volumetricAreaExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(Av, 12);
    });
    
    it("Volume fraction structure square", () => {
        expect(
            volumeFractionStructureExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(Math.PI * 9.0 / 64.0, 12);
    });

    it('Volume fraction structure square against theoretical formula', () => {
        expect(
            volumeFractionStructureExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(fs, 12);
    });
});

describe("Test thermal-hydraulics functions for hexagonal lattice", () => {
    const diameter: number = 3.0;
    const pitch: number = 4.0;
    const latticeType = LatticeType.HEXAGON;

    const At = totalArea(pitch, latticeType);
    const Ap = structureArea(diameter, latticeType);
    const Pm = wetPerimeter(diameter, latticeType);
    const Sp = crossingSection(At, Ap);
    const Dh = hydraulicDiameter(Sp, Pm);
    const fs = volumeFractionStructure(Ap, At);
    const Av = volumetricArea(Pm, At);

    it("Hydraulic diameter hexagon", () => {
        expect(
            hydraulicDiameterExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(Math.sqrt(3.0) * 16.0 / (1.5 * Math.PI) - 3.0, 12);
    });

    it('Hydraulic diameter hexagon against theoretical formula', () => {
        expect(
            hydraulicDiameterExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(Dh, 12);
    });

    it('Hydraulic diameter hexagon against theoretical formula', () => {
        expect(
            hydraulicDiameterExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(diameter*(2*Math.sqrt(3)/Math.PI * Math.pow(pitch/diameter, 2)-1), 12);
    });
    
    it("Volumetric area hexagon", () => {
        expect(
            volumetricAreaExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(6.0*Math.PI/(Math.sqrt(3.0) * 16.0), 12);
    });

    it('Volumetric area hexagon against theoretical formula', () => {
        expect(
            volumetricAreaExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(Av, 12);
    });
    
    it("Volume fraction structure hexagon", () => {
        expect(
            volumeFractionStructureExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(Math.PI * 9.0 / (Math.sqrt(3.0) * 32.0), 12);
    });

    it('Volume fraction structure hexagon against theoretical formula', () => {
        expect(
            volumeFractionStructureExpanded(pitch, diameter, latticeType)
        ).toBeCloseTo(fs, 12);
    });
});

describe("Test get coordinates by symmetry", () => {

    let latticeArray: string[][];

    beforeEach(() => {
        latticeArray = [
            ["NW", "N", "NE"],
            ["W",  "O", "E"],
            ["SW", "S", "SE"]
        ];
    });

    it('Get coordinates by symmetry square', () => {
        const listCoord = getCoordinatesBySymmetry(
            2, 2, latticeArray, 4, LatticeType.SQUARE, true
        );
    
        expect(JSON.parse(JSON.stringify(listCoord))).toEqual(
            [
                [ 2, 2 ], [ 0, 2 ], [ 0, 0 ], [ 2, 0 ]
            ]
        );
    });
    
    it('Get coordinates by symmetry hexagon', () => {
        const listCoord = getCoordinatesBySymmetry(
            2, 1, latticeArray, 6, LatticeType.HEXAGON, true
        );
    
        expect(JSON.parse(JSON.stringify(listCoord))).toEqual(
            [
                [ 2, 1 ], [ 1, 2 ], [ 0, 2 ], [ 0, 1 ], [ 1, 0 ], [ 2, 0 ]
            ]
        );
    });

    it('Get number tiles in hexagon lattice', () => {
        expect(
            getNumberTileInCircle(latticeArray, 0.62, 0.2, LatticeType.HEXAGON)
        ).toEqual(7);
    });

    it('Get number tiles in square lattice', () => {
        expect(
            getNumberTileInCircle(latticeArray, 0.64, 0.2, LatticeType.SQUARE)
        ).toEqual(5);
    });
});

// ************************************************************************** //
