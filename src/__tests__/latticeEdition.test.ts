/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import '@testing-library/jest-dom';
import {
    translateFromMCNPtoOpenMC,
    translateFromOpenMCtoMCNP,
    translateFromCASMOtoMCNP,
    translateFromMCNPtoCASMO,
    addColLeftToLattice,
    removeColLeftToLattice,
    addColRightToLattice,
    removeColRightToLattice,
    addRowTopToLattice,
    removeRowTopToLattice,
    addRowBottomToLattice,
    removeRowBottomToLattice,
    removeAllDirectionsToLattice,
    addAllDirectionsToLattice,
    rotateLeftSquareLattice,
    rotateRightSquareLattice,
    flipVerticalSquareLattice,
    flipHorizontalSquareLattice,
    replaceLabelInLattice,
    replaceTileByIndexes,
    isLatticeEmpty,
    isFirstRowZeros,
    isLastRowZeros,
    isFirstColumnZeros,
    isLastColumnZeros,
    isRegularLattice,
    isSameLattice,
    isLatticeSquareSymmetricHorizontally,
    isLatticeSquareSymmetricVertically,
    isSymmetric,
    isLatticeSquareSymmetricRotationLeft,
    isLatticeSquareSymmetricRotationRight,
    isLatticeHexagonSymmetricHorizontally,
    isLatticeHexagonSymmetricVertically,
    isLatticeHexagonSymmetricUpLeft,
    isLatticeHexagonSymmetricUpRight,
    isLatticeHexagonSymmetricRotationLeft,
    isLatticeHexagonSymmetricRotationRight,
    rotateHexagonalLattice,
    flipHexagonalLattice,
    getNumberLabelInLatticeArrayByLabel,
    getLatticeTextAsArray,
    getLatticeTextFromArray,
    getFlattenLatticeText
} from "../lib/latticeEdition";
import { LatticeType } from '../context/latticeContext';


describe('Lattice edition functions', () => {
    
    let latticeArrayMCNP: string[][];

    beforeEach(() => {
        latticeArrayMCNP = [
            ["NW", "N", "NE"],
            ["W",  "O", "E"],
            ["SW", "S", "SE"]
        ];
    });

    it('Add F column to left of lattice', () => {
        expect(
            addColLeftToLattice(latticeArrayMCNP, "F")
        ).toEqual(
            [["F", "NW", "N", "NE"], ["F", "W", "O", "E"], ["F", "SW", "S", "SE"]]
        );
    });
    
    it('Remove left column of lattice', () => {
        expect(
            removeColLeftToLattice(latticeArrayMCNP)
        ).toEqual(
            [["N", "NE"], ["O", "E"], ["S", "SE"]]
        );
    });

    it('Add F column to right of lattice', () => {
        expect(
            addColRightToLattice(latticeArrayMCNP, "F")
        ).toEqual(
            [["NW", "N", "NE", "F"], ["W", "O", "E", "F"], ["SW", "S", "SE", "F"]]
        );
    });
    
    it('Remove right column of lattice', () => {
        expect(
            removeColRightToLattice(latticeArrayMCNP)
        ).toEqual(
            [["NW", "N"], ["W", "O"], ["SW", "S"]]
        );
    });
    
    it('Add F row to top of lattice', () => {
        expect(
            addRowTopToLattice(latticeArrayMCNP, "F")
        ).toEqual(
            [["F", "F", "F"], ["NW", "N", "NE"], ["W", "O", "E"], ["SW", "S", "SE"]]
        );
    });
    
    it('Remove top row of lattice', () => {
        expect(
            removeRowTopToLattice(latticeArrayMCNP)
        ).toEqual(
            [["W", "O", "E"], ["SW", "S", "SE"]]
        );
    });
    
    it('Add F row to bottom of lattice', () => {
        expect(
            addRowBottomToLattice(latticeArrayMCNP, "F")
        ).toEqual(
            [["NW", "N", "NE"], ["W", "O", "E"], ["SW", "S", "SE"], ["F", "F", "F"]]
        );
    });
    
    it('Remove bottom row of lattice', () => {
        expect(
            removeRowBottomToLattice(latticeArrayMCNP)
        ).toEqual(
            [["NW", "N", "NE"], ["W", "O", "E"]]
        );
    });

    it('Add F all directions to lattice', () => {
        expect(
            addAllDirectionsToLattice(latticeArrayMCNP, "F")
        ).toEqual(
            [
                ["F", "F", "F", "F", "F"],
                ["F", "NW", "N", "NE", "F"],
                ["F", "W", "O", "E", "F"],
                ["F", "SW", "S", "SE", "F"],
                ["F", "F", "F", "F", "F"]
            ]
        );
    });
    
    it('Remove all directions of lattice', () => {
        expect(
            removeAllDirectionsToLattice(latticeArrayMCNP)
        ).toEqual(
            [["O"]]
        );
    });

    it('Rotate square lattice left', () => {
        expect(
            rotateLeftSquareLattice(latticeArrayMCNP)
        ).toEqual(
            [
                ["NE", "E", "SE"],
                ["N", "O", "S"],
                ["NW", "W", "SW"]
            ]
        );
    });

    it('Rotate square lattice right', () => {
        expect(
            rotateRightSquareLattice(latticeArrayMCNP)
        ).toEqual(
            [
                ["SW", "W", "NW"],
                ["S", "O", "N"],
                ["SE", "E", "NE"]
            ]
        );
    });

    it('Rotate hexagonal lattice 60 deg', () => {
        expect(
            rotateHexagonalLattice(latticeArrayMCNP, Math.PI/3)
        ).toEqual(
            [
                ["NW", "NE", "E" ],
                   ["N" , "O" , "S" ],
                      ["W" , "SW", "SE"]
            ]
        );
    });

    it('Flip square lattice vertical', () => {
        expect(
            flipVerticalSquareLattice(latticeArrayMCNP)
        ).toEqual(
            [
                ["SW", "S", "SE"],
                ["W", "O", "E"],
                ["NW", "N", "NE"]
            ]
        );
    });

    it('Flip square lattice horizontal', () => {
        expect(
            flipHorizontalSquareLattice(latticeArrayMCNP)
        ).toEqual(
            [
                ["NE", "N", "NW"],
                ["E", "O", "W"],
                ["SE", "S", "SW"]
            ]
        );
    });

    it("Flip hexagon lattice horizontally", () => {
        expect(
            flipHexagonalLattice(latticeArrayMCNP, Math.PI/2)
        ).toEqual(
            [
                ["NW", "NE", "N" ],
                   ["E" , "O" , "W" ],
                      ["S" , "SW", "SE"]
            ]
        );
    });

    it("Flip hexagon lattice vertically", () => {
        expect(
            flipHexagonalLattice(latticeArrayMCNP, 0)
        ).toEqual(
            [
                ["NW", "SW", "S" ],
                   ["W" , "O" , "E" ],
                      ["N" , "NE", "SE"]
            ]
        );
    });
    
    it("Flip hexagon lattice up left", () => {
        expect(
            flipHexagonalLattice(latticeArrayMCNP, 2*Math.PI/3)
        ).toEqual(
            [
                ["SE", "E" , "NE"],
                   ["S" , "O" , "N" ],
                      ["SW", "W" , "NW"]
            ]
        );
    });
    
    it("Flip hexagon lattice up right", () => {
        expect(
            flipHexagonalLattice(latticeArrayMCNP, Math.PI/3)
        ).toEqual(
            [
                ["NW", "N" , "W" ],
                   ["NE", "O" , "SW"],
                      ["E" , "S" , "SE"]
            ]
        );
    });

    it("Replace label in lattice", () => {
        expect(
            replaceLabelInLattice(latticeArrayMCNP, "W", "WW")
        ).toEqual(
            [
                ["NW", "N", "NE"],
                ["WW", "O", "E"],
                ["SW", "S", "SE"]
            ]
        );
    });

    it("Replace label in lattice", () => {
        expect(
            replaceTileByIndexes(latticeArrayMCNP, 1, 0, "WW")
        ).toEqual(
            [
                ["NW", "N", "NE"],
                ["WW", "O", "E"],
                ["SW", "S", "SE"]
            ]
        );
    });
});


describe('Lattice test functions', () => {

    let latticeArrayMCNP: string[][];
    let symmSquareLatticeArrayMCNP: string[][];
    let symmHexagonalLatticeArrayMCNP: string[][];

    beforeEach(() => {
        latticeArrayMCNP = [
            ["0", "0", "0", "0", "0"],
            ["0", "NW", "N", "NE", "0"],
            ["0", "W", "O", "E", "0"],
            ["0", "SW", "S", "SE", "0"],
            ["0", "0", "0", "0", "0"]
        ];

        symmSquareLatticeArrayMCNP = [
            ["0", "0", "0", "0", "0"],
            ["0", "F", "W", "F", "0"],
            ["0", "W", "O", "W", "0"],
            ["0", "F", "W", "F", "0"],
            ["0", "0", "0", "0", "0"]
        ];

        symmHexagonalLatticeArrayMCNP = [
            ["0", "0", "R", "R", "R"],
               ["0", "R", "W", "W", "R"],
                  ["R", "W", "O", "W", "R"],
                     ["R", "W", "W", "R", "0"],
                        ["R", "R", "R", "0", "0"]
        ];
    });

    it('Lattice is not empty', () => {
        expect(isLatticeEmpty(latticeArrayMCNP)).toBe(false);
    });

    it('Lattice is empty', () => {
        expect(
            isLatticeEmpty([["0", "0", "0"], ["0", "0", "0"], ["0", "0", "0"]])
        ).toBe(true);
    });

    it('First row zeros', () => {
        expect(isFirstRowZeros(latticeArrayMCNP)).toBe(true);
    });

    it('Last row zeros', () => {
        expect(isLastRowZeros(latticeArrayMCNP)).toBe(true);
    });

    it('First column zeros', () => {
        expect(isFirstColumnZeros(latticeArrayMCNP)).toBe(true);
    });

    it('Last column zeros', () => {
        expect(isLastColumnZeros(latticeArrayMCNP)).toBe(true);
    });

    it('Square lattice is symmetric horizontally', () => {
        expect(
            isLatticeSquareSymmetricHorizontally(symmSquareLatticeArrayMCNP)
        ).toBe(true);
    });
    
    it('Square lattice is symmetric vertically', () => {
        expect(
            isLatticeSquareSymmetricVertically(symmSquareLatticeArrayMCNP)
        ).toBe(true);
    });

    it('Square lattice is symmetric rotation left', () => {
        expect(
            isLatticeSquareSymmetricRotationLeft(symmSquareLatticeArrayMCNP)
        ).toBe(true);
    });

    it('Square lattice is symmetric rotation right', () => {
        expect(
            isLatticeSquareSymmetricRotationRight(symmSquareLatticeArrayMCNP)
        ).toBe(true);
    });

    it('Hexagon lattice is symmetric horizontally', () => {
        expect(
            isLatticeHexagonSymmetricHorizontally(symmHexagonalLatticeArrayMCNP)
        ).toBe(true);
    });

    it('Hexagon lattice is symmetric vertically', () => {
        expect(
            isLatticeHexagonSymmetricVertically(symmHexagonalLatticeArrayMCNP)
        ).toBe(true);
    });

    it('Hexagon lattice is symmetric up left', () => {
        expect(
            isLatticeHexagonSymmetricUpLeft(symmHexagonalLatticeArrayMCNP)
        ).toBe(true);
    });

    it('Hexagon lattice is symmetric up right', () => {
        expect(
            isLatticeHexagonSymmetricUpRight(symmHexagonalLatticeArrayMCNP)
        ).toBe(true);
    });

    it('Hexagon lattice is symmetric rotation left', () => {
        expect(
            isLatticeHexagonSymmetricRotationLeft(symmHexagonalLatticeArrayMCNP)
        ).toBe(true);
    });

    it('Hexagon lattice is symmetric rotation right', () => {
        expect(
            isLatticeHexagonSymmetricRotationRight(symmHexagonalLatticeArrayMCNP)
        ).toBe(true);
    });
    
    it('Same lattice', () => {
        expect(isSameLattice(latticeArrayMCNP, latticeArrayMCNP)).toBe(true);
    });
    
    it('Lattice is regular', () => {
        expect(isRegularLattice(latticeArrayMCNP)).toBe(true);
    });
    
    it('Square lattice is symmetric (barycenter)', () => {
        expect(isSymmetric(symmSquareLatticeArrayMCNP, LatticeType.SQUARE)).toBe(true);
    });
    
    it('Hexagon lattice is symmetric (barycenter)', () => {
        expect(isSymmetric(symmHexagonalLatticeArrayMCNP, LatticeType.HEXAGON)).toBe(true);
    });

    it('Circular lattice is symmetric (barycenter)', () => {
        expect(isSymmetric([
            ['F'],
            ['W', 'F', 'F', 'W', 'F', 'F'],
            ['F', 'W', 'F', 'F', 'F', 'W', 'F', 'F', 'F', 'W', 'F', 'F'],
            ['F', 'F', 'W', 'F', 'F', 'W', 'F', 'F', 'W', 'F', 'F', 'W', 'F', 'F', 'W', 'F', 'F', 'W']], 
            LatticeType.CIRCLE
        )).toBe(true);
    });
});

describe('Lattice get info', () => {

    let symmHexagonalLatticeArrayMCNP: string[][];
    let symmHexagonalLatticeTextMCNP: string;
    let squareLatticeTextMCNP: string;

    beforeEach(() => {
        symmHexagonalLatticeArrayMCNP = [
            ["0", "0", "R", "R", "R"],
               ["0", "R", "W", "W", "R"],
                  ["R", "W", "O", "W", "R"],
                     ["R", "W", "W", "R", "0"],
                        ["R", "R", "R", "0", "0"]
        ];

        symmHexagonalLatticeTextMCNP = "0 0 R R R\n 0 R W W R\n  R W O W R\n   R W W R 0\n    R R R 0 0";
        squareLatticeTextMCNP = "0 0 R R R\n0 R W W R\nR W O W R\nR W W R 0\nR R R 0 0";
    });

    it('Get lattice from text to array', () => {
        expect(
            getLatticeTextAsArray(symmHexagonalLatticeTextMCNP)
        ).toEqual(
            symmHexagonalLatticeArrayMCNP
        );
    });

    it('Get hexagonal lattice from array to text', () => {
        expect(
            getLatticeTextFromArray(symmHexagonalLatticeArrayMCNP, true)
        ).toBe(
            symmHexagonalLatticeTextMCNP
        );
    });

    it('Get square lattice from array to text', () => {
        expect(
            getLatticeTextFromArray(symmHexagonalLatticeArrayMCNP, false)
        ).toBe(
            squareLatticeTextMCNP
        );
    });
    
    it('Get flatten lattice in text format', () => {
        expect(
            getFlattenLatticeText(symmHexagonalLatticeTextMCNP)
        ).toBe(
            "0 0 R R R 0 R W W R R W O W R R W W R 0 R R R 0 0"
        );
    });
    
    it('Count number labels in lattice', () => {
        expect(
            getNumberLabelInLatticeArrayByLabel(symmHexagonalLatticeArrayMCNP, "O")
        ).toBe(1);
        expect(
            getNumberLabelInLatticeArrayByLabel(symmHexagonalLatticeArrayMCNP, "W")
        ).toBe(6);
        expect(
            getNumberLabelInLatticeArrayByLabel(symmHexagonalLatticeArrayMCNP, "R")
        ).toBe(12);
        expect(
            getNumberLabelInLatticeArrayByLabel(symmHexagonalLatticeArrayMCNP, "0")
        ).toBe(6);
    });
});

describe('Lattice translation functions', () => {

    test('MCNP -> OpenMC', () => {
        const latticeMCNP = "0 B C\n D E F\n  G H 0";
        const latticeOpenMC = translateFromMCNPtoOpenMC(latticeMCNP);
        expect(latticeOpenMC).toBe("E\nB C F H G D");
    });
    
    test('OpenMC -> MCNP', () => {
        const latticeOpenMC = "E\nB C F H G D";
        const latticeMCNP = translateFromOpenMCtoMCNP(latticeOpenMC);
        expect(latticeMCNP).toBe("0 B C\n D E F\n  G H 0");
    });
    
    test('MCNP -> CASMO', () => {
        const latticeMCNP = "0 0 A B C\n 0 D E F G\n  H I J K L\n   M N O P 0\n    Q R S 0 0";
        const latticeCASMO = translateFromMCNPtoCASMO(latticeMCNP);
        expect(latticeCASMO).toBe("0 A B C 0\n D E F G\nH I J K L\n M N O P\n0 Q R S 0");
    });
    
    test('CASMO -> MCNP', () => {
        const latticeCASMO = "0 A B C 0\n D E F G\nH I J K L\n M N O P\n0 Q R S 0";
        const latticeMCNP = translateFromCASMOtoMCNP(latticeCASMO);
        expect(latticeMCNP).toBe("0 0 A B C\n 0 D E F G\n  H I J K L\n   M N O P 0\n    Q R S 0 0");
    });

    test('CASMO -> MCNP large', () => {
        const latticeCASMO = "0 0 0 0 0 0 0 0\n 0 0 F F F 0 0 \n0 0 F F F F 0 0\n 0 F F W F F 0 \n0 0 F F F F 0 0\n 0 0 F F F 0 0 \n0 0 0 0 0 0 0 0";
        const latticeMCNP = translateFromCASMOtoMCNP(latticeCASMO);
        expect(latticeMCNP).toBe("0 0 0 0 0 0 0\n 0 0 0 F F F 0\n  0 0 F F F F 0\n   0 F F W F F 0\n    0 F F F F 0 0\n     0 F F F 0 0 0\n      0 0 0 0 0 0 0");
    });
});

// ************************************************************************** //
