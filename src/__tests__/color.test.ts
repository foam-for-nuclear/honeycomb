/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import '@testing-library/jest-dom';
import { generateColorListFromRange } from "../lib/color";


describe("Test color list generation", () => {

    it("Generate linear color gradient list", () => {
        expect(
            generateColorListFromRange([1, 2, 3, 4, 5, 6], "#75B3F0", "#F07575")
        ).toEqual(
            ['#74B2F0', '#74F0D7', '#74F081', '#BEF074', '#F0CB74', '#F07474']
        );
    });
});

// ************************************************************************** //
