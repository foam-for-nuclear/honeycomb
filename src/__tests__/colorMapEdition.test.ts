/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import '@testing-library/jest-dom';
import {
    completeColorMap,
    extractColorListFromColorMap,
    extractLabelListFromColorMap,
    getColorMapAsText,
    getColorMapFromText,
    sortColorMap
} from "../lib/colorMapEdition";


const colorMap = [
    {label: "F", color: "#F07575", isActivated: true, isCount: true},
    {label: "W", color: "#75B3F0", isActivated: true, isCount: true},
    {label: "0", color: "#000000", isActivated: true, isCount: false},
    {label: "3", color: "#F0748D", isActivated: true, isCount: true},
    {label: "Z", color: "#74EFA5", isActivated: true, isCount: true},
    {label: "P1", color: "#748DEF", isActivated: true, isCount: true},
    {label: "P2", color: "#A574EF", isActivated: true, isCount: true},
];

const colorMapText = "F:#F07575; W:#75B3F0; 0:#000000; 3:#F0748D; Z:#74EFA5; P1:#748DEF; P2:#A574EF";


test('ColorMap to text', () => {
    expect(getColorMapAsText(colorMap)).toBe(colorMapText);
});

test('ColorMap from text', () => {
    expect(getColorMapFromText(colorMapText)).toEqual(colorMap);
});

test('Extract labels from ColorMap', () => {
    expect(
        extractLabelListFromColorMap(colorMap)
    ).toEqual(["F", "W", "0", "3", "Z", "P1", "P2"]);
});

test('Extract colors from ColorMap', () => {
    expect(
        extractColorListFromColorMap(colorMap)
    ).toEqual(["#F07575", "#75B3F0", "#000000", "#F0748D", "#74EFA5", "#748DEF", "#A574EF"]);
});

test('Complete color map', () => {
    const lattice = [
        ["NW", "N", "NE"],
        ["W",  "O", "E"],
        ["SW", "S", "SE"]
    ];

    const newColorMap = completeColorMap(colorMap, lattice);

    const expectColorMap = [
        { label: 'W', color: '#75B3F0', isActivated: true, isCount: true },
        { label: 'NW', color: '#BEEF74', isActivated: true, isCount: true },
        { label: 'N', color: '#EF8C74', isActivated: true, isCount: true },
        { label: 'NE', color: '#EF74D6', isActivated: true, isCount: true },
        { label: 'O', color: '#74EFD6', isActivated: true, isCount: true },
        { label: 'E', color: '#EFD674', isActivated: true, isCount: true },
        { label: 'SW', color: '#7474EF', isActivated: true, isCount: true },
        { label: 'S', color: '#74EF8C', isActivated: true, isCount: true },
        { label: 'SE', color: '#74EF74', isActivated: true, isCount: true }
    ];
    
    expect(
        extractLabelListFromColorMap(newColorMap)
    ).toEqual(
        extractLabelListFromColorMap(expectColorMap)
    );
});

test('Sort colorMap', () => {
    expect(sortColorMap(colorMapText)).toBe(
        "3:#F0748D; F:#F07575; P1:#748DEF; P2:#A574EF; W:#75B3F0; Z:#74EFA5; 0:#000000"
    );
});

// ************************************************************************** //
