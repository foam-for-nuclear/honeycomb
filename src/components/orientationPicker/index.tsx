/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { memo } from "react";
import { Button } from "../button";
import { useComponentVisible } from "../../hooks/useComponentVisible";
import orientationIcon from "../../assets/orientation-icon.svg";
import './styles.css';


function OrientationPicker({
    orientation,
    setOrientation,
    titleButton = "",
    classNameButton = ""
}: {
    orientation: number,
    setOrientation: React.Dispatch<React.SetStateAction<number>>,
    titleButton?: string,
    classNameButton?: string,
}) {
    const {ref, isComponentVisible, setIsComponentVisible} = useComponentVisible(false);


    return(
        <div ref={ref}>
            <Button
                className={classNameButton}
                title={titleButton}
                onClick={() => setIsComponentVisible(curr => !curr)}
            >
                flip_camera_android
            </Button>

            {isComponentVisible && (
                <div className='orientation-dropdown'>
                    <div className="orientation-dropdown-title">
                        Coordinates Orientation
                    </div>
                    <div className="orientation-picker-row">
                        <input
                            className="orientation-picker-input"
                            value={orientation}
                            type="number"
                            min="0"
                            max="360"
                            onChange={(e) => setOrientation(parseFloat(e.target.value))}
                        />
                        deg
                    </div>
                    <input
                        className="orientation-picker-input-range"
                        type="range"
                        min="0"
                        max="360"
                        step="5"
                        value={orientation}
                        onChange={(e) => setOrientation(parseFloat(e.target.value))}
                    />
                    <img
                        className="orientation-picker-icon"
                        style={{transform: "rotate("+orientation+"deg)"}}
                        src={orientationIcon}
                        alt="Lattice orientation"
                        title="Lattice orientation"
                    />
                </div>
            )}
        </div>
    );
}

const memoOrientationPicker = memo(OrientationPicker);

export { memoOrientationPicker as OrientationPicker };

// ************************************************************************** //

