/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { CSSProperties, useCallback, useContext, useMemo, useRef, useState } from "react";

import {
    addAllDirectionsToLattice,
    addColLeftToLattice,
    addColRightToLattice,
    addRowBottomToLattice,
    addRowTopToLattice,
    isRegularLattice,
    isSymmetric,
    removeAllDirectionsToLattice,
    removeColLeftToLattice,
    removeColRightToLattice,
    removeRowBottomToLattice,
    removeRowTopToLattice,
    replaceTileByIndexes
} from '../../lib/latticeEdition';
import { computeCircleSize, getCoordinatesBySymmetry, numberToLetter } from "../../lib/mathFunctions";

import { LatticeObject, LatticeContext, LatticeTextFormat, LatticeType, CircleLine, LatticeParameterObject } from "../../context/latticeContext";
import { LatticeToolbar } from "../latticeToolbar";
import { LatticeRow } from "../latticeRow";
import { LatticeTile } from "../latticeTile";
import { LatticeCircle } from "../latticeCircle";
import { LatticeCircularAddButtons } from "../latticeCircularAddButtons";
import { Button } from "../button";

import symmetryIcon from "../../assets/symmetry-icon.svg";
import orientationIcon from "../../assets/orientation-icon.svg";

import './styles.css';


export function LatticeCanvas({
    latticeType,
    latticeTextFormat,
    latticeCoordOrientation,
    latticeParameters,
    fontSizeScale = 1,
    circleDiameter,
    isShowCircleDimensions,
    isBorder,
    isFullscreen,
    setIsFullscreen,
    appendToStackHistory
} : {
    latticeType: LatticeType,
    latticeTextFormat: LatticeTextFormat,
    latticeCoordOrientation: number,
    latticeParameters: LatticeParameterObject,
    fontSizeScale?: number
    circleDiameter: CircleLine[],
    isShowCircleDimensions: boolean,
    isBorder: boolean,
    isFullscreen: boolean,
    setIsFullscreen: () => void,
    appendToStackHistory: (value: LatticeObject) => void
}) {
    const {
        latticeObject,
        setLatticeObject,
        setLatticeObjectWithHistory,
        setLatticeParameters,
        setCircleDiameter
    } = useContext(LatticeContext);
    
    const [isSymmetryMode, setIsSymmetryMode] = useState(false);
    const [symmetryNumber, setSymmetryNumber] = useState(2);
    const [isCircleHelperDisplay, setIsCircleHelperDisplay] = useState(false);
    const [isShowRowColumnNames, setIsShowRowColumnNames] = useState(false);
    const [isShowTileLabel, setIsShowTileLabel] = useState(true);
    const [isBrushMode, setIsBrushMode] = useState(false);
    const [isDeleteMode, setIsDeleteMode] = useState(false);
    const [isGreyScale, setIsGreyScale] = useState(false);

    const isSymmetryFlag = useMemo(() => 
        isSymmetric(latticeObject.lattice, latticeType)
    , [latticeObject, latticeType]);

    const latticeSizeStats = useMemo(() => {
        if (latticeType === LatticeType.CIRCLE) {
            return('');
        }
        else if (isRegularLattice(latticeObject.lattice))
        {
            return(latticeObject.lattice.length + ' rows | ' + latticeObject.lattice[0].length + ' cols');
        }
        return("Non regular shape");
    }, [latticeObject.lattice, latticeType]);

    const latticeCanvasRef = useRef<HTMLDivElement | null>(null);
    const latticeSectionRef = useRef<HTMLTableSectionElement | null>(null);

    const [rowLabel, setRowLabel] = useState(0);
    const [colLabel, setColLabel] = useState(0);

    const latticeArrayWithLabel: string[][] = useMemo(() => {
        if (latticeType !== LatticeType.CIRCLE)
        {
            const newLatticeArray = addColLeftToLattice(
                addColRightToLattice(
                    addRowTopToLattice(
                        addRowBottomToLattice(latticeObject.lattice, '/'),
                    '/'),
                '/'),
            '/');
            newLatticeArray[0][colLabel] = "\\";
            newLatticeArray[rowLabel][0] = "\\";
            return(newLatticeArray);
        }
        return(latticeObject.lattice)
    }, [latticeType, latticeObject.lattice, colLabel, rowLabel]);

    const listCoordOver = useMemo(() =>
        getCoordinatesBySymmetry(
            rowLabel-1,
            colLabel-1,
            latticeObject.lattice,
            symmetryNumber,
            latticeType,
            isSymmetryMode,
            latticeObject.circularParameters
        )
    , [
        colLabel, isSymmetryMode, latticeObject.circularParameters,
        latticeObject.lattice, latticeType, rowLabel, symmetryNumber
    ]);

    const latticeSectionStyle: CSSProperties = useMemo(() => (
        isFullscreen
            ? {
                position: 'relative',
                top: '0rem'
            }
            : {
                // position: 'sticky',
                // top: '1rem'
            }
    ), [isFullscreen]);

    const isLatticeTextFormatTilted = useMemo(() =>
        latticeTextFormat === LatticeTextFormat.TILTED
    , [latticeTextFormat]);

    const callSetIsSymmetryMode = useCallback(() => {
        setIsSymmetryMode((curr) => !curr);
    }, []);
    
    const callSetIsCircleHelperDisplay = useCallback(() => {
        setIsCircleHelperDisplay((curr) => !curr);
    }, []);
    
    const callSetSymmetryNumber = useCallback((n: number) => {
        setSymmetryNumber(n);
    }, []);

    const callSetIsShowRowColumnNames = useCallback(() => {
        setIsShowRowColumnNames((curr) => !curr);
    }, []);

    const callSetIsShowTileLabel = useCallback(() => {
        setIsShowTileLabel((curr) => !curr);
    }, []);

    const callSetIsBrushMode = useCallback(() => {
        setIsBrushMode((curr) => !curr);
    }, []);

    const callSetIsDeleteMode = useCallback(() => {
        setIsDeleteMode((curr) => !curr);
    }, []);

    const callSetIsGreyScale = useCallback(() => {
        setIsGreyScale((curr) => !curr);
    }, []);

    const addColLeft = useCallback(() =>
    {
        const newLatticeArray = isLatticeTextFormatTilted
            ? addColLeftToLattice(latticeObject.lattice)
            : addAllDirectionsToLattice(latticeObject.lattice);
        
        setLatticeObjectWithHistory({...latticeObject, lattice: newLatticeArray});
    }, [isLatticeTextFormatTilted, latticeObject, setLatticeObjectWithHistory]);

    const removeColLeft = useCallback(() =>
    {
        if (latticeObject.lattice[0].length >= 2)
        {
            const newLatticeArray = isLatticeTextFormatTilted
                ? removeColLeftToLattice(latticeObject.lattice)
                : removeAllDirectionsToLattice(latticeObject.lattice);

            setLatticeObjectWithHistory({...latticeObject, lattice: newLatticeArray});
        }
    }, [isLatticeTextFormatTilted, latticeObject, setLatticeObjectWithHistory]);

    const addColRight = useCallback(() =>
    {
        const newLatticeArray = isLatticeTextFormatTilted
            ? addColRightToLattice(latticeObject.lattice)
            : addAllDirectionsToLattice(latticeObject.lattice);
        
        setLatticeObjectWithHistory({...latticeObject, lattice: newLatticeArray});
    }, [isLatticeTextFormatTilted, latticeObject, setLatticeObjectWithHistory]);

    const removeColRight = useCallback(() =>
    {
        if (latticeObject.lattice[0].length >= 2)
        {
            const newLatticeArray = isLatticeTextFormatTilted
                ? removeColRightToLattice(latticeObject.lattice)
                : removeAllDirectionsToLattice(latticeObject.lattice);
            
            setLatticeObjectWithHistory({...latticeObject, lattice: newLatticeArray});
        }
    }, [isLatticeTextFormatTilted, latticeObject, setLatticeObjectWithHistory]);

    const addRowTop = useCallback(() =>
    {
        const newLatticeArray = isLatticeTextFormatTilted
            ? addRowTopToLattice(latticeObject.lattice)
            : addAllDirectionsToLattice(latticeObject.lattice);

        const newParams = [...latticeObject.circularParameters];
        newParams.splice(0, 0, {
            r: newParams[0].r / 2,
            theta: 0
        });
        
        setLatticeObjectWithHistory({
            ...latticeObject,
            lattice: newLatticeArray,
            circularParameters: newParams
        });
    }, [isLatticeTextFormatTilted, latticeObject, setLatticeObjectWithHistory]);

    const removeRowTop = useCallback(() =>
    {
        if (latticeObject.lattice.length >= 2)
        {
            const newLatticeArray = isLatticeTextFormatTilted
                ? removeRowTopToLattice(latticeObject.lattice)
                : removeAllDirectionsToLattice(latticeObject.lattice);

            const newParams = [...latticeObject.circularParameters];
            newParams.splice(0, 1);
            
            setLatticeObjectWithHistory({
                ...latticeObject,
                lattice: newLatticeArray,
                circularParameters: newParams
            });
        }
    }, [isLatticeTextFormatTilted, latticeObject, setLatticeObjectWithHistory]);

    const addRowBottom = useCallback(() =>
    {
        const newLatticeArray = isLatticeTextFormatTilted
            ? addRowBottomToLattice(latticeObject.lattice)
            : addAllDirectionsToLattice(latticeObject.lattice);

        const newParams = [...latticeObject.circularParameters];
        newParams.push({
            r: 2*newParams[newParams.length-1].r - newParams[newParams.length-2].r,
            theta: 0
        });
        
        setLatticeObjectWithHistory({
            ...latticeObject,
            lattice: newLatticeArray,
            circularParameters: newParams
        });
    }, [isLatticeTextFormatTilted, latticeObject, setLatticeObjectWithHistory]);

    const removeRowBottom = useCallback(() =>
    {
        if (latticeObject.lattice.length >= 2)
        {
            const newLatticeArray = isLatticeTextFormatTilted
                ? removeRowBottomToLattice(latticeObject.lattice)
                : removeAllDirectionsToLattice(latticeObject.lattice);

            const newParams = [...latticeObject.circularParameters];
            newParams.splice(-1, 1);
            
            setLatticeObjectWithHistory({
                ...latticeObject,
                lattice: newLatticeArray,
                circularParameters: newParams
            });
        }
    }, [isLatticeTextFormatTilted, latticeObject, setLatticeObjectWithHistory]);

    const addRing = useCallback(() =>
    {
        let newLatticeArray: string[][] = [...latticeObject.lattice];
        const newParams = [...latticeObject.circularParameters];
        if (latticeType === LatticeType.CIRCLE)
        {
            newLatticeArray.push('0'.repeat(newLatticeArray[newLatticeArray.length-1].length).split(''));
            newParams.push({
                r: newParams[newParams.length-1].r + latticeParameters.elementDiameter,
                theta: 0
            });
        }
        else
        {
            newLatticeArray = addAllDirectionsToLattice(latticeObject.lattice);
            newParams.push({
                r: 2*newParams[newParams.length-1].r - newParams[newParams.length-2].r,
                theta: 0
            });
            newParams.push({
                r: 2*newParams[newParams.length-1].r - newParams[newParams.length-2].r,
                theta: 0
            });
        }
        setLatticeObjectWithHistory({
            ...latticeObject,
            lattice: newLatticeArray,
            circularParameters: newParams
        });
    }, [
        latticeObject, setLatticeObjectWithHistory,
        latticeParameters, latticeType
    ]);

    const removeRing = useCallback(() =>
    {
        if (
            latticeObject.lattice.length >= 3 &&
            latticeObject.lattice[0].length >= 3 &&
            latticeType !== LatticeType.CIRCLE
        ) {
            const newLatticeArray = removeAllDirectionsToLattice(latticeObject.lattice); 
            setLatticeObjectWithHistory({...latticeObject, lattice: newLatticeArray});
        }
        else if (latticeObject.lattice.length >= 2 && latticeType === LatticeType.CIRCLE)
        {
            const listR: number[] = latticeObject.circularParameters.map(ring => ring.r);
            const maxIdx: number = listR.indexOf(Math.max(...listR));
            
            const newLatticeArray = latticeObject.lattice;
            newLatticeArray.splice(maxIdx, 1);
            const newCircularParams = latticeObject.circularParameters;
            newCircularParams.splice(maxIdx, 1);
            
            setLatticeObjectWithHistory({
                ...latticeObject,
                lattice: newLatticeArray,
                circularParameters: newCircularParams
            });
        }
    }, [latticeObject, setLatticeObjectWithHistory, latticeType]);

    const getTileColor = useCallback((element: string): string =>
    {
        const colors = latticeObject.colorMap.filter(curr => curr.label === element);
        
        return(
            colors.length > 0
                ? colors[0].color
                : "#000000"
        );
    }, [latticeObject.colorMap]);

    const latticeTileOnClick = useCallback((element: string, row: number, col: number) =>
    {
        // Update
        setLatticeObject((curr: LatticeObject) => {
            if (!curr.colorMap.filter(curr => curr.isActivated).some(e => e))
            {
                return(curr as LatticeObject);
            }

            const labelsFiltered = curr.colorMap
                .filter((curr) => curr.isActivated)
                .map((curr) => curr.label);

            const idx = labelsFiltered.indexOf(element);
            const newLabel = !isDeleteMode
                ? labelsFiltered[(idx+1) % labelsFiltered.length]
                : 'toDelete'; // Tag element to delete
            
            const coords = getCoordinatesBySymmetry(
                row, col, curr.lattice, 
                symmetryNumber, latticeType, isSymmetryMode,
                latticeObject.circularParameters
            );
                
            let newLatticeArray = [...curr.lattice];
            let newParams = [...curr.circularParameters]
            
            coords.forEach((coord) => {
                newLatticeArray = replaceTileByIndexes(
                    newLatticeArray, coord[0], coord[1], newLabel
                );
            });

            if (isDeleteMode)
            {
                // Remove elements tagged toDelete
                newLatticeArray = newLatticeArray
                    .map(row => row.filter(e => e !== 'toDelete'));

                // Check empty lines
                const rowIdxRemoved = newLatticeArray
                    .map((row, idx) => row.length >= 1 ? -1 : idx)
                    .filter(row => row !== -1);

                newLatticeArray = newLatticeArray
                    .filter((_, idx) => !rowIdxRemoved.includes(idx));
                newParams = newParams
                    .filter((_, idx) => !rowIdxRemoved.includes(idx));

                if (rowIdxRemoved.includes(rowLabel-1))
                {
                    setRowLabel(-1);
                }
            }
            
            appendToStackHistory({
                ...curr,
                lattice: newLatticeArray,
                circularParameters: newParams
            });
            
            return({
                ...curr,
                lattice: newLatticeArray,
                circularParameters: newParams
            } as LatticeObject);
        });
    }, [setLatticeObject, isDeleteMode, symmetryNumber, latticeType, isSymmetryMode, latticeObject.circularParameters, appendToStackHistory, rowLabel]);

    const decreaseLatticeTileSize = useCallback(() =>
    {
        if (latticeParameters.scale > 0)
        {
            const newLatticeScale = latticeParameters.scale * 0.85;
            setLatticeParameters(curr => ({
                ...curr,
                scale: newLatticeScale
            }));

            // let newLatticeTileMargin = latticeTileMargin;
            // if (newLatticeTileSize < 20) {
            //     newLatticeTileMargin = 0;
            //     setLatticeTileMargin(newLatticeTileMargin);
            // }

            setCircleDiameter(circleDiameter.map((circleLine: CircleLine) => ({
                ...circleLine,
                circleSize: computeCircleSize(
                    circleLine.diameter,
                    newLatticeScale
                )
            })));
        }
    }, [latticeParameters, setCircleDiameter, circleDiameter, setLatticeParameters]);

    const increaseLatticeTileSize = useCallback(() =>
    {
        const newTileSize = Math.ceil(latticeParameters.pitch * latticeParameters.scale / 0.85);
        const newLatticeScale = newTileSize / latticeParameters.pitch;

        setLatticeParameters(curr => ({
            ...curr,
            scale: newLatticeScale
        }));

        // let newLatticeTileMargin = latticeTileMargin;
        // if (newLatticeTileSize >= 20) {
        //     newLatticeTileMargin = 1;
        //     setLatticeTileMargin(newLatticeTileMargin);
        // }
        
        setCircleDiameter(circleDiameter.map((circleLine: CircleLine) => ({
            ...circleLine,
            circleSize: computeCircleSize(
                circleLine.diameter,
                newLatticeScale
            )
        })));
    }, [latticeParameters, setCircleDiameter, circleDiameter, setLatticeParameters]);

    const resizeLatticeCanvas = useCallback(() =>
    {
        let latticeWidth: number = (latticeCanvasRef?.current?.offsetWidth || 100);

        if (latticeType === LatticeType.CIRCLE)
        {
            latticeWidth = 2 * latticeParameters.scale * Math.max(...latticeObject.circularParameters.map(curr => curr.r)) + 2*latticeParameters.tileSize;
        }

        const totalWidth: number = (latticeSectionRef?.current?.offsetWidth || 100) - 2*3.4*16;
        const totalHeight: number = (latticeSectionRef?.current?.offsetHeight || 100) - 2*3.4*16;
        
        const newLatticeScale = Math.ceil(latticeParameters.scale * Math.min(totalWidth, totalHeight)/latticeWidth);
        
        setLatticeParameters(curr => ({
            ...curr,
            scale: newLatticeScale
        }));
        
        setCircleDiameter(circleDiameter.map((circleLine: CircleLine) => ({
            ...circleLine,
            circleSize: computeCircleSize(
                circleLine.diameter,
                newLatticeScale
            )
        })));
    }, [
        latticeParameters, setLatticeParameters, circleDiameter,
        setCircleDiameter, latticeType, latticeObject.circularParameters
    ]);

    const setRowColumnLabel = useCallback((row: number, col: number) => {
        setRowLabel(row+1);
        setColLabel(col+1);
    }, []);

    // Experimental: lattice elements permutations (works in symmetry mode)
    const [elementDrag, setElementDrag] = useState({row: 0, column: 0, element: ""});

    const dragstartHandler = useCallback((
        event: React.MouseEvent<HTMLButtonElement>,
        row: number,
        column: number,
        element: string
    ) => {
        if (event.ctrlKey) {
            // First store the source element to permut
            setElementDrag((curr) => ({
                ...curr, row: row, column: column, element: element
            }));
        }
    }, []);

    const dragendHandler = useCallback((
        event: React.MouseEvent<HTMLButtonElement>,
        row: number,
        column: number,
        element: string
    ) => {
        if (event.ctrlKey)
        {
            // let listOfElementIdBefore = [];
            // let listOfElementIdAfter = [];
            setLatticeObject((curr: LatticeObject) => {

                let newLatticeArray: string[][] = [...curr.lattice];

                const coordsAfter = getCoordinatesBySymmetry(
                    row, column, curr.lattice, 
                    symmetryNumber, latticeType, isSymmetryMode,
                    curr.circularParameters
                );
                const coordsBefore = getCoordinatesBySymmetry(
                    elementDrag.row, elementDrag.column, curr.lattice, 
                    symmetryNumber, latticeType, isSymmetryMode,
                    curr.circularParameters
                );
                
                coordsBefore.forEach((coord) => {
                    newLatticeArray = replaceTileByIndexes(
                        newLatticeArray, coord[0], coord[1], element
                    );
                });
                coordsAfter.forEach((coord) => {
                    newLatticeArray = replaceTileByIndexes(
                        newLatticeArray, coord[0], coord[1], elementDrag.element
                    );
                });

                appendToStackHistory({...curr, lattice: newLatticeArray});

                return({...curr, lattice: newLatticeArray} as LatticeObject);
            });


            // Create the permuation arrows in the SVG
            // createPermuationArrowsInSVG(listOfElementIdBefore, listOfElementIdAfter);
        }
    }, [
        appendToStackHistory, elementDrag, isSymmetryMode, latticeType, setLatticeObject, symmetryNumber
    ]);

    // const latticeWindowRef = useRef<HTMLDivElement | null>(null);
    // const svgRef = useRef<SVGAElement | null>(null);

    /*function createPermuationArrowsInSVG(listOfElementIdBefore, listOfElementIdAfter)
    {
        // Avoid permutation for element ifself
        if (listOfElementIdAfter.every((e, idx) => listOfElementIdBefore[idx] === e)) {
            return;
        }
        const svgns = "http://www.w3.org/2000/svg";

        // append the new rectangle to the svg
        const latticeSectionBox = latticeWindowRef?.current?.getBoundingClientRect();

        for (let i = 0; i < listOfElementIdBefore.length; i++)
        {
            const elementBeforeDiv = document.getElementById(listOfElementIdBefore[i]).getBoundingClientRect();
            const elementDiv = document.getElementById(listOfElementIdAfter[i]).getBoundingClientRect();
            
            const line = document.createElementNS(svgns, "line");
            line.setAttribute("x1", String(elementDiv.left + elementDiv.width/2 - latticeSectionBox?.left));
            line.setAttribute("y1", String(elementDiv.top + elementDiv.height/2 - latticeSectionBox?.top));
            line.setAttribute("x2", String(elementBeforeDiv.left + elementBeforeDiv.width/2 - latticeSectionBox?.left));
            line.setAttribute("y2", String(elementBeforeDiv.top + elementBeforeDiv.height/2 - latticeSectionBox?.top));
            line.setAttribute("stroke", "black");
            line.setAttribute("stroke-width", "4");
            line.setAttribute("marker-end", "url(#head)");

            // Append the style element to the SVG element
            svgRef?.current?.appendChild(line);
        }
    }*/
    
    return (
        <section
            ref={latticeSectionRef}
            className="lattice-section"
            style={latticeSectionStyle}
        >
            <LatticeToolbar
                latticeType={latticeType}
                latticeCanvasRef={latticeCanvasRef}
                isSymmetryMode={isSymmetryMode}
                setIsSymmetryMode={callSetIsSymmetryMode}
                isCircleHelperDisplay={isCircleHelperDisplay}
                setIsCircleHelperDisplay={callSetIsCircleHelperDisplay}
                symmetryNumber={symmetryNumber}
                setSymmetryNumber={callSetSymmetryNumber}
                isShowRowColumnNames={isShowRowColumnNames}
                setIsShowRowColumnNames={callSetIsShowRowColumnNames}
                isShowTileLabel={isShowTileLabel}
                setIsShowTileLabel={callSetIsShowTileLabel}
                isBrushMode={isBrushMode}
                setIsBrushMode={callSetIsBrushMode}
                isDeleteMode={isDeleteMode}
                setIsDeleteMode={callSetIsDeleteMode}
                isGreyScale={isGreyScale}
                setIsGreyScale={callSetIsGreyScale}
            />

            <div
                className="lattice"
                // ref={latticeWindowRef}
            >
                {/* <svg id="lattice-svg" className="lattice-section-svg-arrows">
                    <defs>
                        <marker 
                            id='head' 
                            orient="auto" 
                            markerWidth='3' 
                            markerHeight='4' 
                            refX='0.1' 
                            refY='2'
                        >
                            <path d='M0,0 V4 L2,2 Z' fill="black" />
                        </marker>
                    </defs>
                </svg> */}
    
                <div className="size-container">
                    <Button
                        className="button-no-background"
                        onClick={decreaseLatticeTileSize}
                        title="Zoom out the lattice"
                    >
                        zoom_out
                    </Button>
                    <Button
                        className="button-no-background"
                        onClick={increaseLatticeTileSize}
                        title="Zoom in the lattice"
                    >
                        zoom_in
                    </Button>
                    <Button
                        className="button-no-background"
                        onClick={resizeLatticeCanvas}
                        title="Zoom in the lattice to the maximum size"
                    >
                        zoom_out_map
                    </Button>
                </div>
                
                <Button 
                    className="lattice-button lattice-full-screen display-large"
                    title={isFullscreen ? "Exit fullscreen" : "Fullscreen"}
                    onClick={setIsFullscreen}
                >
                    {isFullscreen ? "fullscreen_exit" : "fullscreen"}
                </Button>

                {isSymmetryFlag && (
                    <img
                        className="lattice-symmetry-flag"
                        src={symmetryIcon}
                        alt="Your lattice is symmetric (barycenter)"
                        title="Your lattice is symmetric (barycenter)"
                    />
                )}

                <img
                    className="lattice-orientation-icon"
                    style={{transform: "rotate("+latticeCoordOrientation+"deg)"}}
                    src={orientationIcon}
                    alt="Lattice orientation"
                    title="Lattice orientation"
                />

                <p className="lattice-size-text">
                    {latticeSizeStats}
                </p>
    
                {latticeType !==  LatticeType.CIRCLE && (
                    <div className="lattice-button-left">
                        <Button
                            className="lattice-button"
                            onClick={addColLeft}
                            title="Add left column"
                        >
                            add
                        </Button>
                        <Button
                            className="lattice-button"
                            onClick={removeColLeft}
                            title="Remove left column"
                        >
                            remove
                        </Button>
                    </div>
                )}
                <div className={
                    latticeType === LatticeType.CIRCLE
                        ? "lattice-button-top"
                        : "lattice-button-top-extended"
                    }>
                    <Button
                        className="lattice-button"
                        onClick={removeRing}
                        title="Remove a ring of element"
                        >
                        remove_circle
                    </Button>
                    {latticeType !==  LatticeType.CIRCLE && (
                        <Button
                            className="lattice-button"
                            onClick={removeRowTop}
                            title="Remove top row"
                        >
                            remove
                        </Button>
                    )}
                    {latticeType !==  LatticeType.CIRCLE && (
                        <Button
                            className="lattice-button"
                            onClick={addRowTop}
                            title="Add top row"
                        >
                            add
                        </Button>
                    )}
                    <Button
                        className="lattice-button"
                        onClick={addRing}
                        title="Add a ring of element"
                    >
                        add_circle
                    </Button>
                </div>
                
                {latticeType !==  LatticeType.CIRCLE && (
                    <div className="lattice-button-bottom">
                        <Button
                            className="lattice-button"
                            onClick={removeRowBottom}
                            title="Remove bottom row"
                        >
                            remove
                        </Button>
                        <Button
                            className="lattice-button"
                            onClick={addRowBottom}
                            title="Add bottom row"
                        >
                            add
                        </Button>
                    </div>
                )}
                {latticeType !==  LatticeType.CIRCLE && (
                    <div className="lattice-button-right">
                        <Button
                            className="lattice-button"
                            onClick={addColRight}
                            title="Add right column"
                        >
                            add
                        </Button>
                        <Button
                            className="lattice-button"
                            onClick={removeColRight}
                            title="Remove right column"
                        >
                            remove
                        </Button>
                    </div>
                )}

                {latticeType !== LatticeType.CIRCLE ? (
                    <div
                        ref={latticeCanvasRef}
                        // className="lattice-canvas"
                        onMouseOut={() => setRowColumnLabel(-1, -1)}
                    >
                        {latticeArrayWithLabel.map((line, row) => (
                            <LatticeRow 
                                key={row}
                                row={row}
                                latticeType={latticeType}
                                latticeTileSize={latticeParameters.tileSize}
                                latticeTileMargin={latticeParameters.tileMargin}
                            >
                                {line.map((element, col) => (
                                    <LatticeTile
                                        key={col}
                                        element={element}
                                        row={row-1}
                                        col={col-1}
                                        latticeTileSize={latticeParameters.tileSize}
                                        latticeTileMargin={latticeParameters.tileMargin}
                                        backgroundColor={getTileColor(element)}
                                        latticeType={latticeType}
                                        fontSizeScale={fontSizeScale}
                                        isCircle={latticeParameters.isElementCircle}
                                        isHover={listCoordOver.filter(curr => curr[0] === row-1 && curr[1] === col-1).length > 0}
                                        isBrushMode={isBrushMode}
                                        isShowLabel={isShowTileLabel}
                                        isRowColumnLabel={
                                            isShowRowColumnNames
                                            &&
                                            (
                                                (row === 0 && col >= 1 && col < latticeArrayWithLabel[row].length-1)
                                                ||
                                                (col === 0 && row >= 1 && row < latticeArrayWithLabel.length-1)
                                            )
                                        }
                                        isEmpty={
                                            (!isShowRowColumnNames && (row === 0 || col === 0)) || 
                                            (row === 0 && col === 0) ||
                                            row === latticeArrayWithLabel.length-1 ||
                                            col === latticeArrayWithLabel[row].length-1
                                        }
                                        isBorder={isBorder}
                                        isGreyScale={isGreyScale}
                                        onClick={latticeTileOnClick}
                                        onMouseOver={setRowColumnLabel}
                                        onMouseDown={dragstartHandler}
                                        onMouseUp={dragendHandler}
                                    />
                                ))}
                            </LatticeRow>
                        ))}
                    </div>
                ) : (
                    <div ref={latticeCanvasRef} className="lattice-canvas">
                        <LatticeCircularAddButtons
                            latticeObject={latticeObject}
                            latticeParameters={latticeParameters}
                            isPlaceRight={true}
                        />
                        <LatticeCircularAddButtons
                            latticeObject={latticeObject}
                            latticeParameters={latticeParameters}
                            isPlaceLeft={true}
                        />
                        {latticeObject.lattice.map((ring, ringIdx) => (
                            <div key={ringIdx}>
                                {ring.map((element, angleIdx) => (
                                    <LatticeTile
                                        key={angleIdx}
                                        element={element}
                                        row={ringIdx}
                                        col={angleIdx}
                                        title={numberToLetter(ringIdx)+String(angleIdx+1)}
                                        xCoord={-latticeParameters.scale * (latticeObject.circularParameters[ringIdx]?.r || 0) * Math.cos(2*Math.PI * angleIdx/latticeObject.lattice[ringIdx].length + Math.PI/180 * (latticeObject.circularParameters[ringIdx]?.theta || 0))}
                                        yCoord={+latticeParameters.scale * (latticeObject.circularParameters[ringIdx]?.r || 0) * Math.sin(2*Math.PI * angleIdx/latticeObject.lattice[ringIdx].length + Math.PI/180 * (latticeObject.circularParameters[ringIdx]?.theta || 0))}
                                        latticeTileSize={latticeParameters.tileSize}
                                        latticeTileMargin={latticeParameters.tileMargin}
                                        backgroundColor={getTileColor(element)}
                                        latticeType={latticeType}
                                        fontSizeScale={fontSizeScale}
                                        isCircle={true}
                                        isHover={listCoordOver.filter(curr => curr[0] === ringIdx && curr[1] === angleIdx).length > 0}
                                        isBrushMode={isBrushMode}
                                        isShowLabel={isShowTileLabel}
                                        isRowColumnLabel={isShowRowColumnNames}
                                        isEmpty={false}
                                        isBorder={isBorder}
                                        isGreyScale={isGreyScale}
                                        onClick={latticeTileOnClick}
                                        onMouseOver={setRowColumnLabel}
                                        onMouseDown={dragstartHandler}
                                        onMouseUp={dragendHandler}
                                    />
                                ))}
                            </div>
                        ))}
                    </div>
                )}
    
                {isCircleHelperDisplay && (
                    [...circleDiameter].sort((prev, curr) => curr.diameter - prev.diameter)
                        .map((circleLine, idx) => (
                        <LatticeCircle
                            key={idx}
                            isDisplay={circleLine.isDisplay}
                            latticeCircleSize={circleLine.circleSize}
                            circleDiameter={circleLine.diameter}
                            lineLengthCoeff={circleLine.length}
                            color={circleLine.color}
                            isAddLineRadius={isShowCircleDimensions}
                            rotationAngle={idx * 10 + 15}
                        />
                    ))
                )}
            </div>
        </section>
    );
}

// ************************************************************************** //
