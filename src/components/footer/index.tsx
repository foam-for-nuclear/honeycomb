/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import './styles.css';
import honeycombLogo from "../../assets/honeycomb-logo.svg";

export function Footer()
{
    return (
        <footer className="footer-container">
            <img src={honeycombLogo} alt="Logo"/>
            <div className="footer-container-text">
                <p>
                    Managed by the <a href="https://gitlab.com/foam-for-nuclear/honeycomb">Foam-For-Nuclear community</a>.
                </p>
                <p>
                    A user-friendly web tool to generate, edit, visualize and
                    translate lattice maps usually used in nuclear codes and
                    softwares such as <a href="https://mcnp.lanl.gov/">MCNP</a>
                    , <a href="https://serpent.vtt.fi/serpent/">Serpent</a>
                    , <a href="https://openmc.org/">OpenMC</a>
                    , and <a href="https://www.studsvik.com/key-offerings/nuclear-simulation-software/software-products/casmo5/">CASMO</a>.
                </p>
                <p>
                    Copyright (C) (2023-2024) EPFL, link to <a href="https://gitlab.com/foam-for-nuclear/honeycomb/-/blob/main/LICENSE?ref_type=heads">LICENCE</a>.
                </p>
            </div>
        </footer>
    );
}

// ************************************************************************** //
