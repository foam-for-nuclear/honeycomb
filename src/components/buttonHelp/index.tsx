/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { memo } from 'react';
import { useComponentVisible } from '../../hooks/useComponentVisible';
import './styles.css';

function ButtonHelp({
    children
}: {
    children: React.ReactElement[]
}) {
    const {ref, isComponentVisible, setIsComponentVisible} = useComponentVisible(false);
    

    return (
        <div ref={ref} className='button-help-button-container'>
            <button
                className="button-help-button"
                title="Description"
                onClick={() => setIsComponentVisible(curr => !curr)}
            >
                <span className="material-symbols-outlined">
                    question_mark
                </span>
            </button>

            {isComponentVisible && (
                <div className='button-help-dropdown'>
                    {children}
                </div>
            )}
        </div>
    );
}

const memoButton = memo(ButtonHelp);

export { memoButton as ButtonHelp };

// ************************************************************************** //
