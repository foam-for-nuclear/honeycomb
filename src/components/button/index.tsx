/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import React, { memo, useMemo } from 'react';
import './styles.css';


interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    isFilled?: boolean,
    isActivated?: boolean,
    rotate?: string
}

function Button({
    isFilled = false,
    isActivated = false,
    rotate = "0deg",
    ...props
}: ButtonProps) {
    const title: string = useMemo(() => (
        (props.title || '') +
        ' ' + 
        (isActivated ? '(Activated)' : '')
    ), [isActivated, props.title]);

    return (
        <button {...props}
            title={title}
            style={{
                background: isActivated ? "var(--blue)" : props.style?.background,
                color: isActivated ? "white" : props.style?.color
            }}
        >
            <span
                className="material-symbols-outlined"
                style={{
                    fontVariationSettings: "'FILL'" + (isFilled ? "1" : "0"),
                    transform: "rotate("+rotate+")"
                }}
            >
                {props.children}
            </span>
        </button>
    );
}

const memoButton = memo(Button);

export { memoButton as Button };

// ************************************************************************** //
