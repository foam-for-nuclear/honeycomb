/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { RefObject, useCallback, useContext, useMemo } from "react";
import { Button } from "../button";
import { Dropdown } from "../dropdown";
import { getFontEmbedCSS, toPng, toSvg } from 'html-to-image';
// import html2canvas from "html2canvas";

import './styles.css';
import { LatticeType, LatticeContext, LatticeTextFormat, CircularParameter } from "../../context/latticeContext";
import {
    flipHexagonalLattice,
    flipHorizontalSquareLattice,
    flipVerticalSquareLattice,
    isFirstColumnZeros,
    isFirstRowZeros,
    isLastColumnZeros,
    isLastRowZeros,
    isLatticeEmpty,
    isLatticeHexagonSymmetricHorizontally,
    isLatticeHexagonSymmetricRotationLeft,
    isLatticeHexagonSymmetricRotationRight,
    isLatticeHexagonSymmetricUpLeft,
    isLatticeHexagonSymmetricUpRight,
    isLatticeHexagonSymmetricVertically,
    isLatticeSquareSymmetricHorizontally,
    isLatticeSquareSymmetricRotationLeft,
    isLatticeSquareSymmetricRotationRight,
    isLatticeSquareSymmetricVertically,
    removeColLeftToLattice,
    removeColRightToLattice,
    removeRowBottomToLattice,
    removeRowTopToLattice,
    rotateHexagonalLattice,
    rotateLeftSquareLattice,
    rotateRightSquareLattice
} from "../../lib/latticeEdition";

interface LatticeToolbarProps {
    latticeType: LatticeType,
    latticeCanvasRef: RefObject<HTMLDivElement>,
    isSymmetryMode: boolean,
    setIsSymmetryMode: () => void,
    symmetryNumber: number,
    setSymmetryNumber: (value: number) => void,
    isCircleHelperDisplay: boolean,
    setIsCircleHelperDisplay: () => void
    isShowRowColumnNames: boolean,
    setIsShowRowColumnNames: () => void,
    isShowTileLabel: boolean,
    setIsShowTileLabel: () => void,
    isBrushMode: boolean,
    setIsBrushMode: () => void,
    isDeleteMode: boolean,
    setIsDeleteMode: () => void,
    isGreyScale: boolean,
    setIsGreyScale: () => void
}


export function LatticeToolbar({
    latticeCanvasRef,
    latticeType,
    isSymmetryMode,
    setIsSymmetryMode,
    symmetryNumber,
    setSymmetryNumber,
    isCircleHelperDisplay,
    setIsCircleHelperDisplay,
    isShowRowColumnNames,
    setIsShowRowColumnNames,
    isShowTileLabel,
    setIsShowTileLabel,
    isBrushMode,
    setIsBrushMode,
    isDeleteMode,
    setIsDeleteMode,
    isGreyScale,
    setIsGreyScale
}:LatticeToolbarProps) {
    const {
        latticeObject,
        setLatticeObjectWithHistory,
        setLatticeType,
        setLatticeTextFormat,
        undo,
        redo
    } = useContext(LatticeContext);

    const isLatticeSymmetricRotateLeft = useMemo(() =>
        latticeType === LatticeType.SQUARE
        ? isLatticeSquareSymmetricRotationLeft(latticeObject.lattice)
        : isLatticeHexagonSymmetricRotationLeft(latticeObject.lattice)
    , [latticeObject.lattice, latticeType]);
    
    const isLatticeSymmetricRotateRight = useMemo(() =>
        latticeType === LatticeType.SQUARE
        ? isLatticeSquareSymmetricRotationRight(latticeObject.lattice)
        : isLatticeHexagonSymmetricRotationRight(latticeObject.lattice)
    , [latticeObject.lattice, latticeType]);

    
    const htmlToImageConvertPNG = async () => {
        const element = latticeCanvasRef?.current as HTMLElement;
        const fontEmbedCss = await getFontEmbedCSS(element);
        toPng(element, { cacheBust: false, fontEmbedCSS: fontEmbedCss })
            .then((dataUrl) => {
                const link = document.createElement("a");
                link.download = "lattice.png";
                link.href = dataUrl;
                link.click();
            });
            // .catch((err) => {
            //     console.log(err);
            // });

        // Not working for Hexagonal lattices, html2canvas is not supporting 
        // clip-path feature from CSS
        // const canvasPromise = html2canvas(latticeCanvasRef?.current as HTMLElement, {
        //     useCORS: true // in case you have images stored in your application
        // });
        // canvasPromise.then((canvas)=> {
        //     const dataURL = canvas.toDataURL("lattice.png");
        //     // Create a link element
        //     const a = document.createElement("a");
        //     a.href = dataURL;
        //     a.download = "lattice.png";
        //     a.click();
        // });
    };
    
    // const htmlToImageConvertJPEG = () => {
    //     toJpeg(latticeCanvasRef?.current as HTMLElement, { cacheBust: false })
    //         .then((dataUrl) => {
    //             const link = document.createElement("a");
    //             link.download = "lattice.jpeg";
    //             link.href = dataUrl;
    //             link.click();
    //         })
    //         .catch((err) => {
    //             console.log(err);
    //         });
    // };
    
    const htmlToImageConvertSVG = () => {
        toSvg(latticeCanvasRef?.current as HTMLElement, { cacheBust: false })
            .then((dataUrl) => {
                const link = document.createElement("a");
                link.download = "lattice.svg";
                link.href = dataUrl;
                link.click();
            })
            .catch((err) => {
                console.log(err);
            });
    };


    const cropLattice = useCallback(() =>
    {
        if (isLatticeEmpty(latticeObject.lattice) && latticeType !== LatticeType.CIRCLE)
        {
            return;
        }
            
        let newLatticeArray: string[][] = [...latticeObject.lattice];
        const newCircularParams: CircularParameter[] = [...latticeObject.circularParameters];
        
        if (latticeType === LatticeType.CIRCLE)
        {
            for (let iter = 0; iter < 100; iter++)
            {
                if (newLatticeArray[newLatticeArray.length-1].every(curr => curr === '0'))
                {
                    newLatticeArray.splice(newLatticeArray.length-1, 1);
                    newCircularParams.splice(newCircularParams.length-1, 1);
                }
                else {
                    break;
                }
            }
        }
        else
        {
            for (let iter = 0; iter < 100; iter++)
            {
                if (isFirstRowZeros(newLatticeArray))
                {
                    newLatticeArray = removeRowTopToLattice(newLatticeArray);
                    newCircularParams.splice(0, 1);
                }
                else if (isLastRowZeros(newLatticeArray))
                {
                    newLatticeArray = removeRowBottomToLattice(newLatticeArray);
                    newCircularParams.splice(-1, 1);
                }
                else if (isFirstColumnZeros(newLatticeArray))
                {
                    newLatticeArray = removeColLeftToLattice(newLatticeArray);
                }
                else if (isLastColumnZeros(newLatticeArray))
                {
                    newLatticeArray = removeColRightToLattice(newLatticeArray);
                }
                else
                {
                    break;
                }
            }
        }

        // Render at the end
        setLatticeObjectWithHistory({
            ...latticeObject,
            lattice: newLatticeArray,
            circularParameters: newCircularParams
        });
    }, [latticeObject, setLatticeObjectWithHistory, latticeType]);

    const rotateLatticeLeft = useCallback(() =>
    {
        let newLatticeArray: string[][] = [];
        if (latticeType === LatticeType.HEXAGON) {
            newLatticeArray = rotateHexagonalLattice(latticeObject.lattice, 2*Math.PI/6);
            setLatticeObjectWithHistory({...latticeObject, lattice: newLatticeArray});
        }
        else if (latticeType === LatticeType.SQUARE) {
            newLatticeArray = rotateLeftSquareLattice(latticeObject.lattice);
        }
        setLatticeObjectWithHistory({...latticeObject, lattice: newLatticeArray});
    }, [latticeObject, latticeType, setLatticeObjectWithHistory]);

    const rotateLatticeRight = useCallback(() =>
    {
        let newLatticeArray: string[][] = [];
        if (latticeType === LatticeType.HEXAGON) {    
            newLatticeArray = rotateHexagonalLattice(latticeObject.lattice, -2*Math.PI/6);
        } 
        else if (latticeType === LatticeType.SQUARE) {
            newLatticeArray = rotateRightSquareLattice(latticeObject.lattice);
        }
        setLatticeObjectWithHistory({...latticeObject, lattice: newLatticeArray});
    }, [latticeObject, latticeType, setLatticeObjectWithHistory]);

    const flipLatticeVertically = useCallback(() =>
    {
        let newLatticeArray: string[][] = [];
        if (latticeType === LatticeType.HEXAGON) {
            newLatticeArray = flipHexagonalLattice(latticeObject.lattice, 0);
        }
        else if (latticeType === LatticeType.SQUARE) {
            newLatticeArray = flipVerticalSquareLattice(latticeObject.lattice);
        }
        setLatticeObjectWithHistory({...latticeObject, lattice: newLatticeArray});
    }, [latticeObject, latticeType, setLatticeObjectWithHistory]);

    const flipLatticeHorizontally = useCallback(() =>
    {
        let newLatticeArray: string[][] = [];
        if (latticeType === LatticeType.HEXAGON)
        {
            newLatticeArray = flipHexagonalLattice(latticeObject.lattice, Math.PI/2);
        }
        else if (latticeType === LatticeType.SQUARE)
        {
            newLatticeArray = flipHorizontalSquareLattice(latticeObject.lattice);
        }
        setLatticeObjectWithHistory({...latticeObject, lattice: newLatticeArray});
    }, [latticeObject, latticeType, setLatticeObjectWithHistory]);

    const flipLatticeHexagonUpLeft = useCallback(() =>
    {
        const newLatticeArray = flipHexagonalLattice(latticeObject.lattice, 2*Math.PI/3);
        
        setLatticeObjectWithHistory({...latticeObject, lattice: newLatticeArray});
    }, [latticeObject, setLatticeObjectWithHistory]);

    const flipLatticeHexagonUpRight = useCallback(() =>
    {
        const newLatticeArray = flipHexagonalLattice(latticeObject.lattice, Math.PI/3);
        
        setLatticeObjectWithHistory({...latticeObject, lattice: newLatticeArray});
    }, [latticeObject, setLatticeObjectWithHistory]);

    
    const undoRedoControlKeyboard = useCallback((event: KeyboardEvent) =>
    {
        if (event.key === "Z" && event.ctrlKey && event.shiftKey)
        {
            event.preventDefault();
            redo();
        }
        else if (event.key === "z" && event.ctrlKey)
        {
            event.preventDefault();
            undo();
        }
    }, [redo, undo]);
    document.onkeydown = undoRedoControlKeyboard;
    

    return (
        <div className="lattice-toolbar">
            <div className="display-row">
                <Button
                    className="button-no-background"
                    title="Hexagonal lattice"
                    isFilled={latticeType === LatticeType.HEXAGON}
                    onClick={() => {
                        setLatticeType(LatticeType.HEXAGON);
                        setLatticeTextFormat(LatticeTextFormat.TILTED);
                    }}
                >
                    hexagon
                </Button>
                <Button
                    className="button-no-background"
                    title="Square lattice"
                    isFilled={latticeType === LatticeType.SQUARE}
                    onClick={() => {
                        setLatticeType(LatticeType.SQUARE);
                        setLatticeTextFormat(LatticeTextFormat.TILTED);
                    }}
                >
                    square
                </Button>
                {/* <Button
                    className="button-no-background"
                    title="Triangular lattice"
                    isFilled={latticeType === LatticeType.TRIANGULAR}
                    onClick={() => setLatticeType(LatticeType.TRIANGULAR)}
                >
                    change_history
                </Button> */}
                <Button
                    className="button-no-background"
                    title="Circular lattice"
                    isFilled={latticeType === LatticeType.CIRCLE}
                    onClick={() => {
                        setLatticeType(LatticeType.CIRCLE);
                        setLatticeTextFormat(LatticeTextFormat.CIRCULAR);
                    }}
                >
                    circle
                </Button>
            </div>
            
            <Button
                className="button-no-background"
                onClick={cropLattice}
                title="Crop the lattice (remove external lines and columns of 0)"
            >
                crop
            </Button>

            {latticeType !== LatticeType.CIRCLE && (
                <div className="display-row">
                    <Button
                        className="button-no-background"
                        onClick={rotateLatticeLeft}
                        title={
                            isLatticeSymmetricRotateLeft
                            ? "Lattice already symmetric by rotation"
                            :
                            "Rotate left"
                        }
                        disabled={isLatticeSymmetricRotateLeft}
                    >
                        rotate_left
                    </Button>
                    <Button
                        className="button-no-background"
                        onClick={rotateLatticeRight}
                        title={
                            isLatticeSymmetricRotateRight
                            ? "Lattice already symmetric by rotation"
                            :
                            "Rotate right"
                        }
                        disabled={isLatticeSymmetricRotateRight}
                    >
                        rotate_right
                    </Button>
                </div>
            )}
            {latticeType !== LatticeType.CIRCLE && (
                <div className="display-row">
                    <Button
                        className="button-no-background"
                        onClick={flipLatticeVertically}
                        title="Flip vertically"
                        rotate="90deg"
                        disabled={
                            latticeType === LatticeType.SQUARE
                            ? isLatticeSquareSymmetricVertically(latticeObject.lattice)
                            : isLatticeHexagonSymmetricVertically(latticeObject.lattice)
                        }
                    >
                        flip
                    </Button>
                    <Button
                        className="button-no-background"
                        onClick={flipLatticeHorizontally}
                        title="Flip horizontally"
                        disabled={
                            latticeType === LatticeType.SQUARE
                            ? isLatticeSquareSymmetricHorizontally(latticeObject.lattice)
                            : isLatticeHexagonSymmetricHorizontally(latticeObject.lattice)
                        }
                    >
                        flip
                    </Button>
                    {latticeType === LatticeType.HEXAGON && (
                        <Button
                            id="lattice-button-flip-up-left"
                            className="button-no-background"
                            onClick={flipLatticeHexagonUpLeft}
                            title="Flip up-left"
                            rotate="30deg"
                            disabled={isLatticeHexagonSymmetricUpLeft(latticeObject.lattice)}
                        >
                            flip
                        </Button>
                    )}
                    {latticeType === LatticeType.HEXAGON && (
                        <Button
                            id="lattice-button-flip-up-right"
                            className="button-no-background"
                            onClick={flipLatticeHexagonUpRight}
                            title="Flip up-right"
                            rotate="150deg"
                            disabled={isLatticeHexagonSymmetricUpRight(latticeObject.lattice)}
                        >
                            flip
                        </Button>
                    )}
                </div>
            )}


            <div className="display-row">
                <Button
                    className="button-no-background"
                    title="Activate symmetry mode"
                    onClick={setIsSymmetryMode}
                    isActivated={isSymmetryMode}
                >
                    hub
                </Button>
                <input
                    disabled={!isSymmetryMode}
                    type="number"
                    className="symmetry-number"
                    maxLength={2}
                    value={symmetryNumber}
                    min={1}
                    max={100}
                    step={1}
                    onChange={(e) => setSymmetryNumber(parseInt(e.target.value))}
                />
            </div>

            <Button
                className="button-no-background"
                title="Display row and column names"
                isActivated={isShowRowColumnNames}
                onClick={setIsShowRowColumnNames}
            >
                format_list_numbered
            </Button>

            <Button
                className="button-no-background"
                title="Display tile labels"
                isActivated={!isShowTileLabel}
                onClick={setIsShowTileLabel}
            >
                format_clear
            </Button>
            
            <Button
                className="button-no-background"
                title="Change color to grey scale"
                isActivated={isGreyScale}
                onClick={setIsGreyScale}
            >
                invert_colors
            </Button>

            <Button
                className="button-no-background display-large"
                title="Change lattice tile with brush, use shift key to paint"
                isActivated={isBrushMode}
                onClick={setIsBrushMode}
            >
                brush
            </Button>

            {latticeType === LatticeType.CIRCLE && (
                <Button
                    className="button-no-background display-large"
                    title="Delete element on click, can be used with brush mode and symmetry mode"
                    isActivated={isDeleteMode}
                    onClick={setIsDeleteMode}
                >
                    cancel
                </Button>
            )}

            <Button
                className="button-no-background"
                title="Display circle helper (see Parameters and Options)"
                onClick={setIsCircleHelperDisplay}
                isActivated={isCircleHelperDisplay}
            >
                adjust
            </Button>

            <Dropdown
                logo="download"
                titleButton="Save your lattice as image"
                classNameButton="button-no-background"
                isLogo={true}
            > 
                <button
                    title="Export lattice to PNG image"
                    onClick={htmlToImageConvertPNG}
                >
                    PNG
                </button>
                {/* <button
                    title="Export lattice to JPEG image"
                    onClick={htmlToImageConvertJPEG}
                >
                    JPEG
                </button> */}
                <button
                    title="Export lattice to SVG image"
                    onClick={htmlToImageConvertSVG}
                >
                    SVG
                </button>
            </Dropdown>
            
            <div className="display-row">
                <Button
                    className="button-no-background"
                    onClick={undo}
                    title="Undo to the previous lattice (ctrl+z)"
                >
                    undo
                </Button>
                <Button
                    className="button-no-background"
                    onClick={redo}
                    title="Redo to the previous lattice (ctrl+shift+z)"
                >
                    redo
                </Button>
            </div>
        </div>
    );
}

// ************************************************************************** //
