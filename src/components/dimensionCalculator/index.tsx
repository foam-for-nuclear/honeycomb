/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { memo, useCallback, useContext, useEffect, useState } from 'react';
import './styles.css';
import { LatticeContext, LatticeParameterObject, LatticeType, PhysicalParameter } from '../../context/latticeContext';
import { 
    crossingSection, crossingSection_r, getLatticeTypeAsString, hydraulicDiameter,
    hydraulicDiameter_r, structureArea,
    structureArea_r, totalArea, totalArea_r, volumeFractionStructure,
    volumeFractionStructure_r, volumetricArea, volumetricArea_r, wetPerimeter,
    wetPerimeter_r
} from '../../lib/mathFunctions';
import { Checkbox } from '../checkbox';
import { Dropdown } from '../dropdown';
import { ButtonHelp } from '../buttonHelp';
import 'katex/dist/katex.min.css';
import { BlockMath, InlineMath } from 'react-katex';


interface DimensionCalculatorProps {
    latticeParameters: LatticeParameterObject,
    latticeType: LatticeType
}

function DimensionCalculator({
    latticeParameters,
    latticeType
}: DimensionCalculatorProps) {
    const { setLatticeParameters } = useContext(LatticeContext);

    const [unit, setUnit] = useState('mm');

    const [Pm, setPm] = useState(wetPerimeter(latticeParameters.elementDiameter, latticeType));
    const [Ap, setAp] = useState(structureArea(latticeParameters.elementDiameter, latticeType));
    const [At, setAt] = useState(totalArea(latticeParameters.pitch, latticeType));
    const [Sp, setSp] = useState(crossingSection(At, Ap));
    const [fs, setFs] = useState(volumeFractionStructure(Ap, At));
    const [Av, setAv] = useState(volumetricArea(Pm, At));
    const [Dh, setDh] = useState(hydraulicDiameter(Sp, Pm));

    useEffect(() => {
        const newPm: number = wetPerimeter(latticeParameters.elementDiameter, latticeType);
        const newAp: number = structureArea(latticeParameters.elementDiameter, latticeType);
        const newAt: number = totalArea(latticeParameters.pitch, latticeType);
        const newSp: number = crossingSection(newAt, newAp);
        setPm(newPm);
        setAp(newAp);
        setAt(newAt);
        setSp(newSp);
        setAv(parseFloat(volumetricArea(newPm, newAt).toFixed(6)));
        setFs(parseFloat(volumeFractionStructure(newAp, newAt).toFixed(6)));
        setDh(parseFloat(hydraulicDiameter(newSp, newPm).toFixed(6)));
    }, [latticeParameters.pitch, latticeParameters.elementDiameter, latticeType]);

    
    useEffect(() => {
        setUnit('cm');
    }, [latticeType]);

    
    const onChangeLatticePitch = useCallback((newPitch: number) =>
    {   
        setLatticeParameters(curr => ({
            ...curr,
            pitch: parseFloat(newPitch.toFixed(7)),
            elementDiameter: newPitch <= curr.elementDiameter
                ? newPitch
                : curr.elementDiameter
        }));
    }, [setLatticeParameters]);

    
    const onChangeLatticeElementDiameter = useCallback((newDiameter: number) =>
    {
        setLatticeParameters(curr => ({
            ...curr,
            pitch: curr.pitch >= newDiameter
                ? curr.pitch
                : newDiameter,
            elementDiameter: parseFloat(newDiameter.toFixed(7))
        }));
    }, [setLatticeParameters]);

    
    const onChangeSecondaryParameters = useCallback((newValue: number, parameter: PhysicalParameter) =>
    {
        let newDiameter: number = latticeParameters.elementDiameter;
        let newPitch: number = latticeParameters.pitch;
        let newAt: number = At;
        let newSp: number = 0;
        switch (parameter) {
            case PhysicalParameter.WET_PERIMETER:
                newDiameter = wetPerimeter_r(newValue, latticeType);
                onChangeLatticeElementDiameter(newDiameter);
                break;
            case PhysicalParameter.STRUCTURE_AREA:
                newDiameter = structureArea_r(newValue, latticeType);
                onChangeLatticeElementDiameter(newDiameter);
                break;
            case PhysicalParameter.TOTAL_AREA:
                newPitch = totalArea_r(newValue, latticeType);
                onChangeLatticePitch(newPitch);
                break;
            case PhysicalParameter.VOLUMETRIC_AREA:
                newAt = volumetricArea_r(newValue, Pm);
                onChangeSecondaryParameters(newAt, PhysicalParameter.TOTAL_AREA);
                break;
            case PhysicalParameter.CROSSING_SECTION:
                newAt = crossingSection_r(newValue, Ap);
                onChangeSecondaryParameters(newAt, PhysicalParameter.TOTAL_AREA);
                break;
            case PhysicalParameter.VOLUME_FRACTION_STRUCTURE:
                newAt = volumeFractionStructure_r(newValue, Ap);
                onChangeSecondaryParameters(newAt, PhysicalParameter.TOTAL_AREA);
                break;
            case PhysicalParameter.HYDRAULIC_DIAMETER:
                newSp = hydraulicDiameter_r(newValue, Pm);
                onChangeSecondaryParameters(newSp, PhysicalParameter.CROSSING_SECTION);
                break;
            default:
                break;
        }
    }, [Ap, At, Pm, latticeParameters.elementDiameter, latticeParameters.pitch, latticeType, onChangeLatticeElementDiameter, onChangeLatticePitch]);

    return (
        <div className="lattice-parameters-body">
            <div className="lattice-parameters-simple-input">
                <p>Real dimensions</p>
                <div className='lattice-parameters-real-dim'>
                    <Checkbox
                        title="Display lattice with real dimensions"
                        isChecked={latticeParameters.isElementCircle}
                        onClick={() => setLatticeParameters(curr => ({
                            ...curr,
                            isElementCircle: !latticeParameters.isElementCircle,
                        }))}
                    />
                    <Dropdown
                        logo={unit}
                        titleButton={'Select length unit ('+unit+')'}
                        classNameButton="button-unit-selection"
                        disabled={latticeType === LatticeType.CIRCLE}
                        isLogo={false}
                    >
                        <button onClick={() => setUnit('m')}>m</button>
                        <button onClick={() => setUnit('cm')}>cm</button>
                        <button onClick={() => setUnit('mm')}>mm</button>
                        <button onClick={() => setUnit('inch')}>inch</button>
                    </Dropdown>
                </div>
            </div>
            <div
                className="lattice-parameters-simple-input"
                style={{display: latticeType !== LatticeType.CIRCLE ? 'grid' : 'none'}}
            >
                <div className="display-row">
                    Lattice Pitch [{unit}]
                    <ButtonHelp>
                        <p>
                            Lattice pitch distance <InlineMath math="p"/> equals
                            to the distance between 2 element's center. Also
                            represents the side-to-side distance of the lattice
                            elements if "Real dimensions" is not checked.
                        </p>
                        <p></p>
                    </ButtonHelp>
                </div>
                <input
                    type="number"
                    min="0"
                    step="0.1"
                    value={latticeParameters.pitch}
                    onChange={(e) => onChangeLatticePitch(parseFloat(e.target.value))}
                />
            </div>
            <div className="lattice-parameters-simple-input">
                <div className="display-row">
                    Element Diameter [{unit}]
                    <ButtonHelp>
                        <p>
                            Element diameter <InlineMath math='D'/>
                        </p>
                        <p></p>
                    </ButtonHelp>
                </div>
                <input
                    type="number"
                    min="0"
                    step="0.1"
                    value={latticeParameters.elementDiameter}
                    onChange={(e) => onChangeLatticeElementDiameter(parseFloat(e.target.value))}
                />
            </div>
            {/* <div className="lattice-parameters-simple-input">
                <p>Wet Perimeter</p>
                <input
                    type="number"
                    min="0"
                    // max={wetPerimeter(latticeParameters.pitch, latticeType)}
                    step="0.1"
                    value={Pm}
                    onChange={(e) => onChangeSecondaryParameters(
                        parseFloat(e.target.value),
                        PhysicalParameter.WET_PERIMETER
                    )}
                />
            </div> */}
            <div
                className="lattice-parameters-simple-input"
                style={{display: latticeType !== LatticeType.CIRCLE ? 'grid' : 'none'}}
            >
                <div className="display-row">
                    Hydraulic Diameter [{unit}]
                    <ButtonHelp>
                        <div>
                            Hydraulic diameter <InlineMath math="D_h"/> computed 
                            as: <BlockMath math="D_h = \frac{4 S_p}{P_m}"/>
                        </div>
                        <div>
                            with <InlineMath math="S_p"/> the fluid flow cross section area
                            and <InlineMath math="P_m"/> the wet perimeter.
                        </div>
                        <div>
                            For {getLatticeTypeAsString(latticeType)} lattice, 
                            the formula can be expressed as:
                            {latticeType === LatticeType.HEXAGON && (
                                <BlockMath math="D_h = \frac{2 \sqrt{3} p^2}{\pi D} - D"/>
                            )}
                            {latticeType === LatticeType.SQUARE && (
                                <BlockMath math="D_h = \frac{4 p^2}{\pi D} - D"/>
                            )}
                        </div>
                    </ButtonHelp>
                </div>
                <input
                    type="number"
                    min="0"
                    step="0.05"
                    value={Dh}
                    onChange={(e) => onChangeSecondaryParameters(
                        parseFloat(e.target.value),
                        PhysicalParameter.HYDRAULIC_DIAMETER
                    )}
                />
            </div>
            <div
                className="lattice-parameters-simple-input"
                style={{display: latticeType !== LatticeType.CIRCLE ? 'grid' : 'none'}}
            >
                <div className='display-row'>
                    <p>
                        Volumetric Area [{unit}<sup>2</sup>/{unit}<sup>3</sup>]
                    </p>
                    <ButtonHelp>
                        <div>
                            Volumetric area <InlineMath math="A_V"/> computed 
                            as: <BlockMath math="A_V = \frac{P_m}{A_{tot}}"/>
                        </div>
                        <div>
                            with <InlineMath math="P_m"/> the wet perimeter
                            and <InlineMath math="A_{tot}"/> the total area of
                            the lattice pattern.
                        </div>
                        <div>
                            For {getLatticeTypeAsString(latticeType)} lattice, 
                            the formula can be expressed as:
                            {latticeType === LatticeType.HEXAGON && (
                                <BlockMath math="A_V = \frac{2 \pi D}{\sqrt{3} p^2}"/>
                            )}
                            {latticeType === LatticeType.SQUARE && (
                                <BlockMath math="A_V = \frac{\pi D}{p^2}"/>
                            )}
                        </div>
                    </ButtonHelp>
                </div>
                <input
                    type="number"
                    min="0"
                    step="0.05"
                    value={Av}
                    onChange={(e) => onChangeSecondaryParameters(
                        parseFloat(e.target.value),
                        PhysicalParameter.VOLUMETRIC_AREA
                    )}
                />
            </div>
            <div
                className="lattice-parameters-simple-input"
                style={{display: latticeType !== LatticeType.CIRCLE ? 'grid' : 'none'}}
            >
                <div className='display-row'>
                    Volume Fraction Structure [-]
                    <ButtonHelp>
                        <div>
                            Volume fraction of structure <InlineMath math="f_s"/> computed 
                            as: <BlockMath math="f_s = \frac{A_p}{A_{tot}}"/>
                        </div>
                        <div>
                            with <InlineMath math="A_p"/> the structure area
                            and <InlineMath math="A_{tot}"/> the total area of
                            the lattice pattern.
                        </div>
                        <div>
                            For {getLatticeTypeAsString(latticeType)} lattice, 
                            the formula can be expressed as:
                            {latticeType === LatticeType.HEXAGON && (
                                <BlockMath math="f_s = \frac{\pi D^2}{2 \sqrt{3} p^2}"/>
                            )}
                            {latticeType === LatticeType.SQUARE && (
                                <BlockMath math="f_s = \frac{\pi D^2}{4 p^2}"/>
                            )}
                        </div>
                    </ButtonHelp>
                </div>
                <input
                    type="number"
                    min="0"
                    max="1"
                    step="0.05"
                    value={fs}
                    onChange={(e) => onChangeSecondaryParameters(
                        parseFloat(e.target.value),
                        PhysicalParameter.VOLUME_FRACTION_STRUCTURE
                    )}
                />
            </div>
        </div>
    );
}

const memoDimensionCalculator = memo(DimensionCalculator);

export { memoDimensionCalculator as DimensionCalculator };

// ************************************************************************** //
