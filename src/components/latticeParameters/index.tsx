/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { Button } from './../button';
import { Checkbox } from './../checkbox';
import { CircleLine, LatticeContext, LatticeParameterObject, LatticeType } from '../../context/latticeContext';
import { computeCircleSize, getNumberTileInCircle, numberToLetter } from '../../lib/mathFunctions';
import { generateColorListFromRange } from '../../lib/color';
import { DimensionCalculator } from '../dimensionCalculator';
import './styles.css';


export function LatticeParameters({
    latticeType,
    latticeParameters,
    fontSizeScale = 1,
    circleDiameter,
    isShowCircleDimensions,
    isBorder,
    isColorGradient
}: {
    latticeType: LatticeType,
    latticeParameters: LatticeParameterObject,
    fontSizeScale?: number,
    circleDiameter: CircleLine[],
    isShowCircleDimensions: boolean,
    isBorder: boolean,
    isColorGradient: boolean
}) {
    const {
        latticeObject,
        setLatticeObjectWithHistory,
        setFontSizeScale,
        setCircleDiameter,
        setIsShowCircleDimensions,
        setIsBorder,
        setIsColorGradient
    } = useContext(LatticeContext);

    const [isDropdown, setIsDropdown] = useState(false);
    const [referenceLineLength, setReferenceLineLength] = useState(1);
    const [colorGradientLow, setColorGradientLow] = useState("#75B3F0");
    const [colorGradientHigh, setColorGradientHigh] = useState("#F07575");
    const [circularParamTemp, setCircularParamTemp] = useState({idx: -1, value: 0});
    
    const isColorMapLabelsAllNumber = useMemo(() => 
        latticeObject.colorMap
            .map(row => row.label)
            .every(value => !isNaN(Number(value)))
    , [latticeObject.colorMap]);

    useEffect(() => {
        setCircleDiameter(curr => curr.map((circleLine: CircleLine) => ({
            ...circleLine,
            circleSize: computeCircleSize(
                circleLine.diameter,
                latticeParameters.scale
            ),
            numberTileInCircle: getNumberTileInCircle(
                latticeObject.lattice,
                circleLine.diameter,
                latticeParameters.pitch,
                latticeType,
                latticeParameters.isElementCircle,
                latticeParameters.elementDiameter,
                latticeObject.circularParameters
            )
        })));
    }, [
        latticeObject.lattice, latticeObject.circularParameters,
        latticeParameters, latticeType, setCircleDiameter
    ]);

    const updateColorMap = useCallback((newIsColorGradient: boolean) => {
        const labels = latticeObject.colorMap.map(row => row.label);
        
        if (isColorMapLabelsAllNumber && labels.length >= 2 && newIsColorGradient)
        {   
            const gradientColors = 
                generateColorListFromRange(
                    labels.map(value => Number(value)),
                    colorGradientLow,
                    colorGradientHigh
                );
            
            const updatedNewColorMap = latticeObject.colorMap.map((color, idx) => {
                return({
                    ...color,
                    color: gradientColors[idx]
                });
            });
    
            setLatticeObjectWithHistory({
                ...latticeObject,
                colorMap: updatedNewColorMap
            });
        }
    }, [
        colorGradientHigh, colorGradientLow, isColorMapLabelsAllNumber,
        latticeObject, setLatticeObjectWithHistory
    ]);

    const updateCircularParameters = useCallback((value: number, idx: number, type: string) => {
        const newParams = [...latticeObject.circularParameters];
        if (type === 'r') {
            newParams[idx].r = value;
        } else if (type === 'theta') {
            newParams[idx].theta = value;
        }
        
        const newLattice = [...latticeObject.lattice];
        newLattice.sort((a, b) => newParams[newLattice.indexOf(a)].r - newParams[newLattice.indexOf(b)].r);
        newParams.sort((a, b) => a.r - b.r);

        setLatticeObjectWithHistory({
            ...latticeObject,
            lattice: newLattice,
            circularParameters: newParams
        });
    }, [setLatticeObjectWithHistory, latticeObject]);

    const updateLineLength = useCallback((
        newCircleDiameter: CircleLine[],
        lineLength: number
    ) => {
        const ratioLineLength = 0.2071;
        const maxDiameter = Math.max(...newCircleDiameter.map(curr => curr.diameter));

        return(
            newCircleDiameter.map(curr => ({
                ...curr,
                length: (maxDiameter/curr.diameter * (1+2*lineLength*ratioLineLength) - 1)/(2*ratioLineLength)
            }))
        );
    }, []);

    const onChangeCircleDiameter = useCallback((diameter: number, idx: number) =>
    {
        setCircleDiameter(curr => {
            const newCircleDiameter = [...curr];
            newCircleDiameter[idx].diameter = diameter;
            newCircleDiameter[idx].circleSize = computeCircleSize(
                diameter,
                latticeParameters.scale
            );
            newCircleDiameter[idx].numberTileInCircle = getNumberTileInCircle(
                latticeObject.lattice,
                diameter,
                latticeParameters.pitch,
                latticeType,
                latticeParameters.isElementCircle,
                latticeParameters.elementDiameter,
                latticeObject.circularParameters
            );
            
            return(updateLineLength(newCircleDiameter, referenceLineLength));
        });

    }, [
        latticeObject.lattice, latticeObject.circularParameters, latticeParameters,
        latticeType, referenceLineLength, setCircleDiameter, updateLineLength
    ]);
    
    const onChangeColor = useCallback((event: React.ChangeEvent<HTMLInputElement>, idx: number) =>
    {
        setCircleDiameter(curr => {
            const newCircleDiameter = [...curr];
            newCircleDiameter[idx].color = event.target.value;
            return(newCircleDiameter);
        });
    }, [setCircleDiameter]);
    
    const onChangeCheck = useCallback((idx: number, isChecked: boolean) =>
    {   
        setCircleDiameter(curr => {
            const newCircleDiameter = [...curr];
            newCircleDiameter[idx].isDisplay = !isChecked;
            return(newCircleDiameter);
        });
    }, [setCircleDiameter]);

    const addCircleRow = useCallback(() => {
        const circleDefaultDiameter = 100;
        setCircleDiameter(curr => updateLineLength([
            ...curr,
            {
                diameter: circleDefaultDiameter,
                circleSize: computeCircleSize(
                    circleDefaultDiameter,
                    latticeParameters.scale
                ),
                color: "#000000",
                length: circleDefaultDiameter,
                numberTileInCircle: getNumberTileInCircle(
                    latticeObject.lattice,
                    circleDefaultDiameter,
                    latticeParameters.pitch,
                    latticeType,
                    latticeParameters.isElementCircle,
                    latticeParameters.elementDiameter,
                    latticeObject.circularParameters
                ),
                isDisplay: true
            }
        ], referenceLineLength));
    }, [
        latticeObject.lattice, latticeObject.circularParameters, latticeParameters,
        latticeType, referenceLineLength, setCircleDiameter, updateLineLength
    ]);

    const deleteCircleRow = useCallback((idx: number) => {
        setCircleDiameter(curr => {
            const copyList = [...curr];
            copyList.splice(idx, 1);
            return(updateLineLength(copyList, referenceLineLength));
        });
    }, [referenceLineLength, setCircleDiameter, updateLineLength]);

    const onClickColorGradient = useCallback(() => {
        setIsColorGradient(curr => {
            updateColorMap(!curr);
            return(!curr);
        });
    }, [setIsColorGradient, updateColorMap]);

    return (
        <section className="lattice-parameters">
            <div className="lattice-parameters-header">
                <p>Parameters and Options</p>
                <Button
                    className="lattice-button"
                    onClick={() => setIsDropdown((curr) => !curr)}
                >
                    {isDropdown ? "keyboard_arrow_up" : "keyboard_arrow_down"}
                </Button>
            </div>
            {isDropdown && (
                <div className="lattice-parameters-body">
                    <div className='lattice-parameters-separation'/>

                    <div className="lattice-parameters-simple-input">
                        <p>Color gradient</p>
                        <div className="lattice-parameter-colors-container">
                            <Checkbox
                                title="Activate color gradient (only for lattice filled with numbers)"
                                isChecked={isColorGradient}
                                onClick={onClickColorGradient}
                                disabled={!isColorMapLabelsAllNumber}
                            />
                            <input
                                type="color"
                                className="circle-list-color-select"
                                value={colorGradientLow}
                                onChange={(e) => setColorGradientLow(e.target.value)}
                                onBlur={() => updateColorMap(isColorGradient)}
                            />
                            <input
                                type="color"
                                className="circle-list-color-select"
                                value={colorGradientHigh}
                                onChange={(e) => setColorGradientHigh(e.target.value)}
                                onBlur={() => updateColorMap(isColorGradient)}
                            />
                        </div>
                    </div>
                    <div className="lattice-parameters-simple-input">
                        <p>Font size</p>
                        <input
                            type="range"
                            min={0}
                            max={1}
                            step={0.1}
                            value={fontSizeScale}
                            onChange={(e) => setFontSizeScale(Number(e.target.value))}
                        />
                    </div>

                    <div className="lattice-parameters-simple-input">
                        <p>Add border</p>
                        <Checkbox
                            title="Show tile border"
                            isChecked={isBorder}
                            onClick={() => setIsBorder(curr => !curr)}
                            disabled={latticeType === LatticeType.HEXAGON}
                        />
                    </div>

                    <div className='lattice-parameters-separation'/>

                    <DimensionCalculator
                        latticeParameters={latticeParameters}
                        latticeType={latticeType}
                    />

                    {latticeType === LatticeType.CIRCLE && (
                        <div className='lattice-circular-list-container'>
                            <div className="lattice-circular-list-grid-header">
                                <p>Ring</p>
                                <p>Count</p>
                                <p>Radius [cm]</p>
                                <p>Angle [deg]</p>
                            </div>
                            {latticeObject.circularParameters.map((ring, idx) => (
                                <div
                                    key={idx}
                                    className="lattice-circular-list-grid-content"
                                >
                                    <p>{numberToLetter(idx)}</p>
                                    <p>{latticeObject.lattice[idx].length}</p>
                                    <input
                                        className="lattice-parameters-body-input"
                                        type="number"
                                        min="0"
                                        step="0.1"
                                        value={idx !== circularParamTemp.idx ? ring.r : circularParamTemp.value}
                                        onFocus={() => setCircularParamTemp(curr => ({...curr, idx: idx, value: ring.r}))}
                                        onChange={(e) => setCircularParamTemp({
                                            idx: idx, value: parseFloat(e.target.value)
                                        })}
                                        onBlur={() => {
                                            updateCircularParameters(circularParamTemp.value, idx, 'r');
                                            setCircularParamTemp(curr => ({...curr, idx: -1}));
                                        }}
                                    />
                                    <input
                                        className="lattice-parameters-body-input"
                                        type="number"
                                        min="-360"
                                        max="360"
                                        step="1"
                                        disabled={ring.r === 0}
                                        value={ring.theta}
                                        onChange={(e) => updateCircularParameters(parseFloat(e.target.value), idx, 'theta')}
                                    />
                                </div>
                            ))}
                        </div>
                    )}

                    <div className='lattice-parameters-separation'/>

                    <div className="lattice-parameters-simple-input">
                        <p>Line length</p>
                        <input
                            type="number"
                            min="0.1"
                            step="0.1"
                            value={referenceLineLength}
                            onChange={(e) => {
                                const newLineLength = Number(e.target.value);
                                setReferenceLineLength(newLineLength);
                                setCircleDiameter(updateLineLength(circleDiameter, newLineLength));
                            }}
                        />
                    </div>
                    <div className='circle-list-container'>
                        <div className="circle-list-grid-header">
                            <Checkbox
                                title="Show circle dimensions"
                                isChecked={isShowCircleDimensions}
                                onClick={() => setIsShowCircleDimensions(curr => !curr)}
                            />
                            <p>Inner radius</p>
                            <p>Count</p>
                            <p>Color</p>
                            <p></p>
                        </div>
                        {circleDiameter.map((circleLine, idx) => (
                            <div
                                key={idx}
                                className="circle-list-grid-content"
                            >
                                <Checkbox
                                    title="Display circle"
                                    isChecked={circleLine.isDisplay}
                                    onClick={() => onChangeCheck(idx, circleLine.isDisplay)}
                                />
                                <input
                                    className="lattice-parameters-body-input"
                                    type="number"
                                    min="0"
                                    step="0.1"
                                    value={circleLine.diameter/2}
                                    onChange={(e) => onChangeCircleDiameter(2.0*Number(e.target.value), idx)}
                                />
                                <p
                                    title={
                                        "There is " +
                                        circleLine.numberTileInCircle +
                                        " cells fully inside the circle"
                                    }
                                >
                                    {circleLine.numberTileInCircle}
                                </p>
                                <input
                                    type="color"
                                    className="circle-list-color-select"
                                    value={circleLine.color}
                                    onChange={(e) => onChangeColor(e, idx)}
                                />
                                <button
                                    title="Delete circle"
                                    className="circle-list-delete-row"
                                    onClick={() => deleteCircleRow(idx)}
                                >
                                    <span className="material-symbols-outlined">
                                        delete
                                    </span>
                                </button>
                            </div>
                        ))}
                        <button
                            className="lattice-parameters-add-circle"
                            onClick={addCircleRow}
                        >
                            Add circle
                        </button>
                    </div>
                </div>
            )}
        </section>
    );
}

// ************************************************************************** //
