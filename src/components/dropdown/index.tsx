/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import React, { memo } from 'react';
import { Button } from "../button";
import { useComponentVisible } from "../../hooks/useComponentVisible";
import './styles.css';


interface DropdownProps {
    logo: string,
    isLogo?: boolean,
    titleButton?: string,
    classNameButton?: string,
    disabled?: boolean,
    children: React.ReactElement[]
}

function Dropdown({
    logo,
    isLogo = true,
    titleButton = "",
    classNameButton = "",
    disabled = false,
    children,
}: DropdownProps) {
    const {ref, isComponentVisible, setIsComponentVisible} = useComponentVisible(false);
    
    return (
        <div ref={ref}>
            {isLogo
                ? (
                    <Button
                        className={classNameButton}
                        title={titleButton}
                        onClick={() => setIsComponentVisible(curr => !curr)}
                        disabled={disabled}
                    >
                        {logo}
                    </Button>
                )
                : (
                    <button
                        className={classNameButton}
                        title={titleButton}
                        onClick={() => setIsComponentVisible(curr => !curr)}
                        disabled={disabled}
                    >
                        {logo}
                    </button>
                )
            }
            

            {isComponentVisible && (
                <div className='dropdown-list'>
                    {
                        children.map((child, key) => React.cloneElement(
                            child, {
                                ...child.props,
                                key: key,
                                onClick: () => {
                                    child.props.onClick();
                                    setIsComponentVisible(false);
                                }
                            }
                        ))
                    }
                </div>
            )}
        </div>
    );
}

const memoDropdown = memo(Dropdown);

export { memoDropdown as Dropdown };

// ************************************************************************** //
