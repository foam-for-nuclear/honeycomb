/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import "./styles.css"

interface LatticeCircleProps {
    latticeCircleSize: number,
    circleDiameter: number,
    color?: string,
    isAddLineRadius?: boolean,
    rotationAngle?: number,
    lineLengthCoeff?: number,
    isDisplay?: boolean
}

export function LatticeCircle({
    latticeCircleSize,
    circleDiameter,
    color = "#000000",
    isAddLineRadius = true,
    rotationAngle = 45,
    lineLengthCoeff = 1,
    isDisplay = true
}: LatticeCircleProps) {

    return (
        <div
            className="lattice-circle-container"
            style={{"display": isDisplay ? "flex" : "none"}}
        >
            <span
                className="lattice-circle"
                style={{
                    outline: "4px solid "+color,
                    width: latticeCircleSize,
                    height: latticeCircleSize,
                    rotate: "calc(-" + rotationAngle + "deg)"
                }}
            >
                {isAddLineRadius &&
                    <div
                        className="lattice-circle-line"
                        style={{
                            backgroundColor: color,
                            right: "calc(-" + lineLengthCoeff + " * 20.71%)",
                            width: "calc( " + lineLengthCoeff + " * 20.71%)"
                        }}
                    >
                        <p className="lattice-circle-radius-text">
                            {circleDiameter/2}
                        </p>
                    </div>
                }
            </span>
        </div>
    );
}

// ************************************************************************** //
