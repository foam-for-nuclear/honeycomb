/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import "./styles.css"
import { LatticeType } from "../../context/latticeContext";
import { memo, useMemo } from "react";

interface LatticeRowProps extends React.HTMLAttributes<HTMLDivElement> {
    row: number,
    latticeType: LatticeType,
    latticeTileSize: number,
    latticeTileMargin: number
}

function LatticeRow({
    row,
    latticeType,
    latticeTileSize,
    latticeTileMargin,
    ...props
}: LatticeRowProps) {

    const marginLeft = useMemo(() => 
        latticeType === LatticeType.HEXAGON
        ? row * (latticeTileSize/2.0 + latticeTileMargin) // 'calc('+row+'*(var(--s)/2 + var(--m)))'
        : 0
    , [latticeTileMargin, latticeTileSize, latticeType, row]);

    const className = useMemo(() => {
        switch (latticeType) {
            case LatticeType.HEXAGON:
                return('row-hexagon');
            case LatticeType.SQUARE:
                return('row-square');
            case LatticeType.TRIANGULAR:
                return('row-triangular');
        }
    }, [latticeType]);

    return (
        <div
            {...props}
            className={className}
            style={{marginLeft: marginLeft}}
        >
            {props.children}
        </div>
    );
}

const memoLatticeRow = memo(LatticeRow);

export { memoLatticeRow as LatticeRow };

// ************************************************************************** //
