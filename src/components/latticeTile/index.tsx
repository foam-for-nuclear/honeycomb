/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { ReactNode, memo, useCallback, useMemo } from 'react';
import { LatticeType } from '../../context/latticeContext';
import { getFontColor, getLuminance, rgbToString } from '../../lib/color';
import { numberToLetter } from '../../lib/mathFunctions';
import './styles.css';

interface LatticeTileProps /*extends Omit<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    "onMouseOver" | "onClick"
>*/ {
    element: string,
    row: number,
    col: number,
    latticeTileSize: number,
    latticeTileMargin: number,
    backgroundColor: string,
    latticeType: LatticeType,
    title?: string,
    fontSizeScale?: number,
    isCircle?: boolean,
    isBrushMode?: boolean,
    isShowLabel?: boolean,
    isHover?: boolean,
    isEmpty?: boolean,
    isBorder?: boolean,
    isRowColumnLabel?: boolean,
    isGreyScale?: boolean,
    xCoord?: number,
    yCoord?: number,
    onClick: (element: string, row: number, col: number) => void,
    onMouseOver: (row: number, col: number) => void,
    onMouseDown: (
        event: React.MouseEvent<HTMLButtonElement>,
        row: number,
        col: number,
        element: string
    ) => void,
    onMouseUp: (
        event: React.MouseEvent<HTMLButtonElement>,
        row: number,
        col: number,
        element: string
    ) => void,
}

function getElement(children: ReactNode, row: number, col: number, isEmpty: boolean, isRowColumnLabel: boolean)
{
    if (isEmpty)
    {
        return("/");
    }
    if (row === -1 && col >= 0 && isRowColumnLabel)
    {
        return(numberToLetter(col));
    }
    else if (col === -1 && row >= 0 && isRowColumnLabel)
    {
        return(row+1);
    }
    return(children);
}

function getBackground(
    backgroundColor: string,
    children: ReactNode,
    isEmpty: boolean,
    isRowColumnLabel: boolean,
    isGreyScale: boolean
): string {
    if (isEmpty)
    {
        return("none");
    }
    else if (isRowColumnLabel && children === "/")
    {
        return("none")
    }
    else if (isRowColumnLabel && children === "\\")
    {
        return("var(--lightgrey)")
    }
    else if (backgroundColor === "#FFFFFF")
    {
        return("rgba(0, 0, 0, 0)");
    }
    else if (isGreyScale)
    {
        const luminance = 256 * getLuminance(backgroundColor);   
        return(rgbToString({r: luminance, g: luminance, b: luminance}));
    }
    return(backgroundColor);
}


function LatticeTile({
    element,
    row,
    col,
    latticeTileSize,
    latticeTileMargin,
    backgroundColor,
    latticeType,
    title = numberToLetter(col)+""+(row+1),
    fontSizeScale = 1,
    isCircle = true,
    isBrushMode = false,
    isShowLabel = false,
    isHover = false,
    isEmpty = false,
    isBorder = false,
    isRowColumnLabel = false,
    isGreyScale = false,
    xCoord = 0,
    yCoord = 0,
    onClick,
    onMouseOver,
    onMouseDown,
    onMouseUp,
    // ...props
}: LatticeTileProps) {

    const children = useMemo(() => 
        latticeType !== LatticeType.CIRCLE 
            ? (
                getElement(element, row, col, isEmpty, isRowColumnLabel)
            ) : (
                element
            )
    , [latticeType, element, row, col, isEmpty, isRowColumnLabel]);

    const className = useMemo(() => {
        if (isCircle)
        {
            return("circle");
        }

        switch (latticeType) {
            case LatticeType.HEXAGON:
                return("hexagon");
            case LatticeType.SQUARE:
                return("square");
            case LatticeType.TRIANGULAR:
                return("triangular");
            case LatticeType.CIRCLE:
                return("circle");
        }
    }, [latticeType, isCircle]);

    const height = useMemo(() => {
        if (isCircle)
        {
            return(latticeTileSize);
        }

        switch (latticeType) {
            case LatticeType.HEXAGON:
                return(latticeTileSize * Math.sqrt(3.0)*2.0/3.0);
            case LatticeType.SQUARE:
                return(latticeTileSize);
            case LatticeType.TRIANGULAR:
                return(latticeTileSize * Math.sqrt(3.0)/2.0);
            case LatticeType.CIRCLE:
                return(latticeTileSize);
        }
    }, [latticeTileSize, latticeType, isCircle]);

    const marginVertical = useMemo(() => {
        const p = 2.0 * latticeTileMargin + latticeTileSize;
        switch (latticeType) {
            case LatticeType.HEXAGON:
                if (isCircle)
                {
                    return((Math.sqrt(3.0)/2.0 * p - latticeTileSize)/2.0);
                }
                return((Math.sqrt(3.0)/2.0 * p - height)/2.0);
                // return(latticeTileMargin - latticeTileSize * Math.sqrt(3.0)/12.0);
            case LatticeType.SQUARE:
                return(latticeTileMargin);
            case LatticeType.TRIANGULAR:
                return(latticeTileMargin);
            case LatticeType.CIRCLE:
                return(latticeTileMargin);
        }
    }, [latticeTileMargin, latticeTileSize, latticeType, isCircle, height]);

    const color = useMemo(() => {
        if (backgroundColor === "#FFFFFF" || (!isShowLabel && !isEmpty && !isRowColumnLabel))
        {
            return("rgba(0, 0, 0, 0)"); // Transparant
        }
        else if (isRowColumnLabel && latticeType !== LatticeType.CIRCLE)
        {
            return("#000000");
        }
        else if (isEmpty || !isShowLabel)
        {
            return("rgba(0, 0, 0, 0)"); // Transparant
        }
        else if (backgroundColor)
        {
            return(getFontColor(backgroundColor));
        }
        return("#000000");
    }, [backgroundColor, isEmpty, isRowColumnLabel, isShowLabel, latticeType]);

    const border = useMemo(() => {
        if (
            latticeType === LatticeType.CIRCLE
            && backgroundColor !== "#FFFFFF"
        )
        {
            return('black solid 0.1rem');
        }
        if (
            latticeType === LatticeType.SQUARE
            && !isEmpty
            && !isRowColumnLabel
            && backgroundColor !== "#FFFFFF"
        ) {
            return('black solid 0.1rem');
        }
        return('none');
    }, [latticeType, isEmpty, isRowColumnLabel, backgroundColor]);

    const style: React.CSSProperties = useMemo(() => {
        const commonStyle = {
            height: height,
            width: latticeTileSize,
            fontSize: fontSizeScale * latticeTileSize/2.0,
            color: color,
            opacity: isHover ? 0.6 : 1,
            border: isBorder ? border : 'none'
        };
        if (latticeType === LatticeType.CIRCLE)
        {
            return({
                ...commonStyle,
                position: 'absolute',
                top: 'calc(' + xCoord + 'px - '+latticeTileSize/2+'px + 50%)',
                left: 'calc(' + yCoord + 'px - '+latticeTileSize/2+'px + 50%)',
                background: getBackground(backgroundColor, element, false, false, isGreyScale),
            });
        }
        return({
            ...commonStyle,
            background: getBackground(backgroundColor, element, isEmpty, isRowColumnLabel, isGreyScale),
            marginLeft: latticeTileMargin,
            marginRight: latticeTileMargin,
            marginTop: marginVertical,
            marginBottom: marginVertical,
            pointerEvents: (isEmpty || isRowColumnLabel) ? "none" : "all"
        });
    }, [
        height, latticeTileSize, fontSizeScale, backgroundColor, element, color,
        isHover, border, latticeType, isEmpty, isRowColumnLabel, isBorder, isGreyScale,
        latticeTileMargin, marginVertical, xCoord, yCoord
    ]);

    const callOnMouseOver = useCallback((
        event: React.MouseEvent<HTMLButtonElement>, row: number, col: number, element: string
    ) => {
        if (isBrushMode && event.shiftKey) {
            onClick(element, row, col)
        } else {
            onMouseOver(row, col);
        }
    }, [isBrushMode, onClick, onMouseOver]);

    return (
        <button
            id={'row'+row+'-h'+col}
            title={title+": "+element}
            className={className}
            style={style}
            onMouseOver={(event) => callOnMouseOver(event, row, col, element)}
            onMouseLeave={(event) => callOnMouseOver(event, -1, -1, element)}
            onMouseDown={(event) => onMouseDown(event, row, col, element)}
            onMouseUp={(event) => onMouseUp(event, row, col, element)}
            onClick={() => onClick(element, row, col)}
        >
            {children}
            {latticeType === LatticeType.CIRCLE && (
                <div style={{
                    fontSize: fontSizeScale * latticeTileSize/4.0,
                    color: color,
                    pointerEvents: 'none'
                }}>
                    {isRowColumnLabel && title}
                </div>
            )}
        </button>
    );
}

const memoLatticeTile = memo(LatticeTile);

export { memoLatticeTile as LatticeTile };

// ************************************************************************** //
