/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { memo } from 'react';
import './styles.css';


interface CheckboxProps { // extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    title?: string,
    isChecked?: boolean,
    disabled?: boolean,
    onClick: () => void
}

function Checkbox({
    title = "",
    isChecked = false,
    disabled = false,
    onClick
}: CheckboxProps) {
    return (
        <button
            title={title}
            className="button-style"
            onClick={() => onClick()}
            disabled={disabled}
        >
            <span className="material-symbols-outlined">
                {isChecked ? "check_box" : "check_box_outline_blank"}
            </span>
        </button>
    );
}

const memoCheckbox = memo(Checkbox);

export { memoCheckbox as Checkbox };

// ************************************************************************** //
