/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import './styles.css';
import honeycombLogo from "../../assets/honeycomb-logo.svg";
import gitlabLogo from "../../assets/gitlab-logo-500.svg";

export function Header()
{
    return (
        <div className="banner">
            <nav className="container">
                <div className="banner-name">
                    <img src={honeycombLogo} alt="Logo"/>
                    <p className="display-large">
                        Honeycomb
                    </p>
                </div>
                <a 
                    href="https://gitlab.com/foam-for-nuclear/honeycomb"
                    className="gitlab-link"    
                >
                    <img 
                        src={gitlabLogo}
                        alt="Gitlab"
                    />
                    foam-for-nuclear
                </a>     
            </nav>
        </div>
    );
}

// ************************************************************************** //
