/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import React, { memo, useCallback, useContext, useEffect, useState } from "react";

import { LatticeContext, ColorLine } from '../../context/latticeContext';
import { Checkbox } from "../checkbox";

import './styles.css';
import { getNumberLabelInLatticeArrayByLabel, replaceLabelInLattice } from "../../lib/latticeEdition";

interface ColorTableRowProps extends React.HTMLAttributes<HTMLDivElement> {
    row: ColorLine
}

function ColorTableRow({
    row,
    ...props
}: ColorTableRowProps) {
    const {
        latticeObject,
        setLatticeObject,
        setLatticeObjectWithHistory
    } = useContext(LatticeContext);

    const [labelOnChange, setLabelOnChange] = useState(row.label);
    const [colorOnChange, setColorOnChange] = useState(row.color);
    const [apiTimeout, setApiTimeout] = useState<NodeJS.Timeout>();

    useEffect(() => {
        setLabelOnChange(row.label);
        setColorOnChange(row.color);
    }, [latticeObject, row.color, row.label, setLatticeObjectWithHistory]);

    const updateLabelInLattice = useCallback(() => {
        if (
            labelOnChange !== '' && 
            !latticeObject.colorMap.map(e => e.label).includes(labelOnChange)
        ) {
            setLatticeObjectWithHistory({
                ...latticeObject,
                lattice: replaceLabelInLattice(
                    latticeObject.lattice,
                    row.label,
                    labelOnChange
                ),
                colorMap: latticeObject.colorMap.map((curr) => 
                    curr.label === row.label
                        ? {...curr, label: labelOnChange}
                        : curr
                )
            });
        }
        else if (
            latticeObject.colorMap.map(e => e.label).includes(labelOnChange) &&
            labelOnChange !== row.label
        ) {
            setLatticeObjectWithHistory({
                ...latticeObject,
                lattice: replaceLabelInLattice(
                    latticeObject.lattice,
                    row.label,
                    labelOnChange
                ),
                colorMap: latticeObject.colorMap.filter((curr) => curr.label !== row.label)
            });
        }
        else {
            setLabelOnChange(row.label);
        }
    }, [labelOnChange, latticeObject, row.label, setLatticeObjectWithHistory]);

    const deleteColorMapRow = useCallback(() => {
        const newColorMap = latticeObject.colorMap
            .filter(l => l.label !== row.label);

        const newLatticeArray = replaceLabelInLattice(
            latticeObject.lattice, row.label, "0"
        );

        setLatticeObjectWithHistory({
            ...latticeObject,
            lattice: newLatticeArray,
            colorMap: newColorMap
        });
    }, [latticeObject, row.label, setLatticeObjectWithHistory]);

    const updateColorInLattice = useCallback((newColor: string) => {
        setLatticeObjectWithHistory({
            ...latticeObject,
            colorMap:
                latticeObject.colorMap.map((l) => 
                    (l.label === row.label)
                        ? {...l, color: newColor.toUpperCase()}
                        : l
                )
        })
    }, [latticeObject, row.label, setLatticeObjectWithHistory]);

    const onChangeColor = useCallback((newColor: string) => {
        // Clear previous timeout
        clearTimeout(apiTimeout);
        
        setColorOnChange(newColor);

        // Reset timeout to 300 ms
        setApiTimeout(
            setTimeout(() => {
                updateColorInLattice(newColor)
            }, 300)
        );
    }, [apiTimeout, updateColorInLattice]);

    const onChangeCheckbox = useCallback(() => {
        setLatticeObject((curr) => ({
            ...curr,
            colorMap:
                curr.colorMap.map((l) =>
                    (l.label === row.label)
                        ? {...l, isActivated: !row.isActivated}
                        : l
                )
        }));
    }, [row.isActivated, row.label, setLatticeObject]);

    const onChangeCheckboxCount = useCallback(() => {
        setLatticeObject((curr) => ({
            ...curr,
            colorMap: 
                curr.colorMap.map((l) =>
                    (l.label === row.label)
                        ? {...l, isCount: !row.isCount}
                        : l
                )
        }));
    }, [row.isCount, row.label, setLatticeObject]);

    return (
        <div className="color-map-grid-content" {...props}>
            <span className="material-symbols-outlined drag-indicator">
                drag_indicator
            </span>
            <Checkbox
                title={"Select '" + row.label + "' in lattice edition"}
                isChecked={row.isActivated}
                onClick={() => onChangeCheckbox()}
            />
            <input
                value={labelOnChange}
                className="color-map-label"
                title="Color map element name input"
                onChange={(e) => setLabelOnChange(e.target.value)}
                onBlur={() => updateLabelInLattice()}
                onKeyDown={(e) => e.key === "Enter" && updateLabelInLattice()}
            />
            <input
                value={colorOnChange}
                type="color"
                className="color-map-color"
                title="Color map element color input"
                onChange={(e) => onChangeColor(e.target.value)}
                onBlur={() => updateColorInLattice(colorOnChange)}
                onKeyDown={(e) => e.key === "Enter" && updateColorInLattice(colorOnChange)}
            />
            <div className="color-map-count-container">
                <Checkbox
                    title={"Count the '" + row.label + "' in total"}
                    isChecked={row.isCount}
                    onClick={() => onChangeCheckboxCount()}
                />
                <p>
                    {getNumberLabelInLatticeArrayByLabel(latticeObject.lattice, row.label)}
                </p>
            </div>
            <button
                title="Delete row in color map list"
                className={
                    row.label === "0"
                    ? "color-map-delete-row-zero"
                    : "color-map-delete-row"
                }
                disabled={row.label === "0"}
                onClick={() => deleteColorMapRow()}
            >
                {row.label !== "0" && (
                    <span className="material-symbols-outlined">
                        delete
                    </span>
                )}
            </button>
        </div>
    );
}

const memoColorTableRow = memo(ColorTableRow);

export { memoColorTableRow as ColorTableRow };

// ************************************************************************** //
