/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { useCallback, useContext, useEffect, useMemo, useRef, useState } from "react";

import { LatticeContext } from '../../context/latticeContext';

import './styles.css';
import { generateRandomString, getNumberLabelInLatticeArrayByLabel } from "../../lib/latticeEdition";
import { getColorMapAsText, getColorMapFromText } from "../../lib/colorMapEdition";
import { completeColorMap } from "../../lib/colorMapEdition";
import { generateRandomColorFromList } from "../../lib/color";
import { ColorTableRow } from "../colorTableRow";
import { Checkbox } from "../checkbox";
import { Button } from "../button";
import { ButtonCopyToClipboard } from "../buttonCopyToClipboard";

export function ColorTable()
{
    const {
        latticeObject,
        setLatticeObjectWithHistory
    } = useContext(LatticeContext);

    const [isShowAllTable, setIsShowAllTable] = useState(true);
    const [isDisplayPaletteText, setIsDisplayPaletteText] = useState(false);

    const isLabelAllSelected = useMemo(() =>
        latticeObject.colorMap.every(e => e.isActivated)
    , [latticeObject.colorMap]);

    const colorTextarea = useRef<HTMLTextAreaElement | null>(null);
    const dragItem = useRef<number>(0);
    const dragOverItem = useRef<number>(0);

    const textToCopy = useCallback(() => {
        return(getColorMapAsText(latticeObject.colorMap));
    }, [latticeObject.colorMap]);

    useEffect(() => {
        // Add missing 0 in color table, should be always available for
        // add<Row|Column|Ring>
        if (latticeObject.colorMap.every(e => e.label !== '0'))
        {
            setLatticeObjectWithHistory({
                ...latticeObject, 
                colorMap: [...latticeObject.colorMap, {
                    label: '0',
                    color: '#000000',
                    isActivated: true,
                    isCount: false
                }]
            });
        }
    }, [latticeObject, setLatticeObjectWithHistory]);
    

    const addRowColorMap = useCallback(() =>
    {
        const elementList = latticeObject.colorMap.map((e) => e.label);

        const isAllNumbers = elementList.every((e) => !isNaN(parseInt(e)));

        let newElement = "";
        if (isAllNumbers) {
            const lastNumber = Math.max(...elementList.map((e) => parseInt(e)));

            newElement = String(lastNumber+1);
        }
        else {
            const maxLength = Math.max(...elementList.map((e) => e.length));
            const isAllLowercase = elementList.every((e) => e === e.toLowerCase());
            const isAllUppercase = elementList.every((e) => e === e.toUpperCase());
            let nIter = 0;
            do {
                newElement = generateRandomString(maxLength);
                if (isAllLowercase) {
                    newElement = newElement.toLowerCase();
                }
                if (isAllUppercase) {
                    newElement = newElement.toUpperCase();
                }
                nIter++;
            }
            while (elementList.includes(newElement) && nIter < 100)

            if (nIter >= 100) {
                newElement = generateRandomString(maxLength+1);
            }
        }
        
        setLatticeObjectWithHistory({
            ...latticeObject, 
            colorMap: [...latticeObject.colorMap, {
                label: newElement,
                color: generateRandomColorFromList(latticeObject.colorMap.map((e) => e.color)),
                isActivated: true,
                isCount: true
            }]
        });
    }, [latticeObject, setLatticeObjectWithHistory]);

    const updateColorMapFromTextarea = useCallback((text: string) => {
        setLatticeObjectWithHistory({
            ...latticeObject,
            colorMap: completeColorMap(getColorMapFromText(text), latticeObject.lattice)
        });
    }, [latticeObject, setLatticeObjectWithHistory]);

    const onDragStart = useCallback((event: React.DragEvent<HTMLDivElement>) => {
        dragItem.current = parseInt(event.currentTarget.id);
    }, []);
    
    const onDragEnter = useCallback((event: React.DragEvent<HTMLDivElement>) => {
        event.preventDefault();
        event.dataTransfer.dropEffect = 'move';
        dragOverItem.current = parseInt(event.currentTarget.id);
    }, []);

    const onDragEnd = useCallback(() => {
        const newColorMap = [...latticeObject.colorMap];
        const dragItemContent = newColorMap[dragItem.current];

        newColorMap.splice(dragItem.current, 1);
        newColorMap.splice(dragOverItem.current, 0, dragItemContent);

        setLatticeObjectWithHistory({...latticeObject, colorMap: newColorMap});
        dragItem.current = 0;
        dragOverItem.current = 0;
    }, [latticeObject, setLatticeObjectWithHistory]);

    return (
        <section className="color-map">
            <div className="color-map-grid-header">
                <p></p>
                <Checkbox
                    title={(!isLabelAllSelected ? "Select" : "Unselect") + " all labels"}
                    isChecked={isLabelAllSelected}
                    onClick={() => setLatticeObjectWithHistory({
                        ...latticeObject,
                        colorMap: latticeObject.colorMap.map((curr) => (
                            {
                                ...curr,
                                isActivated: !isLabelAllSelected
                            }
                        ))
                    })}
                />
                <p>Label</p>
                <p>Color</p>
                <p>Count</p>
                <Button
                    className="color-map-toggle-display-input"
                    title={isShowAllTable ? "Hide color table" : "Show color table"}
                    onClick={() => setIsShowAllTable((curr) => !curr)}
                >
                    {isShowAllTable ? "keyboard_arrow_up" : "keyboard_arrow_down"}
                </Button>
            </div>
            {isShowAllTable && latticeObject.colorMap.map((row, idx) => (
                <ColorTableRow
                    id={String(idx)}
                    key={idx}
                    row={row}
                    draggable
                    onDragStart={(e) => onDragStart(e)}
                    onDragOver={(e) => onDragEnter(e)}
                    onDragEnd={() => onDragEnd()}
                />
            ))}
            <div className="color-map-grid-header">
                <p></p>
                <p></p>
                <p>
                    Unique: {latticeObject.colorMap.length}
                </p>
                <p></p>
                <div className="color-map-total-container">
                    <p>Total:</p>
                    <p>
                        {
                            latticeObject.colorMap
                                .filter((curr) => curr.isCount)
                                .map((row) => getNumberLabelInLatticeArrayByLabel(latticeObject.lattice, row.label))
                                .reduce((acc, curr) => acc + curr, 0)
                        }
                    </p>
                </div>
                <button
                    className="color-map-toggle-display-input"
                    title="Display the color map list for import or export"
                    onClick={() => setIsDisplayPaletteText((curr) => !curr)}
                >
                    <span className="material-symbols-outlined">
                        palette
                    </span>
                </button>
            </div>
            {isDisplayPaletteText && (
                <div className="color-map-input-text-container">
                    <textarea 
                        className="color-map-input-text"
                        onChange={(e) => updateColorMapFromTextarea(e.target.value)}
                        // onBlur={() => appendToStackHistory(latticeObject, colorMap)}
                        value={getColorMapAsText(latticeObject.colorMap)}
                        ref={colorTextarea}
                        style={{
                            height: (colorTextarea.current?.scrollHeight) + "px"
                        }}
                    />
                    <ButtonCopyToClipboard
                        title="Copy color map to clipboard in text format"
                        className="color-map-input-copy"
                        textToCopy={() => textToCopy()}
                        successText=""
                        errorText=""
                    />
                </div>
            )}
            <button
                className="color-map-add-row"
                onClick={addRowColorMap}
            >
                Add row
            </button>
        </section>
    );
}

// ************************************************************************** //
