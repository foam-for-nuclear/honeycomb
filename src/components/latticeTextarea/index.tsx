/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { useCallback, useContext } from 'react';
import { Dropdown } from './../dropdown';
import './styles.css';
import {
    getLatticeTextAsArray,
    getLatticeTextFromArray,
    extractLabelsFromLatticeText,
    translateFromCASMOtoMCNP,
    translateFromMCNPtoCASMO,
    translateFromMCNPtoOpenMC,
    translateFromOpenMCtoMCNP,
    extractLatticeCircularFromText,
    translateFromCircularToText,
    // getLatticeHeader,
} from '../../lib/latticeEdition';
import {
    LatticeType,
    LatticeContext,
    LatticeTextFormat,
    LatticeObject,
    ColorLine,
    LatticeParameterObject,
    // LatticeTextCodeFormat
} from '../../context/latticeContext';
import { completeColorMap } from '../../lib/colorMapEdition';
import { generateColorListFromRange } from '../../lib/color';
import { computeCoordinates, rotateVector } from '../../lib/mathFunctions';
import { ButtonCopyToClipboard } from '../buttonCopyToClipboard';
import { OrientationPicker } from '../orientationPicker';


export function LatticeTextarea({
    latticeObject,
    latticeType,
    latticeTextFormat,
    // latticeTextCodeFormat,
    latticeCoordOrientation,
    latticeParameters
}: {
    latticeObject: LatticeObject
    latticeType: LatticeType,
    latticeTextFormat: LatticeTextFormat,
    // latticeTextCodeFormat: LatticeTextCodeFormat,
    latticeCoordOrientation: number,
    latticeParameters: LatticeParameterObject
}) {
    const {
        setLatticeObjectWithHistory,
        setLatticeTextFormat,
        // setLatticeTextCodeFormat,
        setLatticeCoordOrientation
    } = useContext(LatticeContext);

    const latticeText = useCallback(() => {
        const latticeTextMCNP: string = getLatticeTextFromArray(
            latticeObject.lattice,
            latticeType === LatticeType.HEXAGON
        );

        switch (latticeTextFormat) {
            case LatticeTextFormat.RING:
                return(translateFromMCNPtoOpenMC(latticeTextMCNP));
            case LatticeTextFormat.ALTERNATED:
                return(translateFromMCNPtoCASMO(latticeTextMCNP));
            case LatticeTextFormat.TILTED:
                return(latticeTextMCNP);
            case LatticeTextFormat.CIRCULAR:
                if (latticeType === LatticeType.CIRCLE) {
                    return(translateFromCircularToText(latticeObject.lattice, latticeObject.circularParameters));
                }
        }
        return(latticeTextMCNP);
    }, [latticeObject.lattice, latticeTextFormat, latticeType, latticeObject.circularParameters]);


    const coordText = useCallback(() => {
        const coord: number[][] = computeCoordinates(
            latticeObject.lattice,
            latticeType,
            latticeParameters.pitch,
            latticeType === LatticeType.CIRCLE ? latticeObject.circularParameters : []
        ).flat()
            .map(e => rotateVector(e, latticeCoordOrientation));
        
        const flatLattice = latticeObject.lattice.flat();

        const text = coord.map((e, idx) => 
            String(e[0]) + ';' + String(e[1]) + ';' + flatLattice[idx]
        ).join('\n');

        return(text);
    }, [
        latticeObject.lattice, latticeObject.circularParameters, latticeType,
        latticeParameters.pitch, latticeCoordOrientation
    ]);


    const latticeSymbol = useCallback((format: LatticeTextFormat) => {
        switch (format) {
            case LatticeTextFormat.RING:
                return("target");
            case LatticeTextFormat.ALTERNATED:
                return("format_align_center");
            default:
                return("segment");
        }
    }, []);

    
    // const latticeTextCode = useCallback((format: LatticeTextCodeFormat) => {
    //     switch (format) {
    //         case LatticeTextCodeFormat.MCNP:
    //             return("MCNP");
    //         case LatticeTextCodeFormat.SERPENT:
    //             return("Serpent");
    //         case LatticeTextCodeFormat.OPENMC:
    //             return("OpenMC");
    //         case LatticeTextCodeFormat.CASMO:
    //             return("CASMO");
    //     }
    //     return('');
    // }, []);


    const latticeDropdownButtonTitle = useCallback((format: LatticeTextFormat, type: LatticeType) => {
        if (type === LatticeType.SQUARE) {
            return("Square lattice, only one format");
        }
        if (type === LatticeType.CIRCLE) {
            return("Circular lattice, only one format");
        }
        switch (format) {
            case LatticeTextFormat.RING:
                return("Select lattice format (in rings)");
            case LatticeTextFormat.ALTERNATED:
                return("Select lattice format (alternated)");
            default:
                return("Select lattice format (tilted)");
        }
    }, []);


    const updateOnChange = useCallback((event: React.ChangeEvent<HTMLTextAreaElement>) =>
    {
        const newLatticeText = event.target.value;
        let newCircularParams = [...latticeObject.circularParameters];
        if (newLatticeText.trim() === "" || newLatticeText.split('\n').length <= 1) {
            return;
        }
        const latticeCircularExtracted = extractLatticeCircularFromText(newLatticeText);
        
        let newLatticeArray: string[][] = [];
        switch (latticeTextFormat) {
            case LatticeTextFormat.RING:
                newLatticeArray = getLatticeTextAsArray(translateFromOpenMCtoMCNP(newLatticeText));
                break;
            case LatticeTextFormat.ALTERNATED:
                newLatticeArray = getLatticeTextAsArray(translateFromCASMOtoMCNP(newLatticeText));
                break;
            case LatticeTextFormat.TILTED:
                newLatticeArray = getLatticeTextAsArray(newLatticeText);
                break;
            case LatticeTextFormat.CIRCULAR:
                newLatticeArray = latticeCircularExtracted.array;
                newCircularParams = latticeCircularExtracted.parameters;
                break;
        }
        
        const labels = extractLabelsFromLatticeText(newLatticeArray);
        const isAllNumber = labels.every(value => !isNaN(Number(value)));
        
        const gradientColors: ColorLine[] = 
            generateColorListFromRange(
                labels.map(value => Number(value)),
                "#75B3F0",
                "#F07575"
            ).map((row, idx): ColorLine => {
                return({
                    color: row,
                    label: labels[idx],
                    isActivated: true,
                    isCount: true
                });
            }).sort((a, b) => Number(a.label) - Number(b.label));
            
        const newColorMap = completeColorMap(latticeObject.colorMap, newLatticeArray);
        
        setLatticeObjectWithHistory({
            ...latticeObject,
            lattice: newLatticeArray,
            colorMap: isAllNumber ? gradientColors : newColorMap,
            circularParameters: newCircularParams
        });
    }, [latticeObject, latticeTextFormat, setLatticeObjectWithHistory]);


    return (
        <section className="lattice-text">
            <div className="lattice-text-header">
                <Dropdown
                    logo={latticeSymbol(latticeTextFormat)}
                    // logo={latticeTextCode(latticeTextCodeFormat)}
                    titleButton={latticeDropdownButtonTitle(latticeTextFormat, latticeType)}
                    classNameButton='copy-button'
                    disabled={latticeType !== LatticeType.HEXAGON}
                    isLogo={true}
                    // isLogo={false}
                >
                    <button
                        title='MCNP (tilted lattice text)'
                        onClick={() => {
                            setLatticeTextFormat(LatticeTextFormat.TILTED);
                            // setLatticeTextCodeFormat(LatticeTextCodeFormat.MCNP);
                        }}
                    >
                        MCNP
                        <span className="material-symbols-outlined">
                            {latticeSymbol(LatticeTextFormat.TILTED)}
                        </span>
                    </button>
                    <button
                        title='Serpent (tilted lattice text)'
                        onClick={() => {
                            setLatticeTextFormat(LatticeTextFormat.TILTED)
                            // setLatticeTextCodeFormat(LatticeTextCodeFormat.SERPENT);
                        }}
                    >
                        Serpent
                        <span className="material-symbols-outlined">
                            {latticeSymbol(LatticeTextFormat.TILTED)}
                        </span>
                    </button>
                    <button
                        title='OpenMC (rings lattice text)'
                        onClick={() => {
                            setLatticeTextFormat(LatticeTextFormat.RING);
                            // setLatticeTextCodeFormat(LatticeTextCodeFormat.OPENMC);
                        }}
                    >
                        OpenMC
                        <span className="material-symbols-outlined">
                            {latticeSymbol(LatticeTextFormat.RING)}
                        </span>
                    </button>
                    <button
                        title='CASMO (alternated lattice text)'
                        onClick={() => {
                            setLatticeTextFormat(LatticeTextFormat.ALTERNATED);
                            // setLatticeTextCodeFormat(LatticeTextCodeFormat.CASMO);
                        }}
                    >
                        CASMO
                        <span className="material-symbols-outlined">
                            {latticeSymbol(LatticeTextFormat.ALTERNATED)}
                        </span>
                    </button>
                </Dropdown>
                <div className='lattice-text-coordinates-container'>
                    <ButtonCopyToClipboard
                        title="Copy coordinates of lattice elements to clipboard in text format (x;y;label)"
                        textToCopy={() => coordText()}
                        defaultText={'Coordinates'}
                    />
                    <OrientationPicker
                        orientation={latticeCoordOrientation}
                        setOrientation={setLatticeCoordOrientation}
                        classNameButton='copy-button'
                        titleButton='Change coordinates orientation'
                    >

                    </OrientationPicker>
                </div>
                <ButtonCopyToClipboard
                    title="Copy lattice to clipboard in text format"
                    textToCopy={() => latticeText()}
                    defaultText={'Lattice'}
                />
            </div>
            {/* <code className='lattice-text-codeheader'>
                {getLatticeHeader(
                    latticeTextCodeFormat,
                    latticeType,
                    latticeObject.lattice.length,
                    latticeObject.lattice[0].length,
                    0
                )}
            </code> */}
            <textarea
                title="Lattice text area where the lattice is auto generated in text format. It is also possible to copy-paste a lattice in this area to visualized it."
                onChange={(e) => updateOnChange(e)}
                value={latticeText()}
            />
        </section>
    );
}

// ************************************************************************** //
