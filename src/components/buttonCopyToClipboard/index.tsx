/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { memo, useCallback, useState } from 'react';
import './styles.css';
import { CopyState } from '../../context/latticeContext';


interface ButtonCopyToClipboardProps { // extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    title?: string,
    className?: string,
    textToCopy?: () => string,
    defaultText?: string,
    successText?: string,
    errorText?: string,
}

function ButtonCopyToClipboard({
    title = "",
    className = "copy-button",
    textToCopy = () => "",
    defaultText = "",
    successText = "Copied!",
    errorText = "Impossible to copy"
}: ButtonCopyToClipboardProps) {
    const [copiedStatus, setCopiedStatus] = useState(CopyState.DEFAULT);

    const copyToClipboard = useCallback(() => {
        if (navigator.clipboard)
        {
            navigator.clipboard.writeText(textToCopy()).then(() => {
                setCopiedStatus(CopyState.SUCCESS);
                window.setTimeout(() => {
                    setCopiedStatus(CopyState.DEFAULT);
                }, 2000);
            });
        }
        else {
            setCopiedStatus(CopyState.ERROR);
            window.setTimeout(() => {
                setCopiedStatus(CopyState.DEFAULT);
            }, 2000);
        }
    }, [textToCopy]);
    
    return (
        <button 
            title={title}
            className={className}
            onClick={copyToClipboard}
        >
            <i className="material-symbols-outlined">
                {copiedStatus === CopyState.SUCCESS ? "inventory" : "content_paste"}
            </i>
            {defaultText !== "" && (
                <span className="copy-button-span">
                    {copiedStatus === CopyState.DEFAULT && defaultText}
                    {copiedStatus === CopyState.SUCCESS && successText}
                    {copiedStatus === CopyState.ERROR && errorText}
                </span>
            )}
        </button>
    );
}

const memoButtonCopyToClipboard = memo(ButtonCopyToClipboard);

export { memoButtonCopyToClipboard as ButtonCopyToClipboard };

// ************************************************************************** //
