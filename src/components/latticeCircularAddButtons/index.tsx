/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { memo, useCallback, useContext, useMemo, useState } from 'react';
import './styles.css';
import { LatticeContext, LatticeObject, LatticeParameterObject } from '../../context/latticeContext';


interface LatticeCircularAddButtonsProps { // extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    latticeObject: LatticeObject,
    latticeParameters: LatticeParameterObject,
    buttonWidth?:number,
    areaWidthRatio?:number,
    isPlaceRight?:boolean,
    isPlaceLeft?:boolean
}

function LatticeCircularAddButtons({
    latticeObject,
    latticeParameters,
    buttonWidth=1.25,
    areaWidthRatio=2,
    isPlaceRight=false,
    isPlaceLeft=false
} : LatticeCircularAddButtonsProps) {

    const {
        setLatticeObjectWithHistory
    } = useContext(LatticeContext);

    const [idxDisplay, setIdxDisplay] = useState([-1, -1]);


    const coord: number[][][] = useMemo(() => 
        latticeObject.lattice.map((ring, ringIdx) => 
            ring.map((_, angleIdx) => {
                const nElements = latticeObject.lattice[ringIdx].length;
                const theta = 2 * Math.PI / nElements;
                const initialRadius = latticeObject.circularParameters[ringIdx]?.r || 0;
                const radius = latticeParameters.scale * initialRadius + latticeParameters.tileSize/3;
                const nMax = latticeParameters.tileSize * nElements / (2*Math.PI*latticeParameters.scale * initialRadius);

                let offsetAngle: number = 0;
                if (isPlaceRight && nMax <= 0.7) {
                    offsetAngle = Math.atan2( latticeParameters.tileSize/2, radius) + angleIdx * theta;
                } else if (isPlaceLeft && nMax <= 0.7) {
                    offsetAngle = Math.atan2(-latticeParameters.tileSize/2, radius) + (angleIdx+1) * theta;
                } else {
                    offsetAngle = (angleIdx + 0.5) * theta;
                }

                const angle = offsetAngle + Math.PI/180 * (latticeObject.circularParameters[ringIdx]?.theta || 0);
                const xCoord = -radius * Math.cos(angle);
                const yCoord = +radius * Math.sin(angle);

                return([xCoord, yCoord, initialRadius]);
            })
        )
    , [latticeObject, latticeParameters, isPlaceLeft, isPlaceRight]);


    const onClick = useCallback((ringIdx: number, angleIdx: number) => {
        const newLattice: string[][] = [...latticeObject.lattice];
        newLattice[ringIdx].splice(angleIdx+1, 0, '0');

        setLatticeObjectWithHistory({
            ...latticeObject,
            lattice: newLattice
        });
    }, [latticeObject, setLatticeObjectWithHistory]);


    const displayStyle = useCallback((ringIdx: number, angleIdx: number) => {
        return(
            coord[ringIdx][angleIdx][2] > 0 &&
            idxDisplay[0] === ringIdx &&
            idxDisplay[1] === angleIdx
                ? "flex"
                : "none"
        )
    }, [coord, idxDisplay]);


    return(
        <div>
            {latticeObject.lattice.map((ring, ringIdx) => (
                <div key={ringIdx}>
                    {ring.map((_, angleIdx) => (
                        <div
                            key={angleIdx}
                            className='lattice-circular-add-button-area'
                            style={{
                                display: coord[ringIdx][angleIdx][2] > 0 ? 'flex' : 'none',
                                top: 'calc(' + coord[ringIdx][angleIdx][0] + 'px - ' + areaWidthRatio * buttonWidth/2+'rem + 50%)',
                                left: 'calc(' + coord[ringIdx][angleIdx][1] + 'px - ' + areaWidthRatio * buttonWidth/2+'rem + 50%)',
                                width: areaWidthRatio * buttonWidth + "rem",
                                height: areaWidthRatio * buttonWidth + "rem",
                                borderRadius: areaWidthRatio * buttonWidth + "rem",
                            }}
                            onMouseOver={() => setIdxDisplay([ringIdx, angleIdx])}
                            onMouseLeave={() => setIdxDisplay([-1, -1])}
                        >
                            <button
                                title='Add an element'
                                className='lattice-circular-add-button'
                                style={{
                                    display: displayStyle(ringIdx, angleIdx),
                                    width: buttonWidth + "rem",
                                    height: buttonWidth + "rem",
                                    borderRadius: buttonWidth + "rem"
                                }}
                                onClick={() => onClick(ringIdx, angleIdx)}
                            >
                                +
                            </button>
                        </div>
                    ))}
                </div>
            ))}
        </div>
    )
}

const memoLatticeCircularAddButtons = memo(LatticeCircularAddButtons);

export { memoLatticeCircularAddButtons as LatticeCircularAddButtons };

// ************************************************************************** //
