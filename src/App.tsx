/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

import { Header } from "./components/header";
import { Footer } from "./components/footer";
import { LatticeProvider } from "./context/latticeContext";
import './App.css';

function App() {

  return (
    <>
      <Header />
      <LatticeProvider />
      <Footer />
    </>
  )
}

export default App

// ************************************************************************** //
