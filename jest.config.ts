/*----------------------------------*- TS -*-----------------------------------\
|  H O N   |                                                                   |
|   E Y C  | Honeycomb - Lattice Visualizer and Editor                         |
|    O M B |                                                                   |
|  Copyright (C) (2023-2024) EPFL                                              |
|  Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL (Switzerland)  |
\*----------------------------------------------------------------------------*/

export default {
    preset: 'ts-jest',
    testEnvironment: 'jest-environment-jsdom',
    testMatch: [
        '<rootDir>/src/**/*.test.ts'
    ],
    transform: {
        // "^.+\\.tsx?$": "ts-jest",
        ".+\\.(css|styl|less|sass|scss|min.css)$": "jest-transform-css"
        // process `*.tsx` files with `ts-jest`
    },
    moduleNameMapper: {
        "^.+\\.(css|less|sass|scss)$": "<rootDir>/src/test/__mocks__/styleMock.js",
        '\\.(gif|ttf|eot|svg|png)$': '<rootDir>/src/test/__mocks__/fileMock.js',
    },
    testPathIgnorePatterns: [
        "<rootDir>/node_modules/",
        "<rootDir>/src/components/" // Adjust the path to ignore .tsx files
    ],
}
